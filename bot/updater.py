import logging
import ssl
from multiprocessing import Event
from os import path

import tornado
from botmanlib.dispatcher import BotmanDispatcher
from botmanlib.updater import BotmanUpdater
from bs4 import BeautifulSoup as Soup
from telegram.error import BadRequest, TelegramError
from telegram.utils.webhookhandler import WebhookHandler, WebhookServer, ImageHandler

from bot.plot import create_plot
from models import StrategyType
from settings import PLOTS_FOLDER

logger = logging.getLogger(__name__)


class PlotHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ["GET"]

    def __init__(self, application, request, **kwargs):
        super(PlotHandler, self).__init__(application, request, **kwargs)
        self.logger = logging.getLogger(__name__)

    def initialize(self, bot):
        self.bot = bot

    def get(self, ticker):
        self.logger.debug('Webhook GET triggered')
        try:
            timeframe = self.request.arguments.get('timeframe', None)
            timeframe = int(timeframe[0].decode('utf-8')) if timeframe else 1
            strategy = self.request.arguments.get('strategy', None)
            if strategy:
                try:
                    strategy = StrategyType[strategy[0]]
                except KeyError:
                    strategy = None

            html = open(path.join(PLOTS_FOLDER, 'plot.html'), 'r')
            soup = Soup(html, 'html.parser')
            plot_div = soup.find("div", {"id": "plot"})
            div = create_plot(ticker, timeframe, strategy)
            plot_div.append(Soup(div, 'html.parser'))

            self.set_status(200)
            self.write(soup.prettify())
        except BadRequest:
            self.set_status(404)


class InprofitWebhookAppClass(tornado.web.Application):
    def __init__(self, webhook_path, bot, update_queue):
        self.shared_objects = {"bot": bot, "update_queue": update_queue}
        handlers = [
            (r"{0}/?image/(.*)".format(webhook_path), ImageHandler,
             {"bot": bot}),
            (r"{0}/?plot/(.*)".format(webhook_path), PlotHandler,
             {"bot": bot}),
            (r"{0}/?".format(webhook_path), WebhookHandler,
             self.shared_objects)
        ]
        tornado.web.Application.__init__(self, handlers)

    def log_request(self, handler):
        return


class InprofitUpdater(BotmanUpdater):

    def __init__(self,
                 token=None,
                 base_url=None,
                 workers=4,
                 bot=None,
                 private_key=None,
                 private_key_password=None,
                 user_sig_handler=None,
                 request_kwargs=None,
                 persistence=None,
                 use_context=False,
                 use_sessions=False):
        super(BotmanUpdater, self).__init__(token=token,
                                            base_url=base_url,
                                            workers=workers,
                                            bot=bot,
                                            private_key=private_key,
                                            private_key_password=private_key_password,
                                            user_sig_handler=user_sig_handler,
                                            request_kwargs=request_kwargs,
                                            persistence=persistence,
                                            use_context=use_context,
                                            )

        self.__exception_event = Event()
        self.dispatcher = BotmanDispatcher(
            self.bot,
            self.update_queue,
            job_queue=self.job_queue,
            workers=workers,
            exception_event=self.__exception_event,
            persistence=persistence,
            use_context=use_context,
            use_sessions=use_sessions)
        self.job_queue.set_dispatcher(self.dispatcher)

    def _start_webhook(self, listen, port, url_path, cert, key, bootstrap_retries, clean,
                       webhook_url, allowed_updates):
        self.logger.debug('Updater thread started (webhook)')
        use_ssl = cert is not None and key is not None
        if not url_path.startswith('/'):
            url_path = '/{0}'.format(url_path)

        # Create Tornado app instance
        app = InprofitWebhookAppClass(url_path, self.bot, self.update_queue)

        # Form SSL Context
        # An SSLError is raised if the private key does not match with the certificate
        if use_ssl:
            try:
                ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
                ssl_ctx.load_cert_chain(cert, key)
            except ssl.SSLError:
                raise TelegramError('Invalid SSL Certificate')
        else:
            ssl_ctx = None

        # Create and start server
        self.httpd = WebhookServer(listen, port, app, ssl_ctx)

        if use_ssl:
            # DO NOT CHANGE: Only set webhook if SSL is handled by library
            if not webhook_url:
                webhook_url = self._gen_webhook_url(listen, port, url_path)

            self._bootstrap(
                max_retries=bootstrap_retries,
                clean=clean,
                webhook_url=webhook_url,
                cert=open(cert, 'rb'),
                allowed_updates=allowed_updates)
        elif clean:
            self.logger.warning("cleaning updates is not supported if "
                                "SSL-termination happens elsewhere; skipping")

        self.httpd.serve_forever()

