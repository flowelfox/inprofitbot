import datetime
import logging
import os
import pprint
import re
from re import sub

from botmanlib.bot import BotmanBot
from botmanlib.menus.helpers import get_settings, generate_regex_handlers
from emoji import demojize, emojize
from telegram import TelegramError, Message, Chat
from telegram.ext import MessageHandler, Filters, messagequeue, CallbackQueryHandler, PrefixHandler
from telegram.utils.request import Request

from bot.jobs import start_check_blocked_job, stop_check_blocked_job, start_signals_expire_job, stop_signals_expire_job, start_autotrade_expire_job, stop_autotrade_expire_job, start_check_api_keys_job, stop_check_api_keys_job
from bot.menus.admin.admin import AdminMenu
from bot.menus.disable import DisableMenu
from bot.menus.start import StartMenu
from bot.updater import InprofitUpdater
from models import DBSession, User, SignalsSubscription, Account, AutotradeSubscription, SubscriptionType, Strategy, Coin, StrategyType, SignalSubscriptionType
from settings import MEDIA_FOLDER, RESOURCES_FOLDER, SETTINGS_FILE, WEBHOOK_ENABLE, WEBHOOK_IP, WEBHOOK_PORT, WEBHOOK_URL

logger = logging.getLogger(__name__)


def stop(update, context):
    user = DBSession.query(User).filter(User.chat_id == update.message.chat_id).first()
    context.user_data.clear()
    user.deactivate()
    DBSession.add(user)
    DBSession.commit()


def error(update, context):
    """Log Errors caused by Updates."""
    pp = pprint.PrettyPrinter(indent=4)
    logger.error(f'Update "{pp.pformat(str(update))}" caused error "{context.error}"')


def goto_start(update, context):
    if update.effective_message and update.effective_message.message_id != 0:
        try:
            context.bot.delete_message(chat_id=update.effective_chat.id, message_id=update.effective_message.message_id)
        except TelegramError as e:
            logger.error(str(e))

    update.message = Message(0, update.effective_user, datetime.datetime.utcnow(), Chat(0, Chat.PRIVATE), text='/start', bot=context.bot)
    update._effective_message = None
    update.callback_query = None
    context.update_queue.put(update)


def mark_report(update, context):
    message = update.effective_message
    if update.callback_query.data.endswith("checked"):
        smile = emojize(":white_heavy_check_mark:")
    else:
        smile = emojize(":exclamation_question_mark:")
    new_text = smile + emojize(sub(":\w+?:", "", demojize(message.text_html), 1))
    message.edit_text(text=new_text, parse_mode="HTML")


def autotrade_update(update, context):
    mark_report(update, context)
    if update.callback_query.data.endswith("checked"):
        data = update.callback_query.data
        term = int(re.findall(r"t\d+", data)[0][1:])
        account_id = int(re.findall(r"a\d+", data)[0][1:])
        account = DBSession.query(Account).get(account_id)
        type = re.findall(r"y[a-z]+", data)[0][1:]
        settings = get_settings(SETTINGS_FILE)
        commission = settings.get("autotrade_commission", 50)
        daily_profit = 0.5 if type == 'short' else 0.3
        usd_price = daily_profit * term / 100 * account.get_usd_balance('margin') * (commission / 100)
        user = account.user

        if user.referrer and not user.subscription:
            referrer_reward = settings["referrer_reward"]
            user.referrer.balance += usd_price * (referrer_reward / 100)
            DBSession.add(user.referrer)

        type_enum = SubscriptionType.short_term if type == 'short' else SubscriptionType.mid_term
        if account.subscription is None or (account.subscription and not account.subscription.is_active):
            subscription = AutotradeSubscription(expire_date=datetime.datetime.utcnow() + datetime.timedelta(days=term), price=usd_price, type=type_enum)
            account.subscription = subscription
            DBSession.add(subscription)
        else:
            account.subscription.expire_date += datetime.timedelta(days=term)
            DBSession.add(account.subscription)
            DBSession.commit()

        if account.coins:
            # renew strategies
            for coin in account.coins:
                for strategy in coin.strategies:
                    strategy.activate(DBSession)
            DBSession.commit()

        else:
            # create strategies
            if type == 'short':
                for coin_pair in [("BAB", "USD"), ("BTC", "USD"), ("EOS", "USD"), ("ETH", "USD"), ("LTC", "USD"), ("XRP", "USD")]:
                    coin = Coin(left_ticker=coin_pair[0], right_ticker=coin_pair[1], deposit_part=0.16, account=account)
                    strategy = Strategy(coin=coin, active=True)
                    if coin_pair[0] == "XRP":
                        strategy.indicator = StrategyType.han_solo_nodir
                        strategy.periods = (144, 55)
                        strategy.timeframe = 60
                        strategy.pyramiding = 1
                        strategy.trailing_stop_perc = 3
                    else:
                        strategy.indicator = StrategyType.han_solo_nodir
                        strategy.periods = (96, 44)
                        strategy.timeframe = 60
                        strategy.pyramiding = 1
                        strategy.trailing_stop_perc = 3
                    DBSession.add(strategy)
                    DBSession.add(coin)

        DBSession.add(account)
        DBSession.commit()


def signal_referrer_update(update, context):
    mark_report(update, context)
    if update.callback_query.data.endswith("checked"):
        data = update.callback_query.data
        term = int(re.findall(r"t\d+", data)[0][1:])
        user_id = int(re.findall(r"u\d+", data)[0][1:])
        subtypestr = re.findall(r"S[a-z]+", data)[0][1:]
        if subtypestr == 'bitfinexshortterm':
            subtype = SignalSubscriptionType.bitfinex_short_term
        elif subtypestr == 'binanceshortterm':
            subtype = SignalSubscriptionType.binance_short_term
        else:
            subtype = None

        settings = get_settings(SETTINGS_FILE)
        prices = settings.get("signals_prices")
        usd_price = prices.get(str(term), 0)

        user = DBSession.query(User).get(user_id)

        if user.referrer and not user.subscription:
            referrer_reward = settings["referrer_reward"]
            user.referrer.balance += usd_price * (referrer_reward / 100)
            DBSession.add(user.referrer)

        if user.subscription is None or (user.subscription and not user.subscription.is_active):
            subscription = SignalsSubscription(expire_date=datetime.datetime.utcnow() + datetime.timedelta(days=term), price=usd_price, type=subtype)
            user.subscription = subscription
            DBSession.add(subscription)
        else:
            user.subscription.expire_date += datetime.timedelta(days=term)
            DBSession.add(user.subscription)

        DBSession.add(user)
        DBSession.commit()


def withdraw_user_update(update, context):
    mark_report(update, context)
    if update.callback_query.data.endswith("checked"):
        data = update.callback_query.data
        user_id = int(re.findall(r"u\d+", data)[0][1:])

        user = DBSession.query(User).get(user_id)
        if user:
            user.balance = 0
            DBSession.add(user)
            DBSession.commit()


def close_distribution_message(update, context):
    if update.effective_message:
        update.effective_message.delete()

    update.callback_query.answer()


def main():
    bot_token = os.environ['bot.token']
    mqueue = messagequeue.MessageQueue(all_burst_limit=30, all_time_limit_ms=1000)
    request = Request(con_pool_size=8)

    bot = BotmanBot(token=bot_token, request=request, mqueue=mqueue)
    updater = InprofitUpdater(bot=bot, use_context=True, use_sessions=True)

    job_queue = updater.job_queue
    dispatcher = updater.dispatcher

    if not os.path.exists(MEDIA_FOLDER):
        os.mkdir(MEDIA_FOLDER)
        logger.info("Media folder created")
    if not os.path.exists(RESOURCES_FOLDER):
        os.mkdir(RESOURCES_FOLDER)
        logger.info("Resources folder created")
    if not os.path.exists(SETTINGS_FILE):
        with open(SETTINGS_FILE + ".default", 'r') as settings:
            out = open(SETTINGS_FILE, 'w')
            out.write(settings.read())
            out.close()
        logger.info("Settings file created")

    start_menu = StartMenu(bot=bot, dispatcher=dispatcher)
    admin_menu = AdminMenu(bot=bot, dispatcher=dispatcher)
    disable_menu = DisableMenu(bot=bot, dispatcher=dispatcher)

    dispatcher.add_handler(CallbackQueryHandler(mark_report, pattern=r"^(autotrade|signals|withdraw)_mark_\w+$"))
    dispatcher.add_handler(CallbackQueryHandler(signal_referrer_update, pattern=r"^signals_t\d+_u\d+_S\w+_mark_checked$"))
    dispatcher.add_handler(CallbackQueryHandler(autotrade_update, pattern=r"^autotrade_a\d+_t\d+_y\w+_mark_checked$"))
    dispatcher.add_handler(CallbackQueryHandler(withdraw_user_update, pattern=r"^withdraw_u\d+_mark_checked$"))
    dispatcher.add_handler(CallbackQueryHandler(close_distribution_message, pattern=r"^close_message$"))
    dispatcher.add_handler(MessageHandler(Filters.channel | Filters.group, lambda bot, update: None))
    for handler in generate_regex_handlers("Main menu", goto_start):
        dispatcher.add_handler(handler)
    dispatcher.add_handler(PrefixHandler('/', 'stop', stop))
    dispatcher.add_handler(start_menu.accounts_menu.account_info.account_sub.handler)
    dispatcher.add_handler(start_menu.handler)
    dispatcher.add_handler(admin_menu.handler)
    dispatcher.add_handler(disable_menu.handler)
    dispatcher.add_handler(MessageHandler(Filters.all, goto_start))
    dispatcher.add_handler(CallbackQueryHandler(goto_start))
    dispatcher.add_error_handler(error)

    if WEBHOOK_ENABLE:
        update_queue = updater.start_webhook(listen=WEBHOOK_IP, port=WEBHOOK_PORT, allowed_updates=['message', 'edited_message', 'callback_query', 'channel_post'])
        updater.bot.set_webhook(url=WEBHOOK_URL)
    else:
        update_queue = updater.start_polling(allowed_updates=['message', 'edited_message', 'callback_query', 'channel_post'])

    start_check_blocked_job(job_queue)
    start_signals_expire_job(job_queue)
    start_autotrade_expire_job(job_queue)
    start_check_api_keys_job(job_queue)
    logger.info("Bot started")
    updater.idle()
    stop_check_blocked_job(job_queue)
    stop_signals_expire_job(job_queue)
    stop_autotrade_expire_job(job_queue)
    stop_check_api_keys_job(job_queue)
    bot.stop()


if __name__ == '__main__':
    main()
