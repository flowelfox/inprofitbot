import io
from pathlib import Path

import matplotlib.pyplot as plt
from PIL import Image
from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, require_permission
from botmanlib.messages import delete_interface, send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from models import DBSession, Wallet, WalletChange, Account
from settings import RESOURCES_FOLDER


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)


class YieldCurveMenu(BaseMenu):
    menu_name = 'yield_curve_menu'

    @require_permission(Account.yield_curve_permission)
    def entry(self, update, context):
        _ = context.user_data['_']
        account = DBSession.query(Account).get(context.user_data[self.parent.menu_name]['account_id'])

        if account.get_usd_balance('margin') != 0:
            self.send_message(context)
            context.bot.answer_callback_query(update.callback_query.id)
            return self.States.ACTION
        else:
            context.bot.answer_callback_query(update.callback_query.id, text=_("Your balance is not updated yet.\n\nTry again in 5 minutes."), show_alert=True)
            return ConversationHandler.END

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')]]

        image = self.generate_chart(context)

        delete_interface(context)
        send_or_edit(context, chat_id=user.chat_id, photo=image, reply_markup=InlineKeyboardMarkup(buttons))

    def generate_chart(self, context):
        account = DBSession.query(Account).get(context.user_data[self.parent.menu_name]['account_id'])
        wallet_changes = DBSession.query(WalletChange) \
            .join(WalletChange.wallet) \
            .filter(Wallet.account == account) \
            .filter(Wallet.type == 'margin') \
            .filter(Wallet.currency == "USD") \
            .order_by(WalletChange.create_date).all()

        balance = []
        dates = []

        for wc in wallet_changes:
            balance.append(wc.balance + wc.unsettled_interest)
            dates.append(wc.create_date)

        plt.figure()
        plt.xlabel("Dates")
        plt.ylabel("Balance")
        plt.title(f"{account.name} yield curve")
        plt.plot(dates, balance)
        plt.gcf().autofmt_xdate()
        plt.gcf().set_size_inches(cm2inch(29.7, 21))
        plt.ylim(ymin=0, ymax=max(balance) * 2)

        with io.BytesIO() as buf:
            plt.savefig(buf, format='png')
            buf.seek(0)

            base_image = Image.open(buf)
            watermark = Image.open(Path(RESOURCES_FOLDER, 'watermark.png'))

            # add watermark to your image
            base_image.paste(watermark, ((base_image.width - watermark.width) // 2, (base_image.height - watermark.height) // 2), mask=watermark)
            image = io.BytesIO()
            base_image.convert('RGB').save(image, format="PNG")
            image.seek(0)

        return image

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Empty") + '\n\n'

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data=f'back_from_help_{self.menu_name}')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^yield_curve$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               CallbackQueryHandler(self.entry, pattern=f'^back_from_help_{self.menu_name}$'),
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION)),
                                                               ],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, self.conv_fallback)],
                                      allow_reentry=True)

        return handler
