from botmanlib.menus.add_edit_menu.arrow_add_edit_menu import ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, require_permission
from botmanlib.menus.list_menus.bunch_list_menu import BunchListMenu
from botmanlib.messages import delete_interface, send_or_edit, delete_user_message
from emoji import emojize
from formencode import validators, Invalid, Pipe
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, PrefixHandler

from bot.menus.account_info import AccountInfoMenu
from models import DBSession, Account, Provider


class AccountsMenu(BunchListMenu):
    menu_name = 'accounts_menu'
    auto_hide_arrows = True

    @require_permission(Account.list_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        return super(AccountsMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']
        return DBSession.query(Account).filter(Account.user_id == user.id).order_by(Account.name).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^accounts$')]

    def message_text(self, context, page_objects):
        _ = context.user_data['_']

        if page_objects:
            message_text = _("Accounts") + "\n"
            message_text += f"{'─' * 15}\n"
            for account in page_objects:
                message_text += f"\u2022 {account.provider.value} - {account.name }\n"
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def object_buttons(self, context, page_objects):
        if page_objects:
            buttons = []
            for page_object in page_objects:
                buttons.append([InlineKeyboardButton(page_object.name, callback_data=f"account_{page_object.id}")])

            return buttons
        else:
            return None

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']

        message_text = f"{'─' * 15}\n"
        if max_page > 1:
            message_text += _("Page") + f" {current_page} " + _("from") + f" {max_page}"
        return message_text

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if user.has_permission(Account.add_permission):
            buttons.append(InlineKeyboardButton(_("Add account"), callback_data="account_new"))
        return buttons

    def additional_states(self):
        new_account = AddAccountMenu(self, bot=self.bot)
        self.account_info = AccountInfoMenu(self, bot=self.bot)

        return {self.States.ACTION: [new_account.handler,
                                     self.account_info.handler,
                                     PrefixHandler('/', 'help', self.help),
                                     CallbackQueryHandler(self.parent.delete_help, pattern="^delete_help$"),
                                     CallbackQueryHandler(self.entry, pattern=f'^back_from_help_{self.menu_name}$')]}

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('This is Accounts menu of InProfit Bot.\n{line}\n'
                         '1. The "<b>Add Account</b>" button opens the menu for adding a Bitfinex exchange account.\n\n'
                         '2. Next are your accounts if you have one. Click the button with the name of the account to open the menu of viewing the account and activate the subscription for automatic trading.'
                         # '3. The "Public InProfit Account" button message_text = "\n"opens the menu for viewing the account of our team.'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)
        return self.States.ACTION


class IsLatin(validators.FancyValidator):
    messages = {'wrong_encoding': 'Text not in latin-1'}

    def _validate_python(self, value, state):
        try:
            value.encode("latin-1")
        except UnicodeEncodeError:
            raise Invalid(self.message("wrong_encoding", state), value, state)


class AddAccountMenu(ArrowAddEditMenu):
    menu_name = 'account_create_menu'
    model = Account
    force_action = ArrowAddEditMenu.Action.ADD

    @require_permission(Account.add_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        return super(AddAccountMenu, self).entry(update, context)

    def query_object(self, context):
        return None

    def message_bottom_text(self, context):
        _ = context.user_data['_']
        message_text = emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += _("Required permissions:") + '\n'
        message_text += '- ' + _("Account - READ") + '\n'
        message_text += '- ' + _("Orders - READ and WRITE") + '\n'
        message_text += '- ' + _("Positions - READ and WRITE") + '\n'
        message_text += '- ' + _("Wallets - READ") + '\n'
        return message_text

    def fields(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if user.has_permission('allow_create_binance_account'):
            provider_variants = ["BITFINEX", "BINANCE"]
        else:
            provider_variants = ["BITFINEX"]

        return [ArrowAddEditMenu.Field('name', _('*Name'), validators.String(), required=True),
                ArrowAddEditMenu.Field('api_key', _('*Key'), Pipe(validators.String(), IsLatin()), required=True),
                ArrowAddEditMenu.Field('secret', _('*Secret'), Pipe(validators.String(), IsLatin()), required=True),
                ArrowAddEditMenu.Field('provider', _('*Exchange'), validators.String(), variants=provider_variants, required=True)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^account_new$')]

    def save_object(self, obj, context, session=None):
        user = context.user_data['user']
        obj.user_id = user.id
        obj.provider = Provider[obj.provider]
        obj.check_account()

        if not add_to_db(obj, session):
            return self.conv_fallback(context)
        self.parent.update_objects(context)
        delete_interface(context)

    def back(self, update, context):
        delete_interface(context, 'help')
        return super(AddAccountMenu, self).back(update, context)

    def fallbacks(self):
        return [PrefixHandler('/', 'help', self.parent.parent.missing_help)] + super(AddAccountMenu, self).fallbacks()