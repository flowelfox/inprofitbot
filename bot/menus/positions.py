from botmanlib.menus.helpers import require_permission
from botmanlib.menus.list_menus.bunch_list_menu import BunchListMenu
from botmanlib.messages import delete_interface, send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, PrefixHandler

from models import DBSession, Account, Position, Signal, Strategy, Coin, Order


class PositionsMenu(BunchListMenu):
    menu_name = 'positions_menu'
    search_key_param = 'name'
    auto_hide_arrows = True

    @require_permission(Position.view_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        return super(PositionsMenu, self).entry(update, context)

    def query_objects(self, context):
        account = DBSession.query(Account).filter(Account.id == context.user_data[self.parent.menu_name]['account_id']).first()

        if account.is_valid:
            return DBSession.query(Position) \
                .join(Position.order) \
                .filter(Order.account_id == account.id) \
                .filter(Position.is_active == True) \
                .order_by(Position.create_date.asc())\
                .all()
        else:
            return DBSession.query(Position) \
                .join(Position.signal) \
                .join(Signal.strategy) \
                .join(Strategy.coin) \
                .join(Coin.account) \
                .filter(Account.id == account.id) \
                .filter(Position.is_active == True) \
                .order_by(Position.create_date.asc()) \
                .all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^positions$')]

    def message_text(self, context, page_objects):
        _ = context.user_data['_']

        account = self.parent.query_object(context)
        if page_objects:
            message_text = account.name + ' / ' + _("Positions") + "\n"
            message_text += f"{'─' * 15}\n"
            for position in page_objects:
                message_text += f"\u2022 <code>" + position.name + '</code>\n'
        else:
            message_text = account.name + ' / ' + _("Positions") + "\n"
            message_text += f"{'─' * 15}\n"
            message_text += _("There is nothing to list") + '\n'

        return message_text

    def object_buttons(self, context, page_objects):
        user = context.user_data['user']
        _ = context.user_data['_']
        if page_objects and user.has_permission('allow_remove_all_positions'):
            return [[InlineKeyboardButton(_("Remove all positions"), callback_data='remove_all_positions')]]
        else:
            return None

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']

        message_text = f"{'─' * 15}\n"
        if max_page > 1:
            message_text += _("Page") + f" {current_page} " + _("of") + f" {max_page}"
        return message_text

    def remove_all_positions_ask(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']
        buttons = [[InlineKeyboardButton(_("Yes"), callback_data="remove_all_positions_yes"),
                    InlineKeyboardButton(_("No"), callback_data="remove_all_positions_no")]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Are you sure about remove all positions?"), reply_markup=InlineKeyboardMarkup(buttons))
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def remove_all_positions(self, update, context):
        _ = context.user_data['_']
        data = update.callback_query.data

        if data == 'remove_all_positions_yes':
            for position in self.query_objects(context):
                position.close_position(session=DBSession)

            context.bot.answer_callback_query(update.callback_query.id, text=_("All positions removed."), show_alert=True)
            self.update_objects(context)
        else:
            context.bot.answer_callback_query(update.callback_query.id)

        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.remove_all_positions_ask, pattern='^remove_all_positions$'),
                                     CallbackQueryHandler(self.remove_all_positions, pattern='^remove_all_positions_(yes|no)$')]}

    def back(self, update, context):
        delete_interface(context, 'help')
        return super(PositionsMenu, self).back(update, context)

    def fallbacks(self):
        return [PrefixHandler('/', 'help', self.parent.parent.parent.missing_help)] + super(PositionsMenu, self).fallbacks()
