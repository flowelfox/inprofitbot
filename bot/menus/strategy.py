import enum
from threading import Event

import formencode
from botmanlib.menus.add_edit_menu.arrow_add_edit_menu import ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, require_permission
from botmanlib.menus.list_menus.one_list_menu import OneListMenu
from botmanlib.messages import delete_interface
from botmanlib.models import Database
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import ConversationHandler, CallbackQueryHandler

from backend.controllers.bitfinex.signal_controller import SignalFormer as BFXSignalFormer
from backend.controllers.binance.signal_controller import SignalFormer as BNCSignalFormer
from models import DBSession, Provider
from models import Strategy, StrategyType
from settings import WEBHOOK_URL


class StrategiesMenu(OneListMenu):
    menu_name = 'strategies_menu'
    search_key_param = 'name'
    model = Strategy
    delete_session = DBSession
    auto_hide_arrows = True

    class States(enum.Enum):
        ACTION = 1

    def query_objects(self, context):
        return DBSession.query(Strategy).filter(Strategy.coin_id == context.user_data[self.menu_name]['coin_id']).order_by(Strategy.active.desc()).all()

    @require_permission(Strategy.view_permission)
    def entry(self, update, context):
        self._load(context)
        data = int(update.callback_query.data.replace("strategies_", ""))
        context.user_data[self.menu_name]['coin_id'] = data
        context.user_data[self.menu_name]['strategy_interrupter'] = None

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^strategies_\d+$')]

    def back(self, update, context):
        self.parent.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def message_text(self, context, obj):
        _ = context.user_data['_']

        if obj:
            code_s = "<code>"
            code_e = "</code>"
            bold_s = "<b>"
            bold_e = "</b>"

            message_text = "_____" + obj.coin.account.name + '/' + obj.coin.ticker + "_____\n"
            message_text += bold_s + obj.name + bold_e + '\n'
            if obj.indicator == StrategyType.ao:
                periods_text = "Fast period:" + code_s + ' ' + (str(obj.periods[1]) if len(obj.periods) >= 1 else _("Unknown")) + code_e + '\n'
                periods_text += "Slow period:" + code_s + ' ' + (str(obj.periods[0]) if len(obj.periods) >= 2 else _("Unknown")) + code_e + '\n'
            elif obj.indicator == StrategyType.ma2oc:
                periods_text = "Smoothing:" + code_s + ' ' + (str(obj.periods[0]) if len(obj.periods) >= 1 else _("Unknown")) + code_e + '\n'
            else:
                periods_text = _("Periods:") + code_s + ' ' + (" / ".join([str(p) for p in obj.periods]) if obj.periods else _("Unknown")) + code_e + '\n'
            message_text += periods_text
            message_text += _("Timeframe:") + code_s + ' ' + obj.htimeframe + code_e + '\n'
            message_text += _("Pyramiding:") + code_s + ' ' + f"{obj.pyramiding}" + code_e + '\n'
            message_text += _("Trailing stop:") + code_s + ' ' + (f"{obj.trailing_stop_perc}%" if obj.trailing_stop_perc else _("Disabled")) + code_e + '\n'
            message_text += _("Active:") + code_s + ' ' + (_("Yes") if obj.active else _("No")) + code_e + '\n'
        else:
            coin = context.user_data[self.parent.menu_name]['objects'][context.user_data[self.parent.menu_name]['selected_object']]
            message_text = "_____" + coin.account.name + '/' + coin.ticker + '/' + _("Strategies") + "_____\n"
            message_text += _("There is nothing to show") + '\n'

        return message_text

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if obj:
            if obj.active:
                if user.has_permission(Strategy.deactivate_permission):
                    buttons.append([InlineKeyboardButton(_("Deactivate"), callback_data="strategy_deactivate")])
            else:
                if user.has_permission(Strategy.activate_permission):
                    buttons.append([InlineKeyboardButton(_("Activate"), callback_data="strategy_activate")])
            if user.has_permission(Strategy.view_chart_permission):
                buttons.append([InlineKeyboardButton(_("Chart"), url=f"{WEBHOOK_URL}/plot/{obj.coin.ticker}?timeframe={obj.timeframe}&strategy={obj.indicator.value}")])

            if user.has_permission(Strategy.delete_permission):
                buttons.append([InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}")])

        return buttons

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if obj:
            if user.has_permission(Strategy.edit_permission):
                buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"strategy_edit_{obj.id}"))
        if user.has_permission(Strategy.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="strategy_new"))

        return buttons

    def additional_states(self):
        add_strategy_menu = AddStrategyMenu(self)
        edit_strategy_menu = EditStrategyMenu(self)

        return {self.States.ACTION: [add_strategy_menu.handler,
                                     edit_strategy_menu.handler,
                                     CallbackQueryHandler(self.activate_deactivate, pattern='^strategy_(activate|deactivate)$')]}

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("Strategy") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"

    def generate_missed_signals_job(self, context):
        job = context.job

        sf = job.context['sf']
        interrupt = job.context['interrupt']

        session = Database().sessionmaker()
        sf.generate_signals(interrupt=interrupt)
        session.close()

    def activate_deactivate(self, update, context):
        data = update.callback_query.data
        strategy = self.selected_object(context)
        if data == 'strategy_activate':
            strategy.activate()
            session = Database().sessionmaker()

            interrupt = Event()
            context.user_data[self.menu_name]['strategy_interrupter'] = interrupt

            if strategy.coin.account.provider is Provider.BITFINEX:
                sf = BFXSignalFormer(strategy, session=session)
            elif strategy.coin.account.provider is Provider.BINANCE:
                sf = BNCSignalFormer(strategy, session=session)
            else:
                self.send_message(context)
                return self.States.ACTION

            if sf.has_missing_signals():
                context.job_queue.run_once(self.generate_missed_signals_job, 0, context={'interrupt': interrupt, 'sf': sf})
            else:
                context.user_data[self.menu_name]['strategy_interrupter'] = None
            session.close()
            context.user_data[self.menu_name]['selected_object'] = 0
            context.bot.answer_callback_query(update.callback_query.id)
        elif data == "strategy_deactivate":
            if context.user_data[self.menu_name]['strategy_interrupter']:
                context.user_data[self.menu_name]['strategy_interrupter'].clear()
                if not context.user_data[self.menu_name]['strategy_interrupter'].wait(2):
                    context.bot.answer_callback_query(update.callback_query.id)
                    self.send_message(context)
                    return self.States.ACTION

            strategy = self.query_objects(context)[context.user_data[self.menu_name]['selected_object']]
            strategy.deactivate(DBSession)
            DBSession.commit()
            context.bot.answer_callback_query(update.callback_query.id)
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Strategy deleted")


class EditStrategyMenu(ArrowAddEditMenu):
    menu_name = 'strategy_edit_menu'
    model = Strategy
    show_field_selectors = False
    show_arrows = False
    reset_to_default = True

    @require_permission('edit_strategy_menu_access')
    def entry(self, update, context):
        delete_interface(context, 'help')
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        data = int(update.callback_query.data.replace("strategy_edit_", ""))
        context.user_data[self.menu_name]['strategy_id'] = data

        self._entry(update, context)
        self.load(context)
        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_object(self, context):
        return DBSession.query(Strategy).filter(Strategy.id == context.user_data[self.menu_name]['strategy_id']).first()

    def fields(self, context):
        _ = context.user_data['_']
        return [ArrowAddEditMenu.Field('trailing_stop_perc', _('Trailing stop percent'), validators.Number(min=0, max=100), required=False, default=None)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^strategy_edit_\d+$')]


class AddStrategyMenu(ArrowAddEditMenu):
    menu_name = 'strategy_add_menu'
    model = Strategy
    variants_edit_mode = True
    start_with_none_selected = True
    auto_switch_to_next_field_after_last_field = False

    @require_permission(Strategy.add_permission)
    def entry(self, update, context):
        self._entry(update, context)

        self.load(context)
        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_object(self, context):
        return None

    def fields(self, context):
        _ = context.user_data['_']
        indicators = [key.get_name() for key in StrategyType]
        return [ArrowAddEditMenu.Field('indicator', _('*Indicator'), formencode.Pipe(validators.String(), validators.OneOf(indicators)), required=True, variants=indicators),
                ArrowAddEditMenu.Field('slow', _('*Slow'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.ao.get_name()),
                ArrowAddEditMenu.Field('fast', _('*Fast'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.ao.get_name()),
                ArrowAddEditMenu.Field('smoothing', _('*Smoothing'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.ma2oc.get_name()),
                ArrowAddEditMenu.Field('longer', _('*Longer period'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.han_solo_jr.get_name()),
                ArrowAddEditMenu.Field('shorter', _('*Shorter period'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.han_solo_jr.get_name()),
                ArrowAddEditMenu.Field('longer', _('*Longer period'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.han_solo_sr.get_name()),
                ArrowAddEditMenu.Field('shorter', _('*Shorter period'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.han_solo_sr.get_name()),
                ArrowAddEditMenu.Field('longer', _('*Longer period'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.han_solo_nodir.get_name()),
                ArrowAddEditMenu.Field('shorter', _('*Shorter period'), validators.Int(min=1, max=10080), required=True, depend_field='indicator', depend_value=StrategyType.han_solo_nodir.get_name()),
                ArrowAddEditMenu.Field('timeframe', _('*Timeframe'), validators.Int(min=1, max=10080), required=True),
                ArrowAddEditMenu.Field('pyramiding', _('*Pyramiding'), validators.Int(min=1, max=1000), required=True, default=1),
                ArrowAddEditMenu.Field('trailing_stop_perc', _('Trailing stop percent'), validators.Number(min=0, max=100), required=False)
                ]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^(strategy_new)$')]

    def back(self, update, context):
        delete_interface(context)
        self.parent.update_objects(context)
        self.parent.send_message(context)
        update.callback_query.answer()
        return ConversationHandler.END

    def save(self, update, context):
        _ = context.user_data['_']

        if not self.check_fields(context):
            delete_interface(context)
            context.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        elif context.user_data[self.menu_name].get('longer', False) and context.user_data[self.menu_name].get('shorter', False) and context.user_data[self.menu_name]['longer'] < context.user_data[self.menu_name]['shorter']:
            delete_interface(context)
            context.bot.answer_callback_query(update.callback_query.id, text=_("Longer must be greater than shorter."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        elif context.user_data[self.menu_name].get('fast', False) and context.user_data[self.menu_name].get('slow', False) and context.user_data[self.menu_name]['slow'] < context.user_data[self.menu_name]['fast']:
            delete_interface(context)
            context.bot.answer_callback_query(update.callback_query.id, text=_("Slow must be greater than fast."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION

        delete_interface(context, 'variations_interface')

        self.update_object(context)

        return self.back(update, context)

    def save_object(self, obj, context, session=None):
        if context.user_data[self.menu_name]['indicator'] == StrategyType.ao.get_name():
            obj.indicator = StrategyType.ao
            obj.periods = [context.user_data[self.menu_name]['slow'], context.user_data[self.menu_name]['fast']]
        elif context.user_data[self.menu_name]['indicator'] == StrategyType.ma2oc.get_name():
            obj.indicator = StrategyType.ma2oc
            obj.periods = [context.user_data[self.menu_name]['smoothing']]
        elif context.user_data[self.menu_name]['indicator'] == StrategyType.han_solo_jr.get_name():
            obj.indicator = StrategyType.han_solo_jr
            obj.periods = [context.user_data[self.menu_name]['longer'], context.user_data[self.menu_name]['shorter']]
        elif context.user_data[self.menu_name]['indicator'] == StrategyType.han_solo_sr.get_name():
            obj.indicator = StrategyType.han_solo_sr
            obj.periods = [context.user_data[self.menu_name]['longer'], context.user_data[self.menu_name]['shorter']]
        elif context.user_data[self.menu_name]['indicator'] == StrategyType.han_solo_nodir.get_name():
            obj.indicator = StrategyType.han_solo_nodir
            obj.periods = [context.user_data[self.menu_name]['longer'], context.user_data[self.menu_name]['shorter']]

        obj.coin_id = context.user_data[self.parent.menu_name]['coin_id']
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def message_top_text(self, context):
        _ = context.user_data['_']
        coin = context.user_data[self.parent.parent.menu_name]['objects'][context.user_data[self.parent.parent.menu_name]['selected_object']]
        return "_____" + coin.account.name + '/' + coin.ticker + '/' + _("New strategy") + '_____\n'
