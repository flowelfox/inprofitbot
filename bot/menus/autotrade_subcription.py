from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import group_buttons, get_settings, generate_underscore
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface
from emoji import emojize
from formencode import Invalid, validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, PrefixHandler, MessageHandler, Filters

from models import DBSession, Account, SubscriptionType, Ticker, User, AutotradeSubscription
from settings import SETTINGS_FILE


class AutotradeSubscriptionMenu(BaseMenu):
    class States:
        ACTION = 1
        TXID = 2

    menu_name = 'autotrade_subscription_menu'

    def entry(self, update, context):
        user = context.user_data.get("user")
        if not user:
            user = self.prepare_user(User, update, context)

        _ = context.user_data['_'] = generate_underscore(user.language_code)

        delete_interface(context, 'help')
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if not user.has_permission(AutotradeSubscription.view_permission):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return self.States.ACTION

        data = update.callback_query.data
        if data.startswith('renew_autotrade_'):
            account = context.user_data[self.menu_name]['account'] = DBSession.query(Account).get(int(data.replace("renew_autotrade_", "")))
            context.user_data[self.menu_name]['notification'] = True
            update.effective_message.delete()
        else:
            account = context.user_data[self.menu_name]['account'] = DBSession.query(Account).get(context.user_data[self.parent.menu_name]['account_id'])
            context.user_data[self.menu_name]['notification'] = False

        if data == 'account_sub':
            context.user_data[self.menu_name]['action'] = "new"
            return self.ask_autotrade_type(update, context)
        else:
            context.user_data[self.menu_name]['action'] = "renew"
            if account.subscription.type is SubscriptionType.short_term:
                context.user_data[self.menu_name]['type'] = 'short'
            # elif account.subscription.type is SubscriptionType.mid_term:
            #     context.user_data[self.menu_name]['type'] = 'mid'

            return self.ask_autotrade_term(update, context)

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Please select autotrading strategy:")

        buttons = [[InlineKeyboardButton(_("Short term"), callback_data=f'type_short')],
                   [InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def ask_autotrade_type(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def set_autotrade_type(self, update, context):
        data = update.callback_query.data.replace("type_", "")

        context.user_data[self.menu_name]['type'] = data
        return self.ask_autotrade_term(update, context)

    def ask_autotrade_term(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        account = context.user_data[self.menu_name]['account']
        balance = account.get_usd_balance('margin')
        type = context.user_data[self.menu_name]['type']
        if type == 'short':
            if balance < 500 or balance > 6000:
                update.callback_query.answer(text=_("To buy short-term autotrade subscription you need to have 500-6000 USD on your account."), show_alert=True)
                if context.user_data[self.menu_name]['action'] == 'new':
                    return self.States.ACTION
                else:
                    return self.back(update, context)
        # elif type == 'mid':
        #     if balance < 1000:
        #         update.callback_query.answer(text=_("To buy mid-term autotrade subscription you need to have at least 1000 USD on your account."), show_alert=True)
        #         if context.user_data[self.menu_name]['action'] == 'new':
        #             return self.States.ACTION
        #         else:
        #             return self.back(update, context)
        else:
            update.callback_query.answer(text=_("Wrong subscription type"), show_alert=True)
            return self.back(update, context)

        buttons = []
        flat_buttons = []
        message_text = _("Please select subscription term:")
        # if context.user_data[self.menu_name]['type'] == 'mid':
        #     flat_buttons.append(InlineKeyboardButton(_("180 days"), callback_data='term_180'))
        #     flat_buttons.append(InlineKeyboardButton(_("360 days"), callback_data='term_360'))

        if context.user_data[self.menu_name]['type'] == 'short':
            flat_buttons.append(InlineKeyboardButton(_("30 days"), callback_data='term_30'))
            flat_buttons.append(InlineKeyboardButton(_("90 days"), callback_data='term_90'))
        if flat_buttons:
            buttons.extend(group_buttons(flat_buttons, 3))

        if context.user_data[self.menu_name]['notification']:
            buttons.append([InlineKeyboardButton(_("Main menu"), callback_data=f'start')])
        else:
            if context.user_data[self.menu_name]['action'] == 'new':
                buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_type')])
            else:
                buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_autotrade_term(self, update, context):
        data = int(update.callback_query.data.replace("term_", ""))

        context.user_data[self.menu_name]['term'] = data
        return self.ask_pay_currency(update, context)

    def ask_pay_currency(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        message_text = _("Please select payment currency:")
        buttons.append([InlineKeyboardButton(_("BTC"), callback_data=f'currency_btc')])
        buttons.append([InlineKeyboardButton(_("ETH"), callback_data=f'currency_eth')])
        buttons.append([InlineKeyboardButton(_("LTC"), callback_data=f'currency_ltc')])
        buttons.append([InlineKeyboardButton(_("USDT Tether (Omni)"), callback_data=f'currency_usdt')])

        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_term')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def set_pay_currency(self, update, context):
        data = update.callback_query.data.replace("currency_", "")

        context.user_data[self.menu_name]['currency'] = data.upper()
        self.payment_preview(context)
        return self.States.ACTION

    def payment_preview(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        account = context.user_data[self.menu_name]['account']
        currency = context.user_data[self.menu_name]['currency']
        settings = get_settings(SETTINGS_FILE)
        address = settings['addresses'].get(currency, _("Unknown"))

        commission = settings.get("autotrade_commission", 50)
        daily_profit = 0.5 if context.user_data[self.menu_name]['type'] == 'short' else 0.3
        usd_price = daily_profit * context.user_data[self.menu_name]['term'] / 100 * account.get_usd_balance('margin') * (commission / 100)

        if currency == "USDT":
            amount = usd_price
        else:
            ticker = DBSession.query(Ticker).filter(Ticker.symbol == f"{currency}USD").first()
            amount = usd_price / ticker.last_price if ticker else _("Unknown")

        context.user_data[self.menu_name]['amount'] = amount
        context.user_data[self.menu_name]['usd_price'] = usd_price
        buttons = []
        message_text = _("Please send {amount:.4f} {currency} ({usd_price:.0f} USD) to this wallet:\n\n<b>{currency}</b>: <code>{address}</code>").format(amount=amount, usd_price=usd_price, currency=currency, address=address)
        buttons.append([InlineKeyboardButton(_("I've sent {}").format(context.user_data[self.menu_name]['currency']), callback_data=f'payment_txid')])
        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_currency')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def ask_txid(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Please send txid, so we can track you transaction.")
        send_or_edit(context, chat_id=user.chat_id, text=message_text)
        return self.States.TXID

    def payment_finish(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        text = update.effective_message.text
        try:
            validator = validators.String()
            txid = validator.to_python(text)
            delete_user_message(update)
        except Invalid:
            send_or_edit(context, chat_id=user.chat_id, text=_("Wrong txid, please try again"))
            return self.States.TXID

        currency = context.user_data[self.menu_name]['currency']
        amount = context.user_data[self.menu_name]['amount']
        usd_price = context.user_data[self.menu_name]['usd_price']
        term = context.user_data[self.menu_name]['term']
        type = context.user_data[self.menu_name]['type']

        account = context.user_data[self.menu_name]['account']
        settings = get_settings(SETTINGS_FILE)
        channel_id = str(settings.get('account_subscription_report_channel_id', 0))
        if not channel_id.startswith('-100'):
            channel_id = "-100" + channel_id

        if account.subscription is not None and account.subscription.is_active:
            report_text = emojize(":cross_mark: ") + f"<a href=\"tg://user?id={user.chat_id}\">{user.get_name()}</a> renewed subscription <b>{type}-term {term} days.</b>\n\n"
        else:
            report_text = emojize(":cross_mark: ") + f"<a href=\"tg://user?id={user.chat_id}\">{user.get_name()}</a> bought subscription <b>{type}-term {term} days.</b>\n\n"

        report_text += f"<code>{amount:.4f}</code> <b>{currency}</b>\n"
        report_text += f"<code>{usd_price:.2f}</code> <b>USD</b>\n\n"
        report_text += f"TXID: <code>{txid}</code>"

        buttons = [[InlineKeyboardButton(emojize(":white_heavy_check_mark:"), callback_data=f"autotrade_a{account.id}_t{term}_y{type.lower()}_mark_checked"),
                    InlineKeyboardButton(emojize(":exclamation_question_mark:"), callback_data="autotrade_mark_wrong")]]
        context.bot.send_message(chat_id=channel_id, text=report_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        message_text = _("Thank you!\n\nWe will check your transaction and contact you soon.\nFor any support contact @inprofit_admin or @artemium.")
        if context.user_data[self.menu_name]['notification']:
            buttons = [[InlineKeyboardButton(_("Return to main menu"), callback_data=f'start')]]
        else:
            buttons = [[InlineKeyboardButton(_("Return to account"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def back(self, update, context):
        if update.callback_query:
            update.callback_query.answer()
        delete_interface(context, 'help')
        self.parent.send_message(context)
        return ConversationHandler.END

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('This is menu of autotrading subscription.\n{line}\n'
                         '1. You see the buttons with the choice of subscription type. Short-term works on the hourly chart.\n\n'
                         '2. Next you choose the term of the subscription.\n\n'
                         '3. Next you choose the cryptocurrency, which you\'d like to use to pay for the subscription.\n\n'
                         '4. You will see the subscription price in the amount of the chosen cryptocurrency and its dollar equivalent. The course for calculation takes from Bitfinex.\n\n'
                         '5. When you send the required amount, click on the payment confirmation button.'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)
        return self.States.ACTION

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern=r'account_sub'),
                                                    CallbackQueryHandler(self.entry, pattern=r'account_renew_sub'),
                                                    CallbackQueryHandler(self.entry, pattern=r'renew_autotrade_\d+')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               CallbackQueryHandler(self.set_autotrade_type, pattern=r'^type_\w+$'),
                                                               CallbackQueryHandler(self.set_autotrade_term, pattern=r'^term_\d+$'),
                                                               CallbackQueryHandler(self.set_pay_currency, pattern=r'^currency_\w+$'),
                                                               CallbackQueryHandler(self.ask_txid, pattern=r'^payment_txid$'),

                                                               CallbackQueryHandler(self.ask_autotrade_type, pattern=f'^back_to_type$'),
                                                               CallbackQueryHandler(self.ask_autotrade_term, pattern=f'^back_to_term$'),
                                                               CallbackQueryHandler(self.ask_pay_currency, pattern=f'^back_to_currency$'),
                                                               PrefixHandler('/', 'help', self.help)],
                                          self.States.TXID: [MessageHandler(Filters.text, self.payment_finish),
                                                             PrefixHandler('/', 'help', self.help)]
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
