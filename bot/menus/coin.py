import formencode
from botmanlib.menus.add_edit_menu.arrow_add_edit_menu import ArrowAddEditMenu
from botmanlib.menus.helpers import group_buttons, add_to_db, require_permission
from botmanlib.menus.list_menus.bunch_list_menu import BunchListMenu
from botmanlib.menus.list_menus.one_list_menu import OneListMenu
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, PrefixHandler

from bot.menus.strategy import StrategiesMenu
from models import DBSession, Coin, PositionType, Strategy, Position, Account, Provider
from settings import BITFINEX_AVAILABLE_COINS, BITFINEX_AVAILABLE_RIGHT_TICKERS, BINANCE_AVAILABLE_COINS, BINANCE_AVAILABLE_RIGHT_TICKERS


class CoinsBunchMenu(BunchListMenu):
    menu_name = 'coins_bunch_menu'
    search_key_param = 'ticker'
    objects_per_page = 6
    auto_hide_arrows = True

    @require_permission(Coin.list_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        return super(CoinsBunchMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(Coin).filter(Coin.account_id == context.user_data[self.parent.menu_name]['account_id']).order_by(Coin.left_ticker).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^display_all_coins$')]

    def object_buttons(self, context, obj):
        _ = context.user_data['_']
        buttons = []
        if obj:
            coin_buttons = []
            for ob in obj:
                coin_buttons.append(InlineKeyboardButton(ob.ticker, callback_data=f"coin_{ob.id}"))
            buttons.extend(group_buttons(coin_buttons))
        return buttons

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if user.has_permission(Coin.add_permission):
            buttons.append(InlineKeyboardButton(_("Add coin"), callback_data="coin_new"))
        return buttons

    def additional_states(self):
        add_coin_menu = AddCoinMenu(self, bot=self.bot)

        return {self.States.ACTION: [CallbackQueryHandler(self.back, pattern='^coin_\d+$'),
                                     add_coin_menu.handler,
                                     PrefixHandler('/', 'help', self.help),
                                     CallbackQueryHandler(self.parent.parent.parent.delete_help, pattern="^delete_help$"),
                                     ]}

    def message_text(self, context, page_objects):
        user = context.user_data['user']
        _ = context.user_data['_']

        account = self.parent.query_object(context)
        if page_objects:
            message_text = account.name + ' / ' + _("Coins") + "\n"
            message_text += f"{'─' * 15}\n"
            for coin in page_objects:
                active_strategy = coin.active_strategy(DBSession)
                message_text += coin.ticker
                if user.has_permission(Strategy.view_permission):
                    message_text += (("<code>" + ' ' + active_strategy.name + "</code>") if active_strategy else "") + '\n'
                else:
                    message_text += "\n"
        else:
            message_text = account.name + ' / ' + _("Coins") + "\n"
            message_text += f"{'─' * 15}\n"
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']

        message_text = f"{'─' * 15}\n"
        if max_page > 1:
            message_text += _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page)
        return message_text

    def back(self, update, context):
        update.callback_query.data = 'coins'
        context.update_queue.put(update)
        delete_interface(context, 'help')
        if update.callback_query:
            update.callback_query.answer()

        return ConversationHandler.END

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('Here you see the list of coins connected for autotrading on chosen account.\n{line}\n'
                         '1. The "<b>All coins</b>" button opens a list of coins connected to trading on the selected account.\n\n'
                         '2. The arrows move you through the coins. On each coin page you will see opened position on the coin, balance of the coin on exchange and margin wallets.\n\n'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)
        return self.States.ACTION


class CoinsMenu(OneListMenu):
    menu_name = 'coins_menu'
    search_key_param = 'ticker'
    model = Coin
    auto_hide_arrows = True

    @require_permission(Coin.view_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        self._load(context)
        data = update.callback_query.data
        if data.startswith('coin_'):
            coin_id = int(data.replace("coin_", ""))
            for idx, obj in enumerate(self.query_objects(context)):
                if obj.id == coin_id:
                    context.user_data[self.menu_name]['selected_object'] = idx

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Coin).filter(Coin.account_id == context.user_data[self.parent.menu_name]['account_id']).order_by(Coin.left_ticker).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^coins$'),
                CallbackQueryHandler(self.entry, pattern=r'^coin_\d+$')]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            message_text = obj.account.name + ' / ' + _("Coins") + "\n"
            message_text += f"{'─' * 15}\n"
            message_text += "<b>" + obj.ticker + "</b>" + '\n'
            if user.has_permission(Coin.view_deppart_permission):
                message_text += _("Deposit part:") + "<code>" + f" {obj.deposit_part:.3f}" + "</code>" + '\n'
            active_strategy = obj.active_strategy()
            if user.has_permission(Strategy.view_permission):
                message_text += _("Active strategy:") + "<code>" + ' ' + (active_strategy.name if active_strategy else "-") + "</code>" + '\n'
            if active_strategy:
                active_position = active_strategy.active_position()
            else:
                active_position = None
            message_text += _("Open position:") + "<code>" + ('\n' + str(active_position) if active_position else ' ' + "-") + "</code>" + '\n'
            message_text += _("Exchange balance:") + "<code>" + f" {obj.get_balance('exchange'):.5f} {obj.left_ticker}" + "</code>" + '\n'
            message_text += _("Margin balance:") + "<code>" + f" {obj.get_balance('margin'):.5f} {obj.left_ticker}" + "</code>" + '\n'
        else:
            account = self.parent.query_object(context)
            message_text = account.name + ' / ' + _("Coins") + "\n"
            message_text += f"{'─' * 15}\n"
            message_text += _("There is nothing to show") + '\n'

        return message_text

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if obj:
            if user.has_permission(Coin.list_permission):
                buttons.append([InlineKeyboardButton(_("All your coins"), callback_data="display_all_coins")])

            if user.has_permission(Strategy.view_permission):
                buttons.append([InlineKeyboardButton(_("Strategies"), callback_data=f"strategies_{obj.id}")])

            if user.has_permission(Coin.edit_deppart_permission):
                buttons.append([InlineKeyboardButton(_("Change dep. part"), callback_data=f"coin_edit_{obj.id}")])

            if user.has_permission(Coin.delete_permission):
                buttons.append([InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}")])

            active_strategy = obj.active_strategy()
            position = active_strategy.active_position() if active_strategy else None

            close_buttons = []
            if position and user.has_permission(Position.remove_permission):
                close_buttons.append(InlineKeyboardButton(_("Remove position"), callback_data="remove_position"))
            if position and position.type is PositionType.real and user.has_permission(Position.close_permission):
                close_buttons.append(InlineKeyboardButton(_("Close position"), callback_data="close_position"))

            if close_buttons:
                buttons.append(close_buttons)
            return buttons
        else:
            return None

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if user.has_permission(Coin.add_permission):
            buttons.append(InlineKeyboardButton(_("Add coin"), callback_data="coin_new"))
        return buttons

    def display_all_coins(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def additional_states(self):
        add_coin_menu = AddCoinMenu(self, bot=self.bot)
        strategies_menu = StrategiesMenu(self, bot=self.bot)

        return {self.States.ACTION: [CallbackQueryHandler(self.display_all_coins, pattern='^display_all_coins$'),
                                     CallbackQueryHandler(self.close_position_ask, pattern='^close_position$'),
                                     CallbackQueryHandler(self.close_position, pattern='^close_position_(yes|no)$'),
                                     CallbackQueryHandler(self.remove_position_ask, pattern='^remove_position$'),
                                     CallbackQueryHandler(self.remove_position, pattern='^remove_position_(yes|no)$'),
                                     add_coin_menu.handler,
                                     strategies_menu.handler,
                                     PrefixHandler('/', 'help', self.help),
                                     CallbackQueryHandler(self.parent.parent.parent.delete_help, pattern="^delete_help$"),
                                     ]}

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']

        message_text = f"{'─' * 15}\n"
        if max_page > 1:
            message_text += _("Coin") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page)
        return message_text

    @require_permission(Position.remove_permission)
    def remove_position_ask(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']
        buttons = [[InlineKeyboardButton(_("Yes"), callback_data="remove_position_yes"),
                    InlineKeyboardButton(_("No"), callback_data="remove_position_no")]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Are you sure about removing position?"), reply_markup=InlineKeyboardMarkup(buttons))
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def remove_position(self, update, context):
        _ = context.user_data['_']
        data = update.callback_query.data
        if data == 'remove_position_yes':
            objects = self._get_objects(context)
            selected_coin = objects[context.user_data[self.menu_name]['selected_object']]
            coin_active_strategy = selected_coin.active_strategy(session=DBSession)
            if coin_active_strategy:
                strategy_active_position = coin_active_strategy.active_position(session=DBSession)
                if strategy_active_position:
                    strategy_active_position.close_position(session=DBSession)

            update.callback_query.answer(text=_("{ticker} position removed.").format(ticker=selected_coin.ticker), show_alert=True)
            self.update_objects(context)
        else:
            update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    @require_permission(Position.close_permission)
    def close_position_ask(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']
        buttons = [[InlineKeyboardButton(_("Yes"), callback_data="close_position_yes"),
                    InlineKeyboardButton(_("No"), callback_data="close_position_no")]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Are you sure about closing position?"), reply_markup=InlineKeyboardMarkup(buttons))
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def close_position(self, update, context):
        _ = context.user_data['_']
        data = update.callback_query.data
        if data == 'close_position_yes':
            objects = self._get_objects(context)
            selected_coin = objects[context.user_data[self.menu_name]['selected_object']]
            coin_active_strategy = selected_coin.active_strategy(session=DBSession)
            if coin_active_strategy:
                strategy_active_position = coin_active_strategy.active_position(session=DBSession)
                if strategy_active_position:
                    strategy_active_position.close_market(session=DBSession)

            update.callback_query.answer(text=_("{ticker} position closed.").format(ticker=selected_coin.ticker), show_alert=True)
            self.update_objects(context)
        else:
            update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Coin deleted")

    def back(self, update, context):
        delete_interface(context, 'help')
        return super(CoinsMenu, self).back(update, context)

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('Here you see the list of coins connected for autotrading on chosen account.\n{line}\n'
                         '1. The "<b>All coins</b>" button opens a list of coins connected to trading on the selected account.\n\n'
                         '2. The arrows move you through the coins. On each coin page you will see opened position on the coin, balance of the coin on exchange and margin wallets.\n\n'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)
        return self.States.ACTION


class AddCoinMenu(ArrowAddEditMenu):
    menu_name = 'coin_add_menu'
    model = Coin
    show_arrows = False

    @require_permission(Coin.add_permission)
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        context.user_data[self.menu_name]['max_deposit_part'] = None

        data = update.callback_query.data
        if data.startswith('coin_edit_'):
            context.user_data[self.menu_name]['coin_id'] = int(data.replace("coin_edit_", ''))
        else:
            context.user_data[self.menu_name]['coin_id'] = None

        self._entry(update, context)
        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        if context.user_data[self.menu_name]['coin_id']:
            return DBSession.query(Coin).filter(Coin.id == context.user_data[self.menu_name]['coin_id']).first()
        else:
            return None

    def load(self, context):
        context.user_data[self.menu_name]['max_deposit_part'] = 1 - sum([x[0] for x in DBSession.query(Coin.deposit_part).filter(Coin.account_id == context.user_data[self.parent.parent.menu_name]['account_id']).all()])
        if self.action(context) is self.Action.EDIT:
            obj = self.query_object(context)
            context.user_data[self.menu_name]['max_deposit_part'] += obj.deposit_part
        context.user_data[self.menu_name]['max_deposit_part'] = round(context.user_data[self.menu_name]['max_deposit_part'], 8)
        return self._load(context)

    def fields(self, context):
        _ = context.user_data['_']

        if self.action(context) is self.Action.ADD:
            account = DBSession.query(Account).get(context.user_data[self.parent.parent.menu_name]['account_id'])
            if account.provider is Provider.BITFINEX:
                left_tickers = BITFINEX_AVAILABLE_COINS
                right_tickers = BITFINEX_AVAILABLE_RIGHT_TICKERS
            elif account.provider is Provider.BINANCE:
                left_tickers = BINANCE_AVAILABLE_COINS
                right_tickers = BINANCE_AVAILABLE_RIGHT_TICKERS
            else:
                left_tickers = []
                right_tickers = []

            return [ArrowAddEditMenu.Field('deposit_part', _('*Deposit part') + f" (≤{context.user_data[self.menu_name]['max_deposit_part']:.3f})",
                                           validators.Number(min=0.01, max=context.user_data[self.menu_name]['max_deposit_part']), required=True),
                    ArrowAddEditMenu.Field('left_ticker', _('*Left ticker'), formencode.Pipe(validators.String(), validators.OneOf(left_tickers)), required=True, variants=left_tickers, before_validate=str.upper),
                    ArrowAddEditMenu.Field('right_ticker', _('*Right ticker'), formencode.Pipe(validators.String(), validators.OneOf(right_tickers)), required=True, variants=right_tickers, before_validate=str.upper)]

        else:
            return [ArrowAddEditMenu.Field('deposit_part',
                                           _('*Deposit part') + f" (≤{context.user_data[self.menu_name]['max_deposit_part']:.3f})",
                                           validators.Number(min=0.01, max=context.user_data[self.menu_name]['max_deposit_part']),
                                           required=True)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^(coin_new|coin_edit_\d+)$')]

    def save(self, update, context):
        _ = context.user_data['_']

        if not self.check_fields(context):
            delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION

        if self.action(context) is self.Action.ADD:
            account = self.parent.parent.query_object(context)
            existing_coin = DBSession.query(Coin).filter(Coin.account_id == account.id).filter(Coin.left_ticker == context.user_data[self.menu_name]['left_ticker']).filter(Coin.right_ticker == context.user_data[self.menu_name]['right_ticker']).first()
            if existing_coin:
                context.bot.answer_callback_query(update.callback_query.id, text=_("That pair already exists."), show_alert=True)
                return self.States.ACTION

        delete_interface(context, 'variations_interface')

        self.bot.answer_callback_query(update.callback_query.id, text=self.after_save_text(context), show_alert=False)
        self.update_object(context)

        return self.back(update, context)

    def save_object(self, obj, context, session=None):
        obj.account_id = context.user_data[self.parent.parent.menu_name]['account_id']
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def message_top_text(self, context):
        _ = context.user_data['_']
        account = self.parent.parent.query_object(context)
        if self.action(context) is self.Action.ADD:
            return "_____" + account.name + '/' + _("Add coin") + "_____\n"
        else:
            obj = self.query_object(context)
            return "_____" + account.name + '/' + obj.ticker + "_____\n"

    def back(self, update, context):
        context.user_data[self.menu_name]['max_deposit_part'] = None
        delete_interface(context, 'help')
        return super(AddCoinMenu, self).back(update, context)

    def fallbacks(self):
        return [PrefixHandler('/', 'help', self.parent.parent.parent.parent.missing_help)] + super(AddCoinMenu, self).fallbacks()
