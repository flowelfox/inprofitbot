from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import require_permission, group_buttons, get_settings
from botmanlib.messages import delete_interface, send_or_edit, delete_user_message
from emoji import emojize
from formencode import validators, Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters, PrefixHandler

from models import DBSession, Ticker, SignalSubscriptionType
from settings import SETTINGS_FILE


class SignalsSubscriptionMenu(BaseMenu):
    class States:
        ACTION = 1
        TXID = 2

    menu_name = 'signals_subscription_menu'

    @require_permission('subscribe_to_signals_menu_access')
    def entry(self, update, context):
        delete_interface(context, 'help')
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        update.callback_query.answer()

        user = context.user_data['user']
        if user.subscription and user.subscription.is_active:
            if user.subscription.type is SignalSubscriptionType.bitfinex_short_term:
                context.user_data[self.menu_name]['type'] = "bitfinexshortterm"
            elif user.subscription.type is SignalSubscriptionType.binance_short_term:
                context.user_data[self.menu_name]['type'] = "binanceshortterm"
            else:
                context.user_data[self.menu_name]['type'] = "none"

            return self.ask_term(update, context)

        return self.ask_sub_type(update, context)

    def ask_sub_type(self, update, context):
        buttons = []
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        if user.subscription and user.subscription.is_active:
            message_text = _("You subscription active until") + f" <code>{user.subscription.expire_date.strftime('%d.%m.%Y')}</code>\n"
        else:
            message_text = _("You don't have signals subscription") + "\n"

        message_text += _("Please select subscription type:")

        flat_buttons.append(InlineKeyboardButton(_("Bitfinex short term"), callback_data='type_bitfinexshortterm'))
        flat_buttons.append(InlineKeyboardButton(_("Binance short term"), callback_data='type_binanceshortterm'))

        if flat_buttons:
            buttons.extend(group_buttons(flat_buttons, 3))

        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_sub_type(self, update, context):
        data = update.callback_query.data.replace("type_", "")
        context.user_data[self.menu_name]['type'] = data
        return self.ask_term(update, context)

    def ask_term(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        flat_buttons = []

        flat_buttons.append(InlineKeyboardButton(_("90 days"), callback_data='term_90'))
        flat_buttons.append(InlineKeyboardButton(_("180 days"), callback_data='term_180'))
        flat_buttons.append(InlineKeyboardButton(_("360 days"), callback_data='term_360'))

        if flat_buttons:
            buttons.extend(group_buttons(flat_buttons, 3))

        if user.subscription and user.subscription.is_active:
            message_text = _("You subscription active until") + f" <code>{user.subscription.expire_date.strftime('%d.%m.%Y')}</code>\n"
            buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')])
        else:
            message_text = _("You don't have signals subscription") + "\n"
            buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_type')])

        message_text += _("Please select subscription term:")

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_signals_term(self, update, context):
        data = int(update.callback_query.data.replace("term_", ""))

        context.user_data[self.menu_name]['term'] = data
        return self.ask_pay_currency(update, context)

    def ask_pay_currency(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        message_text = _("Please select payment currency:")
        buttons.append([InlineKeyboardButton(_("BTC"), callback_data=f'currency_btc')])
        buttons.append([InlineKeyboardButton(_("ETH"), callback_data=f'currency_eth')])
        buttons.append([InlineKeyboardButton(_("LTC"), callback_data=f'currency_ltc')])
        buttons.append([InlineKeyboardButton(_("USDT Tether (Omni)"), callback_data=f'currency_usdt')])

        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_term')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def set_pay_currency(self, update, context):
        data = update.callback_query.data.replace("currency_", "")

        context.user_data[self.menu_name]['currency'] = data.upper()
        self.payment_preview(update, context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def payment_preview(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        currency = context.user_data[self.menu_name]['currency']
        term = context.user_data[self.menu_name]['term']
        settings = get_settings(SETTINGS_FILE)
        address = settings['addresses'].get(currency, _("Unknown"))
        prices = settings.get("signals_prices")
        usd_price = prices.get(str(term), 0)

        if currency == "USDT":
            amount = usd_price
        else:
            ticker = DBSession.query(Ticker).filter(Ticker.symbol == f"{currency}USD").first()
            amount = usd_price / ticker.last_price if ticker else _("Unknown")

        context.user_data[self.menu_name]['amount'] = amount
        context.user_data[self.menu_name]['usd_price'] = usd_price
        buttons = []
        message_text = _("Please send {amount:.4f} {currency} ({usd_price:.0f} USD) to this wallet:\n\n<b>{currency}</b>: <code>{address}</code>").format(amount=amount, usd_price=usd_price, currency=currency, address=address)
        buttons.append([InlineKeyboardButton(_("I've sent {}").format(context.user_data[self.menu_name]['currency']), callback_data=f'payment_txid')])
        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_currency')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def ask_txid(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Please send txid, so we can track you transaction.")
        send_or_edit(context, chat_id=user.chat_id, text=message_text)
        return self.States.TXID

    def payment_finish(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        text = update.effective_message.text
        try:
            validator = validators.String()
            txid = validator.to_python(text)
            delete_user_message(update)
        except Invalid:
            send_or_edit(context, chat_id=user.chat_id, text=_("Wrong txid, please try again"))
            return self.States.TXID

        currency = context.user_data[self.menu_name]['currency']
        amount = context.user_data[self.menu_name]['amount']
        usd_price = context.user_data[self.menu_name]['usd_price']
        term = context.user_data[self.menu_name]['term']
        subtype = context.user_data[self.menu_name]['type']

        settings = get_settings(SETTINGS_FILE)
        channel_id = str(settings.get('signal_subscription_report_channel_id', 0))
        if not channel_id.startswith('-100'):
            channel_id = "-100" + channel_id
        report_text = emojize(":cross_mark: ") + f"<a href=\"tg://user?id={user.chat_id}\">{user.get_name()}</a> bought signals subscription for <b>{term} days.</b>\n\n"

        report_text += f"<code>{amount:.4f}</code> <b>{currency}</b>\n"
        report_text += f"<code>{usd_price:.2f}</code> <b>USD</b>\n\n"
        report_text += f"TXID: <code>{txid}</code>"

        buttons = [[InlineKeyboardButton(emojize(":white_heavy_check_mark:"), callback_data=f"signals_t{term}_u{user.id}_S{subtype}_mark_checked"),
                    InlineKeyboardButton(emojize(":exclamation_question_mark:"), callback_data=f"signals_mark_wrong")]]
        context.bot.send_message(chat_id=channel_id, text=report_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        message_text = _("Thank you!\n\nWe will check your transaction and contact you soon.\nFor any support contact @inprofit_admin or @artemium.")
        buttons = [[InlineKeyboardButton(_("Return to main menu"), callback_data=f'back_{self.menu_name}')]]

        delete_interface(context, 'help')
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return ConversationHandler.END

    def back(self, update, context):
        self.parent.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('In this is menu of subscribing to signals and analytics.\n{line}\n'
                         '1. Choose the subscription term.\n\n'
                         '2. Next, choose a cryptocurrency, which is convenient for you to pay for subscription.\n\n'
                         '3. You will see the subscription price in the amount of the chosen cryptocurrency and the dollar equivalent. The course for calculation is taken realtime from Bitfinex.\n\n'
                         '4. When you send the required amount, click on the payment confirmation button.'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)
        return self.States.ACTION

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='signals_sub')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               CallbackQueryHandler(self.set_signals_term, pattern=r'^term_\d+$'),
                                                               CallbackQueryHandler(self.set_sub_type, pattern=r'^type_\w+$'),
                                                               CallbackQueryHandler(self.set_pay_currency, pattern=r'^currency_\w+$'),
                                                               CallbackQueryHandler(self.ask_txid, pattern=r'^payment_txid$'),
                                                               CallbackQueryHandler(self.ask_term, pattern=r'^back_to_term$'),
                                                               CallbackQueryHandler(self.ask_sub_type, pattern=r'^back_to_type$'),
                                                               CallbackQueryHandler(self.ask_pay_currency, pattern=r'^back_to_currency$'),
                                                               PrefixHandler('/', 'help', self.help)],
                                          self.States.TXID: [MessageHandler(Filters.text, self.payment_finish),
                                                             PrefixHandler('/', 'help', self.help)]
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
