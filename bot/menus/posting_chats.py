import enum

from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, require_permission
from botmanlib.messages import delete_user_message, send_or_edit, _send_telegram_text
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler, MessageHandler, Filters

from models import DBSession, PostingChat


class PostingChatsMenu(OneListMenu):
    menu_name = "posting_chat_menu"
    search_key_param = 'name'
    add_delete_button = True
    parse_mode = "MARKDOWN"

    @require_permission(PostingChat.view_permission)
    def entry(self, update, context):
        return super(PostingChatsMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(PostingChat).filter(PostingChat.account_id == context.user_data[self.parent.menu_name]['account_id']).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^posting_chats$')]

    def message_text(self, context, obj):
        _ = context.user_data['_']

        if obj:
            code_s = "`"
            code_e = "`"

            message_text = _("Name: ") + code_s + obj.name + code_e + '\n'
            message_text += _("Chat ID: ") + code_s + str(obj.chat_id) + code_e + '\n'
            message_text += _("Send positions ratio: ") + code_s + (_("Yes") if obj.send_positions_ratio else _("No")) + code_e + '\n'
            message_text += _("Allow hold signals: ") + code_s + (_("Yes") if obj.allow_hold_signals else _("No")) + code_e + '\n'
            message_text += _("Buy message: ") + code_s + (_("Added") if obj.buy_message else _("Empty")) + code_e + '\n'
            message_text += _("Sell message: ") + code_s + (_("Added") if obj.sell_message else _("Empty")) + code_e + '\n'
            if obj.allow_hold_signals:
                message_text += _("Hold message: ") + code_s + (_("Added") if obj.hold_message else _("Empty")) + code_e + '\n'
            message_text += _("Stop position message: ") + code_s + (_("Added") if obj.stop_position_message else _("Empty")) + code_e + '\n'
            message_text += _("Open position message: ") + code_s + (_("Added") if obj.open_position_message else _("Empty")) + code_e + '\n'
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def center_buttons(self, context, obj=None):
        _ = context.user_data['_']

        buttons = []
        if obj is not None:
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f'posting_chat_edit_{obj.id}'))
        buttons.append(InlineKeyboardButton(_("Add"), callback_data='add_posting_chat'))
        return buttons

    def additional_states(self):
        add_edit_posting_chat_menu = AddEditPostingChatMenu(self)
        return {self.States.ACTION: [add_edit_posting_chat_menu.handler]}

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END


class AddEditPostingChatMenu(ArrowAddEditMenu):
    menu_name = 'posting_chat_addedit_menu'
    model = PostingChat
    auto_switch_to_next_field_after_last_field = False
    parse_mode = "MARKDOWN"

    class States(enum.Enum):
        ACTION = 1
        CHANNEL_ID = 2

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        data = update.callback_query.data
        if data.startswith('posting_chat_edit_'):
            context.user_data[self.menu_name]['posting_chat_id'] = int(data.replace("posting_chat_edit_", ''))
        else:
            context.user_data[self.menu_name]['posting_chat_id'] = None

        self._entry(update, context)
        self.load(context)

        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        if context.user_data[self.menu_name]['posting_chat_id']:
            return DBSession.query(PostingChat).filter(PostingChat.id == context.user_data[self.menu_name]['posting_chat_id']).first()
        else:
            return None

    def fields(self, context):
        _ = context.user_data['_']

        return [ArrowAddEditMenu.Field('name', _('\*Name'), validators.String(), required=True),
                ArrowAddEditMenu.Field('chat_id', _('\*Chat ID'), validators.Number(min=0), callback=self.channel_id, required=True, auto_switch=False),
                ArrowAddEditMenu.Field('send_positions_ratio', _('Send position ratio'), validators.StringBool()),
                ArrowAddEditMenu.Field('allow_hold_signals', _('\*Allow hold signals'), validators.StringBool(), required=True, default=False),
                ArrowAddEditMenu.Field('buy_message', _('\*Buy message'), validators.String(), required=True),
                ArrowAddEditMenu.Field('sell_message', _('\*Sell message'), validators.String(), required=True),
                ArrowAddEditMenu.Field('hold_message', _('\*Hold message'), validators.String(), required=True, depend_field='allow_hold_signals', depend_value=True),
                ArrowAddEditMenu.Field('stop_position_message', _('Stop position message'), validators.String(), required=False, default=None),
                ArrowAddEditMenu.Field('open_position_message', _('Open position message'), validators.String(), required=False, default=None),
                ]

    def channel_id(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_to_channels')]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Please forward any message to me from channel you want to connect."), reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.CHANNEL_ID

    def set_channel_id(self, update, context):
        _ = context.user_data['_']

        if not update.effective_message.forward_from_chat:
            delete_user_message(update)
            return self.States.CHANNEL_ID
        else:
            delete_user_message(update)
            context.user_data[self.menu_name]['chat_id'] = update.effective_message.forward_from_chat.id

            message = _send_telegram_text(bot=context.bot, chat_id=context.user_data[self.menu_name]['chat_id'], text=_("Chat connected to InProfit Bot\n\n<i>This message will be deleted after 5 seconds</i>"), parse_mode="HTML")
            context.job_queue.run_once(self.delete_chat_connected_message, 5, context={'chat_id': context.user_data[self.menu_name]['chat_id'], "message_id": message.message_id})
            return self.next_field(update, context)

    def delete_chat_connected_message(self, context):
        chat_id = context.job.context['chat_id']
        message_id = context.job.context['message_id']

        context.bot.delete_message(chat_id=chat_id, message_id=message_id)

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^(add_posting_chat|posting_chat_edit_\d+)$')]

    def message_top_text(self, context):
        _ = context.user_data['_']
        if self.action(context) is self.Action.ADD:
            return "\_\_\_\_\_" + _("New posting chat") + '\_\_\_\_\_\n'
        else:
            return "\_\_\_\_\_" + _("Edit posting chat") + '\_\_\_\_\_\n'

    def save_object(self, obj, context, session=None):
        obj.account_id = context.user_data[self.parent.parent.menu_name]['account_id']
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def additional_states(self):
        return {self.States.CHANNEL_ID: [CallbackQueryHandler(self.prev_field, pattern='^back_to_channels$'),
                                         MessageHandler(Filters.all, self.set_channel_id),
                                         ]
                }
