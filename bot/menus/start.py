import enum
import logging

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import group_buttons, require_permission, add_to_db, generate_underscore, generate_regex_handlers
from botmanlib.messages import delete_interface, send_or_edit, delete_user_message
from emoji import emojize
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from telegram.ext import ConversationHandler, MessageHandler, Filters, CallbackQueryHandler, PrefixHandler

from bot.menus.account import AccountsMenu
from bot.menus.signals_subscription import SignalsSubscriptionMenu
from bot.menus.withdraw import WithdrawMenu
from models import User, DBSession

logger = logging.getLogger(__name__)


class StartMenu(BaseMenu):
    menu_name = 'start_menu'

    class States(enum.Enum):
        ACTION = 1
        LANGUAGE = 2
        ABOUT = 3
        REFERRALS = 4

    def entry(self, update, context):
        pass
        # \/ \/ \/ \/ \/ \/ \/ USER INIT \/ \/ \/ \/ \/ \/ \/
        user = DBSession.query(User).filter(User.chat_id == update.effective_user.id).first()
        tuser = update.effective_user
        if not user:
            user = User(language_code=tuser.language_code)
            user.init_permissions()

        if context.args and (not user or (user and user.disabled)):
            query = DBSession.query(User).filter(User.chat_id == context.args[0]).filter(User.chat_id != update.effective_user.id)
            user_who_invite = query.first()
            if user_who_invite is not None:
                user.referrer = user_who_invite
                if user_who_invite.referrer_id == user.id:
                    user_who_invite.referrer_id = None
                    DBSession.add(user_who_invite)

        user.chat_id = tuser.id
        user.first_name = tuser.first_name
        user.last_name = tuser.last_name
        user.username = tuser.username
        user.activate()

        context.user_data['user'] = user
        _ = context.user_data['_'] = generate_underscore(user.language_code)

        DBSession.add(user)
        DBSession.commit()
        # /\ /\ /\ /\ /\ /\ /\ USER INIT /\ /\ /\ /\ /\ /\ /\

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if update.message and 'start' in update.message.text:
            delete_interface(context, 'main')
            delete_interface(context)

        if update.callback_query and update.callback_query.data == 'start':
            delete_interface(context)

        if not user.has_permission('start_menu_access'):
            context.bot.send_message(chat_id=user.chat_id, text=_("You were restricted from using bot"))
            return StartMenu.States.ACTION

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []

        flat_buttons.append(InlineKeyboardButton(_("Accounts"), callback_data='accounts'))
        flat_buttons.append(InlineKeyboardButton(_("Subscription to signals"), callback_data='signals_sub'))
        flat_buttons.append(InlineKeyboardButton(_("Referrals"), callback_data='referrals'))
        flat_buttons.append(InlineKeyboardButton(_("About"), callback_data='about'))
        flat_buttons.append(InlineKeyboardButton(_("Language"), callback_data='language'))
        main_message_text = _("This is first Beta release. Everything works ok, but if you find any bugs, please report to @artemium or @inprofit_admin.\n\n"
                              "Almost every menu has a description. When you enter somewhere you need help, use /help command.")
        message_text = _("Choose what you'd like to do:")

        if "main" not in context.user_data['interfaces']:
            send_or_edit(context, "main", chat_id=user.chat_id, text=main_message_text, reply_markup=ReplyKeyboardMarkup([[_("Main menu")]], resize_keyboard=True))

        delete_interface(context, 'help')
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(group_buttons(flat_buttons)))

    @require_permission('change_language_menu_access')
    def ask_language(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(emojize(":United_Kingdom: ") + "English", callback_data='language_en')],
                   [InlineKeyboardButton(emojize(":Russia: ") + "Русский", callback_data='language_ru')],
                   [InlineKeyboardButton(emojize(":Ukraine: ") + "Українська", callback_data='language_uk')]]
        delete_interface(context, 'help')
        send_or_edit(context, chat_id=user.chat_id, text=_("Select a language:"), reply_markup=InlineKeyboardMarkup(buttons))
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.LANGUAGE

    def set_language(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        value = update.callback_query.data.replace("language_", "")

        user.language_code = value

        add_to_db(user)
        context.user_data['_'] = _ = generate_underscore(user.language_code)

        main_message_text = _("This is first Beta release. Everything works ok, but if you find any bugs, please report to @artemium or @inprofit_admin.")
        delete_interface(context, 'main')
        delete_interface(context)
        send_or_edit(context, "main", chat_id=user.chat_id, text=main_message_text, reply_markup=ReplyKeyboardMarkup([[_("Main menu")]], resize_keyboard=True))

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def referals(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Referrals") + '\n'
        message_text += f"{'─' * 15}\n"
        if user.referrer:
            message_text += _("You are invited by") + f' <a href="tg://user?id={user.referrer.chat_id}">{user.referrer.get_name()}</a>.\n\n'
        else:
            message_text += _("You are invited by Nobody.") + '\n\n'

        message_text += _("Invited users") + f" ({len(user.referrals)})"
        if user.referrals:
            message_text += ":\n"
            for idx, referral in enumerate(user.referrals):
                message_text += f'{idx + 1}. <a href="tg://user?id={referral.chat_id}">' + referral.get_name() + "</a>" + '\n'
        message_text += "\n"
        message_text += _("Balance:") + f" ${user.balance:.2f}\n\n"

        message_text += _("Invite link: ") + f"\n{context.bot.link}?start={user.chat_id}"

        buttons = []
        if user.balance > 0:
            buttons.append([InlineKeyboardButton(_("Withdraw"), callback_data=f'withdraw')])

        if user.language_code == 'ru':
            buttons.append([InlineKeyboardButton(_("Details"), url="https://teletype.in/@inprofit/Hy5ODAYMr")])
        elif user.language_code == 'uk':
            buttons.append([InlineKeyboardButton(_("Details"), url="https://teletype.in/@inprofit/B1N6uAFGB")])
        else:
            buttons.append([InlineKeyboardButton(_("Details"), url="https://teletype.in/@inprofit/r1VNuCKzr")])

        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_menu_{self.menu_name}')])

        delete_interface(context, 'help')
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), disable_web_page_preview=True, parse_mode="HTML")
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.REFERRALS

    def about(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("We created profitable algorithms for crypto-trading.\n\n"
                         "You can work manually by following InProfit signals or connect your Bitfinex account to autotrading.\n\n"
                         "Short-term bot earns from +15% monthly.\n\n"
                         "You can read more about us by clicking \"About InProfit\" button.")

        buttons = []
        if user.language_code == 'ru':
            buttons.append([InlineKeyboardButton(_("About InProfit"), url="https://teletype.in/@inprofit/HJsUB0KzB")])
        elif user.language_code == 'uk':
            buttons.append([InlineKeyboardButton(_("About InProfit"), url="https://teletype.in/@inprofit/H1FFVAKfr")])
        else:
            buttons.append([InlineKeyboardButton(_("About InProfit"), url="https://teletype.in/@inprofit/Byt7rRtzr")])

        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_menu_{self.menu_name}')])
        delete_interface(context, 'help')
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ABOUT

    def goto_admin(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def goto_disable(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('This is the main menu of InProfit Bot.\n{line}\n'
                         '1. The "<b>Accounts</b>" button opens the menu of viewing and adding Bitfinex accounts.\n\n'
                         '2. The "<b>Subscribe to signals</b>" button opens the menu of viewing and activating subscription to the &lt;Private&gt; channel and chat.\n\n'
                         '3. The "<b>Referral program</b>" button opens the menu for viewing your referral statistics. There is also a button to request the withdrawal of funds earned through invitations.\n\n'
                         '4. The "<b>About project</b>" button opens the InProfit info menu.\n\n'
                         '5. The "<b>Language</b>" button opens the menu of changing the interface language. Currently available: Russian, English, Ukrainian.'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)

    def referrals_help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('The referral program menu shows your referral link, a list of invited users and earnings from their subscriptions.\n{line}\n'
                         'To invite a friend, send him your referral link. You will receive 20% of the money paid by your invite.\n\n'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)

    def delete_help(self, update, context):
        delete_interface(context, 'help')

    def missing_help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('There is no help for this menu. If you need help here, write @inprofit_admin.')

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)

    def get_handler(self):
        self.accounts_menu = AccountsMenu(self)
        signals_subscription_menu = SignalsSubscriptionMenu(self)
        withdraw_menu = WithdrawMenu(self)

        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'start', self.entry),
                                                    CallbackQueryHandler(self.entry, pattern='^start$')],
                                      states={
                                          self.States.ACTION: generate_regex_handlers("Main menu", lambda update, context: delete_user_message(update)) +
                                                              [PrefixHandler('/', 'disable', self.goto_disable),
                                                               CallbackQueryHandler(self.ask_language, pattern="^language$"),
                                                               CallbackQueryHandler(self.about, pattern='^about$'),
                                                               CallbackQueryHandler(self.referals, pattern='^referrals$'),
                                                               self.accounts_menu.handler,
                                                               signals_subscription_menu.handler,
                                                               withdraw_menu.handler,
                                                               PrefixHandler('/', 'help', self.help)],
                                          self.States.LANGUAGE: [CallbackQueryHandler(self.set_language, pattern="^language_\w\w$")],
                                          self.States.REFERRALS: [PrefixHandler('/', 'help', self.referrals_help)],
                                      },
                                      fallbacks=[PrefixHandler('/', 'help', self.missing_help),
                                                 CallbackQueryHandler(self.delete_help, pattern="^delete_help$"),
                                                 CallbackQueryHandler(self.back_to_menu, pattern=f'^back_to_menu_{self.menu_name}$'),
                                                 PrefixHandler('/', 'admin', self.goto_admin),
                                                 MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
