import logging

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import delete_interface, send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, MessageHandler, Filters, CallbackQueryHandler, PrefixHandler

from models import User, DBSession, Account

logger = logging.getLogger(__name__)


class DisableMenu(BaseMenu):
    menu_name = 'disable_menu'

    @require_permission(Account.disable_permission)
    def entry(self, update, context):
        user = self.prepare_user(User, update, context)
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        delete_interface(context)
        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Yes"), callback_data='disable_yes'),
                    InlineKeyboardButton(_("No"), callback_data=f'back_{self.menu_name}')]]
        message_text = _("This will delete all your accounts and active subscriptions.\n\n"
                         "Are you sure that you want to disable your account?")

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def ask_confirm(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Totally sure"), callback_data='disable_totally_yes')],
                   [InlineKeyboardButton(_("No, I changed my mind"), callback_data=f'back_{self.menu_name}')]]
        message_text = _("Are you totally sure that you want to disable your account?.\n\n"
                         "THIS OPERATION CANNOT BE UNDONE!")

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ACTION

    def disable_account(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        for account in user.accounts:
            DBSession.delete(account)
        user.deactivate()
        DBSession.add(user)
        DBSession.commit()

        message_text = _("You disabled your account.\n"
                         "You can send me /start command to enter bot or use referral link to become someones referral.")
        send_or_edit(context, chat_id=user.chat_id, text=message_text)
        return ConversationHandler.END

    def goto_admin(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def goto_start(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def back(self, update, context):
        user = context.user_data['user']

        self.fake_callback_update(user, "start")
        return ConversationHandler.END

    def delete_help(self, update, context):
        delete_interface(context, 'help')

    def missing_help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('There is no help for this menu. If you need help here, write @inprofit_admin.')

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)

    def get_handler(self):
        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'disable', self.entry)],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.ask_confirm, pattern='^disable_yes$'),
                                                               CallbackQueryHandler(self.disable_account, pattern='^disable_totally_yes$')],
                                      },
                                      fallbacks=[PrefixHandler('/', 'admin', self.goto_admin),
                                                 PrefixHandler('/', 'start', self.goto_start),
                                                 PrefixHandler('/', 'help', self.missing_help),
                                                 CallbackQueryHandler(self.delete_help, pattern="^delete_help$"),
                                                 MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
