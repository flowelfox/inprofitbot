from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.menus.redy_to_use.instant_distribution import InstantDistributionMenu
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from models import User


class DistributionsMenu(BaseMenu):
    menu_name = 'distribution_menu'

    @require_permission("distribution_menu_access")
    def entry(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Instant distribution"), callback_data='instant_distribution')],
                   [InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Choose distribution type:"), reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        instant_distribution_menu = InProfitInstantDistributionMenu(User, self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^distribution$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               instant_distribution_menu.handler],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class InProfitInstantDistributionMenu(InstantDistributionMenu):
    menu_name = 'instant_distribution'

    def available_buttons(self, context):
        _ = context.user_data['_']
        buttons = [{"data": "start", "name": _("Return user to start menu")},
                   {"data": "close_message", "name": _("Close distribution message")}]
        return buttons

    @require_permission("instant_distribution_menu_access")
    def entry(self, update, context):
        return super(InProfitInstantDistributionMenu, self).entry(update, context)
