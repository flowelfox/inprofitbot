import datetime
from threading import Event

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import unknown_command, to_state, group_buttons, prepare_user
from botmanlib.menus.redy_to_use.permissions import PermissionsMenu
from botmanlib.messages import send_or_edit, delete_interface
from sqlalchemy import func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, Filters, MessageHandler, CallbackQueryHandler, PrefixHandler

from bot.menus.admin.accounts import AccountsInfoMenu
from bot.menus.admin.distributions import DistributionsMenu
from models import User, Permission, DBSession


class AdminMenu(BaseMenu):
    menu_name = 'admin_menu'

    def entry(self, update, context):
        user = prepare_user(User, update, context)

        if not user.has_permission('admin_menu_access'):
            return ConversationHandler.END

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if 'updating' not in context.user_data[self.menu_name]:
            context.user_data[self.menu_name]['updating'] = Event()

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        flat_buttons = []

        if user.has_permission('distribution_menu_access'):
            flat_buttons.append(InlineKeyboardButton(_("Distribution"), callback_data='distribution'))

        if user.has_permission('permissions_menu_access'):
            flat_buttons.append(InlineKeyboardButton(_("Permissions"), callback_data='permissions'))
        if user.has_permission('admin_accounts_info_menu_access'):
            flat_buttons.append(InlineKeyboardButton(_("Accounts info"), callback_data='accounts_info'))
        if user.has_permission('statistics_menu_access'):
            flat_buttons.append(InlineKeyboardButton(_("Statistics"), callback_data='statistics'))

        if flat_buttons:
            buttons = group_buttons(flat_buttons, 1)
        buttons.append([InlineKeyboardButton(_("Back to main menu"), callback_data='goto_start')])

        send_or_edit(context, chat_id=user.chat_id, text=_("Admin menu:"), reply_markup=InlineKeyboardMarkup(buttons))

    def goto_start(self, update, context):
        delete_interface(context)
        update.callback_query.data = 'start'
        context.update_queue.put(update)
        return ConversationHandler.END

    def statistics(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        all_users = DBSession.query(func.count(User.id)).first()[0]
        day_joined = DBSession.query(func.count(User.id)).filter(User.join_date > datetime.datetime.utcnow() - datetime.timedelta(days=1)).first()[0]
        week_joined = DBSession.query(func.count(User.id)).filter(User.join_date > datetime.datetime.utcnow() - datetime.timedelta(days=7)).first()[0]
        month_joined = DBSession.query(func.count(User.id)).filter(User.join_date > datetime.datetime.utcnow() - datetime.timedelta(days=30)).first()[0]
        block_users = DBSession.query(func.count(User.id)).filter(User.active == False).first()[0]

        message_text = _("Users in bot:") + f" {all_users}\n"
        message_text += _("Joined in past 1 day:") + f" {day_joined}\n"
        message_text += _("Joined in past 7 days:") + f" {week_joined}\n"
        message_text += _("Joined in past 30 days:") + f" {month_joined}\n"
        message_text += _("Users that stop or block bot:") + f" {block_users}\n"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.ACTION

    def get_handler(self):
        accounts_info_menu = AccountsInfoMenu(self)
        permissions_menu = PermissionsMenu(User, Permission, parent=self)
        distribution_menu = DistributionsMenu(self)

        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'admin', self.entry)],
                                      states={
                                          self.States.ACTION: [accounts_info_menu.handler,
                                                               permissions_menu.handler,
                                                               distribution_menu.handler,
                                                               CallbackQueryHandler(self.statistics, pattern='^statistics$'),
                                                               CallbackQueryHandler(self.goto_start, pattern='^goto_start$'),
                                                               CallbackQueryHandler(self.entry, pattern='^back$'),
                                                               MessageHandler(Filters.regex('^/start$'), self.goto_start),
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION))],
                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
