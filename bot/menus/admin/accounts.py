import datetime

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission, add_to_db, get_settings
from botmanlib.messages import send_or_edit
from sqlalchemy import func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler

from backend.helpers import close_all_positions
from models import DBSession, AutotradeSubscription, Account, SubscriptionType, Coin, Strategy, StrategyType, Position, Order
from settings import SETTINGS_FILE


class AccountsInfoMenu(OneListMenu):
    menu_name = 'admin_accounts_info_menu'
    auto_hide_arrows = True

    @require_permission('admin_accounts_info_menu_access')
    def entry(self, update, context):
        self._load(context)

        context.user_data[self.menu_name]['show_valid'] = True
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        query = DBSession.query(Account).join(Account.user)
        if context.user_data[self.menu_name]['show_valid']:
            query = query.filter(Account.is_valid == True)
        else:
            query = query.filter(Account.is_valid == False)

        return query.all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^accounts_info$')]

    def back(self, update, context):
        self.parent.entry(update, context)
        return ConversationHandler.END

    def active_positions(self, context):
        account = self.selected_object(context)

        active_positions = DBSession.query(func.count(Position.id)) \
            .join(Position.order) \
            .filter(Order.account_id == account.id) \
            .filter(Position.is_active == True) \
            .scalar()
        return active_positions

    def message_text(self, context, account):
        _ = context.user_data['_']

        if account:
            message_text = _("User: ") + f'<a href="tg://user?id={account.user.chat_id}">' + account.user.name + "</a>" + '\n'
            message_text += _("Account name: ") + "<code>" + account.name + "</code>" + '\n'
            message_text += _("Is valid: ") + "<code>" + (_("Yes") if account.is_valid else _("No")) + "</code>" + '\n'
            message_text += _("Email: ") + "<code>" + (account.email if account.email else _("Unknown")) + "</code>" + '\n'
            message_text += _("USD Margin balance: ") + "<code>" + str(account.get_usd_balance('margin')) + "</code>" + '\n'
            message_text += _("Active positions: ") + "<code>" + str(self.active_positions(context)) + "</code>" + '\n'
            if account.subscription is not None and account.subscription.is_active:
                sub_type = _("Unknown")
                if account.subscription.type is SubscriptionType.short_term:
                    sub_type = _("Short-term autotrading")
                # elif account.subscription.type is SubscriptionType.mid_term:
                #    sub_type = _("Mid-term autotrading")
                message_text += _("{type} subscription active until <code>{date}</code> ({days} days left)").format(type=sub_type, date=account.subscription.expire_date.strftime("%d.%m.%Y"), days=account.subscription.days) + '\n'
            else:
                message_text += _("Subscription inactive") + '\n'

        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def center_buttons(self, context, obj=None):
        _ = context.user_data['_']
        buttons = []
        if obj:
            if obj.subscription is not None and obj.subscription.is_active:
                buttons.append(InlineKeyboardButton(_("Renew"), callback_data='renew_subscription'))
            else:
                buttons.append(InlineKeyboardButton(_("Add subscription"), callback_data='add_subscription'))
        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if obj:
            if obj.subscription is not None and obj.subscription.is_active:
                buttons.append([InlineKeyboardButton(_("Disable subscription"), callback_data='disable_subscription')])
            if user.has_permission('allow_close_account_positions') and self.active_positions(context) > 0:
                buttons.append([InlineKeyboardButton(_("Close account positions"), callback_data='close_account_positions')])
        if context.user_data[self.menu_name]['show_valid']:
            buttons.append([InlineKeyboardButton(_("Show only invalid"), callback_data='show_invalid')])
        else:
            buttons.append([InlineKeyboardButton(_("Show only valid"), callback_data='show_valid')])

        return buttons

    def toggle_show_valid(self, update, context):
        data = update.callback_query.data

        if data == 'show_valid':
            context.user_data[self.menu_name]['show_valid'] = True
        else:
            context.user_data[self.menu_name]['show_valid'] = False

        self.update_objects(context)
        self.send_message(context)

        return self.States.ACTION

    def back_to_accounts(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    @require_permission("allow_close_account_positions")
    def ask_close_account_positions(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']
        buttons = [[InlineKeyboardButton(_("Yes"), callback_data="close_account_positions_yes"),
                    InlineKeyboardButton(_("No"), callback_data="back_to_accounts")]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Are you sure about closing all account positions?"), reply_markup=InlineKeyboardMarkup(buttons))
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def close_account_positions(self, update, context):
        _ = context.user_data['_']
        data = update.callback_query.data

        if data == 'close_account_positions_yes':
            close_all_positions(self.selected_object(context).id)
            context.bot.answer_callback_query(update.callback_query.id, text=_("All account positions closed."), show_alert=True)
            self.update_objects(context)

        self.send_message(context)
        return self.States.ACTION

    def ask_subscription_type(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Short term autotrading"), callback_data='sub_type_short')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_accounts')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Select autotrading type:"), reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.ACTION

    def set_subscription_type(self, update, context):
        context.user_data[self.menu_name]['type'] = update.callback_query.data.replace("sub_type_", "")
        self.ask_term(context)
        return self.States.ACTION

    def ask_renew_subscription(self, update, context):
        context.user_data[self.menu_name]['type'] = 'renew'
        self.ask_term(context)
        return self.States.ACTION

    def renew_subscription(self, update, context):
        _ = context.user_data['_']

        term = int(update.callback_query.data.replace("renew_sub_term_", ""))
        account = self.selected_object(context)
        account.subscription.expire_date += datetime.timedelta(days=term)
        if not add_to_db(account.subscription):
            return self.conv_fallback(context)

        context.bot.answer_callback_query(update.callback_query.id, text=_("Subscription renewed"), show_alert=True)
        self.send_message(context)
        return self.States.ACTION

    def ask_term(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if context.user_data[self.menu_name]['type'] == 'renew':
            renew = True
            account = self.selected_object(context)
            if account.subscription.type is SubscriptionType.short_term:
                sub_type = 'short'
            elif account.subscription.type is SubscriptionType.mid_term:
                sub_type = 'mid'
            else:
                sub_type = None
        else:
            renew = False
            sub_type = context.user_data[self.menu_name]['type']

        if sub_type == 'short':
            buttons.append([InlineKeyboardButton(_("30 days"), callback_data=("renew_" if renew else "") + 'sub_term_30')])
            buttons.append([InlineKeyboardButton(_("90 days"), callback_data=("renew_" if renew else "") + 'sub_term_90')])

        # if sub_type == 'another_one':
        #     buttons.append([InlineKeyboardButton(_("180 days"), callback_data=("renew_" if renew else "") + 'sub_term_180')])
        #     buttons.append([InlineKeyboardButton(_("360 days"), callback_data=("renew_" if renew else "") + 'sub_term_360')])

        buttons.append([InlineKeyboardButton(_("Back"), callback_data='back_to_type')])

        send_or_edit(context, chat_id=user.chat_id, text=_("Select subscription term:"), reply_markup=InlineKeyboardMarkup(buttons))

    def disable_subscription(self, update, context):
        account = self.selected_object(context)
        subscription = account.subscription
        if subscription:
            subscription.expire_date = datetime.datetime.utcnow()
            DBSession.add(subscription)
            DBSession.commit()

        if account.coins:
            # deactivate strategies
            for coin in account.coins:
                for strategy in coin.strategies:
                    strategy.deactivate(DBSession)
                DBSession.delete(coin)
        DBSession.commit()

        self.send_message(context)
        return self.States.ACTION

    def add_subscription(self, update, context):
        _ = context.user_data['_']

        term = int(update.callback_query.data.replace("sub_term_", ""))
        type = context.user_data[self.menu_name]['type']
        account = self.selected_object(context)

        if (account.subscription and not account.subscription.is_active) or (account.subscription is None):
            # create subscription
            subscription = AutotradeSubscription()
            subscription.expire_date = datetime.datetime.utcnow() + datetime.timedelta(days=term)
            subscription.account = account
            if type == 'short':
                subscription.type = SubscriptionType.short_term
            # elif type == 'another_one':
            # subscription.type = SubscriptionType.another_one
            DBSession.add(subscription)

        DBSession.commit()
        if account.coins:
            # renew strategies
            for coin in account.coins:
                for strategy in coin.strategies:
                    strategy.activate(DBSession)
            DBSession.commit()

        else:
            # create strategies
            if type == 'short':
                for coin_pair in [("BAB", "USD"), ("BTC", "USD"), ("EOS", "USD"), ("ETH", "USD"), ("LTC", "USD"), ("XRP", "USD")]:
                    coin = Coin(left_ticker=coin_pair[0], right_ticker=coin_pair[1], deposit_part=0.16, account=account)
                    strategy = Strategy(coin=coin, active=True)
                    if coin_pair[0] == "XRP":
                        strategy.indicator = StrategyType.han_solo_nodir
                        strategy.periods = (144, 55)
                        strategy.timeframe = 60
                        strategy.pyramiding = 1
                        strategy.trailing_stop_perc = 3
                    else:
                        strategy.indicator = StrategyType.han_solo_nodir
                        strategy.periods = (96, 44)
                        strategy.timeframe = 60
                        strategy.pyramiding = 1
                        strategy.trailing_stop_perc = 3
                    DBSession.add(strategy)
                    DBSession.add(coin)

        # referrer balance update
        if account.user.referrer:
            settings = get_settings(SETTINGS_FILE)
            commission = settings.get("autotrade_commission", 50)
            referrer_reward = settings["referrer_reward"]
            daily_profit = 0.5 if type == 'short' else 0.3
            usd_price = daily_profit * term / 100 * account.get_usd_balance('margin') * (commission / 100)
            account.user.referrer.balance += usd_price * (referrer_reward / 100)
            DBSession.add(account.user.referrer)

        DBSession.commit()
        context.bot.answer_callback_query(update.callback_query.id, text=_("Subscription added"), show_alert=True)
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.ask_subscription_type, pattern='^add_subscription$'),
                                     CallbackQueryHandler(self.ask_renew_subscription, pattern='^renew_subscription$'),
                                     CallbackQueryHandler(self.renew_subscription, pattern=r'^renew_sub_term_\d+$'),
                                     CallbackQueryHandler(self.ask_subscription_type, pattern='^back_to_type$'),
                                     CallbackQueryHandler(self.disable_subscription, pattern='^disable_subscription$'),
                                     CallbackQueryHandler(self.ask_close_account_positions, pattern='^close_account_positions$'),
                                     CallbackQueryHandler(self.close_account_positions, pattern='^close_account_positions_yes$'),
                                     CallbackQueryHandler(self.set_subscription_type, pattern='^sub_type_(short|another_one)$'),
                                     CallbackQueryHandler(self.toggle_show_valid, pattern='^show_(valid|invalid)$'),
                                     CallbackQueryHandler(self.add_subscription, pattern=r'^sub_term_\d+$'),
                                     CallbackQueryHandler(self.back_to_accounts, pattern='^back_to_accounts$'),
                                     ]}
