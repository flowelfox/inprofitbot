from botmanlib.menus.add_edit_menu.arrow_add_edit_menu import ArrowAddEditMenu
from botmanlib.menus.helpers import require_permission, get_settings
from botmanlib.messages import delete_interface, send_or_edit
from emoji import emojize
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

from models import DBSession, Ticker
from settings import SETTINGS_FILE


class WithdrawMenu(ArrowAddEditMenu):
    menu_name = 'withdraw_menu'
    model = dict
    force_action = ArrowAddEditMenu.Action.ADD
    show_field_selectors = False

    @require_permission('withdraw_menu_access')
    def entry(self, update, context):
        return super(WithdrawMenu, self).entry(update, context)

    def query_object(self, context):
        return None

    def save_button(self, context):
        return None

    def bottom_buttons(self, context):
        _ = context.user_data['_']
        if context.user_data[self.menu_name]['currency'] and context.user_data[self.menu_name]['address']:
            return [[InlineKeyboardButton(_("Send withdraw request"), callback_data=f"save_{self.menu_name}")]]

    def reset_button(self, context):
        return None

    def fields(self, context):
        _ = context.user_data['_']
        return [ArrowAddEditMenu.Field('currency', _('*Currency'), validators.String(), variants=["BTC", "ETH", "LTC", "USDT Tether (Omni)"], required=True),
                ArrowAddEditMenu.Field('address', _('*Address'), validators.String(), required=True)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^withdraw$')]

    def save(self, update, context):
        _ = context.user_data['_']

        if not self.check_fields(context):
            delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        delete_interface(context, 'variations_interface')

        self.bot.answer_callback_query(update.callback_query.id, text=self.after_save_text(context), show_alert=False)
        self.update_object(context)
        return self.States.ACTION

    def save_object(self, obj, context, session=None):
        user = context.user_data['user']
        _ = context.user_data['_']

        settings = get_settings(SETTINGS_FILE)
        channel_id = str(settings['withdraw_report_channel_id'])
        if not channel_id.startswith('-100'):
            channel_id = "-100" + channel_id
        currency = obj['currency']
        address = obj['address']

        if currency == "USDT":
            amount = user.balance
        else:
            ticker = DBSession.query(Ticker).filter(Ticker.symbol == f"{currency}USD").first()
            amount = user.balance / ticker.last_price if ticker else _("Unknown")

        report_text = emojize(":cross_mark: ") + f"<a href=\"tg://user?id={user.chat_id}\">{user.get_name()}</a> wants to withdraw money from referrals.\n\n"

        report_text += f"<b>{currency}</b>: <code>{address}</code>\n"
        report_text += f"<code>{amount:.4f}</code> <b>{currency}</b>\n"
        report_text += f"<code>{user.balance:.2f}</code> <b>USD</b>\n"

        buttons = [[InlineKeyboardButton(emojize(":white_heavy_check_mark:"), callback_data=f"withdraw_u{user.id}_mark_checked"),
                    InlineKeyboardButton(emojize(":exclamation_question_mark:"), callback_data=f"withdraw_mark_wrong")]]
        context.bot.send_message(chat_id=channel_id, text=report_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        message_text = _("Thank you!\n\nWe withdraw money manually, so it may take a while. As soon as transaction reaches blockchain we will contact you.\n\nFor any support contact @inprofit_admin or @artemium.")
        buttons = [[InlineKeyboardButton(_("Return to main menu"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
