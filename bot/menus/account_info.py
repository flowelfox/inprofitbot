import enum

import emoji
from botmanlib.menus import BaseMenu
from botmanlib.menus.add_edit_menu.arrow_add_edit_menu import ArrowAddEditMenu
from botmanlib.menus.helpers import remove_from_db, to_state, inline_placeholder, require_permission
from botmanlib.messages import delete_interface, send_or_edit, delete_user_message
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, MessageHandler, CallbackQueryHandler, Filters, PrefixHandler

from bot.menus.autotrade_subcription import AutotradeSubscriptionMenu
from bot.menus.coin import CoinsMenu, CoinsBunchMenu
from bot.menus.positions import PositionsMenu
from bot.menus.posting_chats import PostingChatsMenu
from bot.menus.yield_curve import YieldCurveMenu
from models import DBSession, Account, SubscriptionType, Wallet, Coin, Position, AutotradeSubscription, PostingChat


class AccountInfoMenu(BaseMenu):
    menu_name = 'account_info_menu'

    class States(enum.Enum):
        ACTION = 1
        DELETE_CONFIRMATION = 2

    @require_permission(Account.view_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        data = int(update.callback_query.data.replace("account_", ""))

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['account_id'] = data

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_object(self, context):
        return DBSession.query(Account).filter(Account.id == context.user_data[self.menu_name]['account_id']).first()

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        account = self.query_object(context)
        buttons = [[]]
        if account:
            code_s = "<code>"
            code_e = "</code>"
            i_s = "<i>"
            i_e = "</i>"

            message_text = account.name + "\n"
            message_text += f"{'─' * 15}\n"
            message_text += _("Exchange balance:") + code_s + " {:.2f} USD".format(account.get_usd_balance('exchange')) + code_e + '\n'
            message_text += _("Margin balance:") + code_s + " {:.2f} USD".format(account.get_usd_balance('margin')) + code_e + '\n'
            if account.subscription is not None and account.subscription.is_active:
                sub_type = _("Unknown")
                if account.subscription.type is SubscriptionType.short_term:
                    sub_type = _("Short-term autotrading")
                elif account.subscription.type is SubscriptionType.mid_term:
                    sub_type = _("Mid-term autotrading")

                message_text += "\n" + _("{type} subscription active until <code>{date}</code> ({days} days left)").format(type=sub_type, date=account.subscription.expire_date.strftime("%d.%m.%Y"), days=account.subscription.days) + '\n'
            else:
                message_text += _("Subscription inactive") + '\n'
            if account.is_valid:
                buttons.append([InlineKeyboardButton(_("Exchange balance info"), callback_data='exchange_balance'),
                                InlineKeyboardButton(_("Margin balance info"), callback_data='margin_balance')])
                buttons.append([InlineKeyboardButton(_("Yield curve"), callback_data='yield_curve')])
            else:
                message_text += '\n' + i_s + _("API is not valid.") + i_e + '\n'

            key_permissions = account.get_bitfinex_key_permissions()
            if key_permissions.get('withdraw', False) and (key_permissions['withdraw'].get('read', False) or key_permissions['withdraw'].get('write', False)):
                message_text += '\n' + i_s + _("You give withdraw permissions to account key. It's not needed for bot.") + i_e + '\n'

            if user.has_permission(AutotradeSubscription.view_permission) and account.is_valid:
                if account.subscription is None or (account.subscription and not account.subscription.is_active):
                    buttons.append([InlineKeyboardButton(_("Autotrade subscription"), callback_data=f'account_sub')])
                else:
                    buttons.append([InlineKeyboardButton(_("Renew subscription"), callback_data=f'account_renew_sub')])

            if (user.has_permission(Position.view_permission) and account.is_valid) or user.has_permission(Position.oninvalid_permission):
                buttons[-1].append(InlineKeyboardButton(_("Positions"), callback_data='positions'))

            coins_trade_buttons = []
            if (user.has_permission(Coin.view_permission) and account.is_valid) or user.has_permission(Coin.oninvalid_permission):
                coins_trade_buttons.append(InlineKeyboardButton(_("Coins"), callback_data='coins'))
            if user.has_permission('trade_menu_access'):
                coins_trade_buttons.append(InlineKeyboardButton(_("Trade"), callback_data="trade"))
            if coins_trade_buttons:
                buttons.append(coins_trade_buttons)

            if user.has_permission(PostingChat.view_permission):
                buttons.append([InlineKeyboardButton(_("Posting chats"), callback_data='posting_chats')])

            if user.has_permission(Account.edit_permission):
                buttons.append([InlineKeyboardButton(_("Rename account"), callback_data=f'account_edit_{account.id}'),
                                InlineKeyboardButton(_("Delete"), callback_data=f'account_delete')])
            else:
                buttons.append([InlineKeyboardButton(_("Delete"), callback_data=f'account_delete')])

        else:
            message_text = _("There is nothing to show") + '\n'

        buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')])
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def exchange_balance(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        account = self.query_object(context)
        wallets = DBSession.query(Wallet).distinct(Wallet.currency).filter(Wallet.type == 'exchange').filter(Wallet.account == account).all()
        message_text = account.name + '/' + _("Exchange balance") + "\n"
        message_text += f"{'─' * 15}\n"
        for wallet in wallets:
            message_text += f"<b>{wallet.currency}</b>: {wallet.get_balance():.5f} (<code>${wallet.get_usd_balance():.2f}</code>)\n"
        message_text += "\n"

        message_text += _("Total: ") + f" <code>{account.get_usd_balance('exchange'):.2f} USD</code>" + '\n'
        buttons = [[InlineKeyboardButton(_("Back"), callback_data=f'back_to_menu_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def margin_balance(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        account = self.query_object(context)
        wallets = DBSession.query(Wallet).distinct(Wallet.currency).filter(Wallet.type == 'margin').filter(Wallet.account == account).all()
        message_text = account.name + '/' + _("Margin balance") + "\n"
        message_text += f"{'─' * 15}\n"
        for wallet in wallets:
            message_text += f"<b>{wallet.currency}</b>: {wallet.get_balance():.5f} (<code>${wallet.get_usd_balance():.2f}</code>)\n"
        message_text += "\n"

        message_text += _("Total: ") + f" <code>{account.get_usd_balance('margin'):.2f} USD</code>" + '\n'
        buttons = [[InlineKeyboardButton(_("Back"), callback_data=f'back_to_menu_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def delete_account(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        exm_mark_emo = emoji.emojize(':white_heavy_check_mark:')
        no_entry_emo = emoji.emojize(':no_entry:')
        buttons.append([InlineKeyboardButton(exm_mark_emo + ' ' + _("Yes") + ' ' + exm_mark_emo, callback_data='delete_yes')])
        buttons.append([InlineKeyboardButton(no_entry_emo + ' ' + _("No") + ' ' + no_entry_emo, callback_data='delete_no')])

        delete_interface(context, 'help')
        send_or_edit(context, chat_id=user.chat_id, text=_('Are you sure you want to delete this account?'), reply_markup=InlineKeyboardMarkup(buttons))
        update.callback_query.answer()

        return AccountInfoMenu.States.DELETE_CONFIRMATION

    def delete_account_confirmation(self, update, context):
        _ = context.user_data['_']

        data = update.callback_query.data

        if data == 'delete_yes':
            account = self.query_object(context)
            if not remove_from_db(account, DBSession):
                return self.conv_fallback(context)

            update.callback_query.answer(text=_("Account deleted"), show_alert=True)

            self.parent.entry(update, context)

            return ConversationHandler.END
        elif data == 'delete_no':
            update.callback_query.answer()
            self.send_message(context)
            return AccountInfoMenu.States.ACTION
        else:
            return self.conv_fallback(context)

    def help(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _('This is menu of viewing your account.\n{line}\n'
                         '1. The "<b>Exchange Wallet</b>" and "<b>Margin Wallet</b>" buttons open the menu to view more detailed information about your exchange wallets of the selected account.\n\n'
                         '2. The "<b>Yield Curve</b>" button shows the changes of the balance on the Margin wallet of the selected account.\n\n'
                         '3. The "<b>Subscription</b>" button opens the automatic trading subscription menu in the selected account.\n\n'
                         '4. The "<b>Positions</b>" button opens the list of current positions opened by the bot on the selected account.\n\n'
                         '5. The "<b>Rename account</b>" button allows you to change the name of the selected account.\n\n'
                         '6. The "<b>Delete</b>" button deletes the selected account.'
                         ).format(line=f"{'─' * 15}\n")

        buttons = [[InlineKeyboardButton(_("Close"), callback_data=f'delete_help')]]
        send_or_edit(context, 'help', chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        delete_user_message(update)
        return self.States.ACTION

    def back(self, update, context):
        if self.parent.menu_name not in context.user_data:
            self.fake_callback_update(context.user_data['user'], "accounts")
        else:
            self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        edit_account_menu = RenameAccountMenu(self)
        coins_menu = CoinsMenu(self)
        coins_bunch_menu = CoinsBunchMenu(self)
        positions_menu = PositionsMenu(self)
        posting_groups_menu = PostingChatsMenu(self)
        yield_curve_menu = YieldCurveMenu(self)
        self.account_sub = AutotradeSubscriptionMenu(self)

        return ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^account_\d+$")],
                                   states={self.States.ACTION: [CallbackQueryHandler(self.delete_account, pattern="^account_delete$"),
                                                                CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                                CallbackQueryHandler(self.back_to_menu, pattern=f'^back_to_menu_{self.menu_name}$'),
                                                                CallbackQueryHandler(inline_placeholder(self.States.ACTION, "Section still in development"), pattern='^trade$'),
                                                                CallbackQueryHandler(self.exchange_balance, pattern='^exchange_balance$'),
                                                                CallbackQueryHandler(self.margin_balance, pattern='^margin_balance$'),
                                                                positions_menu.handler,
                                                                edit_account_menu.handler,
                                                                coins_menu.handler,
                                                                coins_bunch_menu.handler,
                                                                posting_groups_menu.handler,
                                                                yield_curve_menu.handler,
                                                                self.account_sub.handler,
                                                                PrefixHandler('/', 'help', self.help),
                                                                CallbackQueryHandler(self.parent.parent.delete_help, pattern="^delete_help$"),
                                                                MessageHandler(Filters.all, to_state(self.States.ACTION))],
                                           self.States.DELETE_CONFIRMATION: [CallbackQueryHandler(self.delete_account_confirmation, pattern="^delete_(yes|no)$")]},
                                   fallbacks=[PrefixHandler('/', 'help', self.parent.parent.missing_help),
                                              MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                   allow_reentry=True)


class RenameAccountMenu(ArrowAddEditMenu):
    menu_name = 'account_rename_menu'
    model = Account

    @require_permission(Account.edit_permission)
    def entry(self, update, context):
        delete_interface(context, 'help')
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        data = int(update.callback_query.data.replace("account_edit_", ""))
        context.user_data[self.menu_name]['account_id'] = data

        self._entry(update, context)
        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        return DBSession.query(Account).filter(Account.id == context.user_data[self.menu_name]['account_id']).first()

    def fields(self, context):
        _ = context.user_data['_']
        return [self.Field("name", _("*Name"), validators.String(), required=True)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^account_edit_\d+$')]

    def back(self, update, context):
        delete_interface(context, 'help')
        return super(RenameAccountMenu, self).back(update, context)


    def fallbacks(self):
        return [PrefixHandler('/', 'help', self.parent.parent.parent.missing_help)] + super(RenameAccountMenu, self).fallbacks()
