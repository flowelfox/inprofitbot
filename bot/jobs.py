import datetime
import logging

from botmanlib.menus.helpers import generate_underscore, get_settings
from botmanlib.models import Database
from emoji import emojize
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import Unauthorized, BadRequest

from models import User, SignalsSubscription, Account, AutotradeSubscription, SubscriptionType
from settings import SETTINGS_FILE

logger = logging.getLogger(__name__)


def start_check_blocked_job(job_queue):
    stop_check_blocked_job(job_queue)
    job_queue.run_repeating(check_blocked_job, 60 * 120, first=0, name="check_blocked_job")
    logging.info("Check blocked users job started...")


def stop_check_blocked_job(job_queue):
    for job in job_queue.get_jobs_by_name("check_blocked_job"):
        job.schedule_removal()
        logging.info("Check blocked users job stopped...")


def check_blocked_job(context):
    session = Database().DBSession

    users = session.query(User).all()
    for user in users:
        try:
            context.bot.send_chat_action(user.chat_id, 'typing')
        except (Unauthorized, BadRequest):
            user.deactivate()
            session.add(user)

    session.commit()


def start_signals_expire_job(job_queue):
    stop_signals_expire_job(job_queue)
    job_queue.run_daily(signals_expire_job, time=datetime.time(hour=11), name="signals_expire_job")
    logging.info("Check signals subscription expire job started...")


def stop_signals_expire_job(job_queue):
    for job in job_queue.get_jobs_by_name("signals_expire_job"):
        job.schedule_removal()
        logging.info("Check signals subscription expire job stopped...")


def signals_expire_job(context):
    session = Database().sessionmaker()
    now = datetime.datetime.utcnow()
    base_query = session.query(User).join(User.subscription).filter(User.is_active == True)
    users_to_notify_1 = base_query.filter(SignalsSubscription.expire_date >= now + datetime.timedelta(days=1)).filter(SignalsSubscription.expire_date < now + datetime.timedelta(days=2)).all()
    users_to_notify_3 = base_query.filter(SignalsSubscription.expire_date >= now + datetime.timedelta(days=3)).filter(SignalsSubscription.expire_date < now + datetime.timedelta(days=4)).all()
    users_to_notify_7 = base_query.filter(SignalsSubscription.expire_date >= now + datetime.timedelta(days=7)).filter(SignalsSubscription.expire_date < now + datetime.timedelta(days=8)).all()
    users_expired = base_query.filter(SignalsSubscription.expire_date <= now).filter(SignalsSubscription.expire_date > now - datetime.timedelta(days=1)).all()

    settings = get_settings(SETTINGS_FILE)
    channel_id = str(settings.get('signal_subscription_report_channel_id', 0))
    if not channel_id.startswith('-100'):
        channel_id = "-100" + channel_id

    for user in users_to_notify_1 + users_to_notify_3 + users_to_notify_7:
        _ = generate_underscore(user.language_code)
        try:
            if user.subscription.days <= 1:
                days_text = _("24 hours")
                report_days_text = "24 hours"
            elif 1 < user.subscription.days <= 3:
                days_text = _("3 days")
                report_days_text = "3 days"
            elif 3 < user.subscription.days <= 7:
                days_text = _("7 days")
                report_days_text = "7 days"
            else:
                days_text = _("? days")
                report_days_text = "? days"

            message_text = emojize(_("Hi!\n\n"
                                     "Your subscription to signals and analytics expires in {days_text}, {expire_date}.\n\n"
                                     "You can renew subscription in Main menu of InProfit Bot.\n\n"
                                     "Thank you :slightly_smiling_face:").format(days_text=days_text,
                                                                                 expire_date=user.subscription.expire_date.strftime('%d.%m.%Y %H:%M')))
            report_text = emojize(":hourglass_not_done: ") + f"<a href=\"tg://user?id={user.chat_id}\">{user.get_name()}</a> signals subscription expires in <b>{report_days_text}</b> (<code>{user.subscription.expire_date.strftime('%d.%m.%Y %H:%M')}</code>)."

            buttons = [[InlineKeyboardButton(_("Renew subscription"), callback_data='renew_signals_subscription')]]
            context.bot.send_message(chat_id=user.chat_id, text=message_text, parse_mode="HTML", reply_markup=InlineKeyboardMarkup(buttons))
            context.bot.send_message(chat_id=channel_id, text=report_text, parse_mode="HTML")
        except (Unauthorized, BadRequest):
            pass

    for user in users_expired:
        _ = generate_underscore(user.language_code)

        try:
            message_text = emojize(_("Hi!\n\n"
                                     "Your subscription to signals and analytics expired, so we have to remove you from &lt;Private&gt; group :(\n\n"
                                     "You can join InProfit &lt;Private&gt; anytime you want by \"Subscribe to signals\" button in Main menu.\n\n"
                                     "We're waiting for you to come back :red_heart:"))
            report_text = emojize(":hourglass_not_done: ") + f"<a href=\"tg://user?id={user.chat_id}\">{user.get_name()}</a> signals subscription expired."
            context.bot.send_message(chat_id=user.chat_id, text=message_text, parse_mode="HTML")
            context.bot.send_message(chat_id=channel_id, text=report_text, parse_mode="HTML")

        except (Unauthorized, BadRequest):
            pass


def start_autotrade_expire_job(job_queue):
    stop_autotrade_expire_job(job_queue)
    job_queue.run_daily(autotrade_expire_job, time=datetime.time(hour=11), name="autotrade_expire_job")

    logging.info("Check autotrade subscription expire job started...")


def stop_autotrade_expire_job(job_queue):
    for job in job_queue.get_jobs_by_name("autotrade_expire_job"):
        job.schedule_removal()
        logging.info("Check autotrade subscription expire job stopped...")


def autotrade_expire_job(context):
    session = Database().sessionmaker()
    now = datetime.datetime.utcnow()
    base_query = session.query(Account).join(Account.subscription).filter(Account.is_valid == True)
    accounts_to_notify_1 = base_query.filter(AutotradeSubscription.expire_date >= now + datetime.timedelta(days=1)).filter(AutotradeSubscription.expire_date < now + datetime.timedelta(days=2)).all()
    accounts_to_notify_3 = base_query.filter(AutotradeSubscription.expire_date >= now + datetime.timedelta(days=3)).filter(AutotradeSubscription.expire_date < now + datetime.timedelta(days=4)).all()
    accounts_to_notify_7 = base_query.filter(AutotradeSubscription.expire_date >= now + datetime.timedelta(days=7)).filter(AutotradeSubscription.expire_date < now + datetime.timedelta(days=8)).all()
    accounts_expired = base_query.filter(AutotradeSubscription.expire_date <= now).filter(AutotradeSubscription.expire_date > now - datetime.timedelta(days=1)).all()

    settings = get_settings(SETTINGS_FILE)
    channel_id = str(settings.get('account_subscription_report_channel_id', 0))
    if not channel_id.startswith('-100'):
        channel_id = "-100" + channel_id

    for account in accounts_to_notify_1 + accounts_to_notify_3 + accounts_to_notify_7:
        _ = generate_underscore(account.user.language_code)

        if account.subscription.days <= 1:
            days_text = _("24 hours")
            report_days_text = "24 hours"
        elif 1 < account.subscription.days <= 3:
            days_text = _("3 days")
            report_days_text = "3 days"
        elif 3 < account.subscription.days <= 7:
            days_text = _("7 days")
            report_days_text = "7 days"
        else:
            days_text = _("? days")
            report_days_text = "? days"

        if account.subscription.type is SubscriptionType.short_term:
            sub_type = _("Short-term")
        # elif account.subscription.type is SubscriptionType.mid_term:
            # sub_type = _("Mid-term")
        else:
            sub_type = _("Unknown")

        try:
            message_text = emojize(_("Hi!\n\n"
                                     "Your account <b>\"{account_name}\"</b> subscription to autotrading on {sub_type} strategy expires in {days_text}, <code>{expire_date}</code>.\n\n"
                                     "You can renew subscription in Account menu of InProfit Bot.\n\n"
                                     "Thank you :slightly_smiling_face:").format(
                account_name=account.name,
                sub_type=sub_type,
                days_text=days_text,
                expire_date=account.subscription.expire_date.strftime('%d.%m.%Y %H:%M')))
            report_text = emojize(":hourglass_not_done: ") + f"<a href=\"tg://user?id={account.user.chat_id}\">{account.user.get_name()}</a> autotrade {account.subscription.type.value} subscription on {account.name} account expires in <b>{report_days_text}</b> " \
                f"(<code>{account.subscription.expire_date.strftime('%d.%m.%Y %H:%M')}</code>)."

            buttons = [[InlineKeyboardButton(_("Renew subscription"), callback_data=f'renew_autotrade_{account.id}')],
                       [InlineKeyboardButton(_("Close"), callback_data=f'close_message')]]
            context.bot.send_message(chat_id=account.user.chat_id, text=message_text, parse_mode="HTML", reply_markup=InlineKeyboardMarkup(buttons))
            context.bot.send_message(chat_id=channel_id, text=report_text, parse_mode="HTML")
        except (Unauthorized, BadRequest):
            pass

    for account in accounts_expired:
        _ = generate_underscore(account.user.language_code)

        if account.subscription.type is SubscriptionType.short_term:
            sub_type = _("Short-term")
        # elif account.subscription.type is SubscriptionType.mid_term:
        #     sub_type = _("Mid-term")
        else:
            sub_type = _("Unknown")
        try:
            message_text = emojize(_("Hi!\n\n"
                                     "Your account <b>\"{account_name}\"</b> subscription to autotrading on {sub_type} strategy expired.\n\n"
                                     "Unfortunately we have to deactivate autotrading for this account :(\n\n"
                                     "You can subscribe to autotrading anytime you want by \"Subscribe to autorading\" button in Account menu.\n\n"
                                     "We're waiting for you to connect to InProfit Bot back :red_heart:").format(
                account_name=account.name,
                sub_type=sub_type,
            ))
            report_text = emojize(":hourglass_not_done: ") + f"<a href=\"tg://user?id={account.user.chat_id}\">{account.user.get_name()}</a> autotrade subscription expired on {account.name}."
            context.bot.send_message(chat_id=account.user.chat_id, text=message_text, parse_mode="HTML")
            context.bot.send_message(chat_id=channel_id, text=report_text, parse_mode="HTML")

            if account.coins:
                # deactivate strategies
                for coin in account.coins:
                    for strategy in coin.strategies:
                        strategy.deactivate(session)
            session.commit()
        except (Unauthorized, BadRequest):
            pass


def start_check_api_keys_job(job_queue):
    stop_check_api_keys_job(job_queue)
    job_queue.run_repeating(check_api_keys_job, 60 * 60, first=0, name="check_api_keys_job")
    logging.info("Check API keys job started...")


def stop_check_api_keys_job(job_queue):
    for job in job_queue.get_jobs_by_name("check_api_keys_job"):
        job.schedule_removal()
        logging.info("Check API keys job stopped...")


def check_api_keys_job(context):
    session = Database().DBSession

    accounts = session.query(Account).filter(Account.is_valid == True).all()
    for account in accounts:
        account.check_account()
        if account.is_valid == False:
            session.add(account)

    session.commit()