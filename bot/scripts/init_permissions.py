from models import DBSession, Permission, User, Account, AutotradeSubscription, Coin, Position, PostingChat, Strategy


def main():
    Permission.create(Account.list_permission, {'en': 'Access to "Accounts" menu', 'ru': 'Доступ к меню "Аккаунты"', 'uk': 'Доступ до меню "Аккаунти"'})
    Permission.create(Account.add_permission, {'en': 'Access to "Add account" menu', 'ru': 'Доступ к меню "Добавить аккаунты"', 'uk': 'Доступ до меню "Додати аккаунти"'})
    Permission.create(Account.view_permission, {'en': 'Access to "Account info" menu', 'ru': 'Доступ к меню "Информация о аккаунте"', 'uk': 'Доступ до меню "Інформація про аккаунт"'})
    Permission.create(Account.edit_permission, {'en': 'Access to "Edit account" menu', 'ru': 'Доступ к меню "Редактировать аккаунт"', 'uk': 'Доступ до меню "Редагувати аккаунт"'})
    Permission.create(Account.yield_curve_permission, {'en': 'Access to "Yield curve" menu', 'ru': 'Доступ к меню "Кривая доходности"', 'uk': 'Доступ до меню "Крива дохідності"'})
    Permission.create(Account.disable_permission, {'en': 'Allow disable account', 'ru': 'Разрешить деактивировать аккаунт', 'uk': 'Дозволити деактивувати аккаунт'})

    Permission.create(AutotradeSubscription.view_permission, {'en': 'Access to "Autotrade subscription" menu', 'ru': 'Доступ к меню "Подписка на авто-торговлю"', 'uk': 'Доступ до меню "Передплата за авто-торгівлю"'})

    Permission.create(Coin.view_permission, {'en': 'Access to "Coins" menu', 'ru': 'Доступ к меню "Монеты"', 'uk': 'Доступ до меню "Монети"'})
    Permission.create(Coin.oninvalid_permission, {'en': 'Access to "Coins" menu from account with invalid API', 'ru': 'Доступ к меню "Монеты" из аккаунта с недействительным API', 'uk': 'Доступ до меню "Монети" з аккаунта з недійсним API'})
    Permission.create(Coin.list_permission, {'en': 'Access to "All coins" menu', 'ru': 'Доступ к меню "Все монеты"', 'uk': 'Доступ до меню "Усі монети"'})
    Permission.create(Coin.add_permission, {'en': 'Access to "Add coin" menu', 'ru': 'Доступ к меню "Добавить монету"', 'uk': 'Доступ до меню "Додати монету"'})
    Permission.create(Coin.delete_permission, {'en': 'Allow delete coin', 'ru': 'Разрешить удалять монеты', 'uk': 'Дозволити видаляти монети'})
    Permission.create(Coin.view_deppart_permission, {'en': 'Allow view deposit part', 'ru': 'Разрешить смотреть часть депозита', 'uk': 'Дозволити перегляд частин депозиту'})
    Permission.create(Coin.edit_deppart_permission, {'en': 'Allow change deposit part', 'ru': 'Разрешить изменение часть депозита', 'uk': 'Дозволити зміну частин депозиту'})

    Permission.create(Position.view_permission, {'en': 'Access to "Positions" menu', 'ru': 'Доступ к меню "Позиции"', 'uk': 'Доступ до меню "Позиції"'})
    Permission.create(Position.oninvalid_permission, {'en': 'Access to "Positions" menu from account with invalid API', 'ru': 'Доступ к меню "Позиции" из аккаунта с недействительным API', 'uk': 'Доступ до меню "Позиції" з аккаунта з недійсним API'})
    Permission.create(Position.close_all_permission, {'en': 'Allow close all positions', 'ru': 'Разрешить закрывать все позиции', 'uk': 'Дозволити закривати усі позиції'})
    Permission.create(Position.close_permission, {'en': 'Allow close position', 'ru': 'Разрешить закрывать позицию', 'uk': 'Дозволити закривати позицію'})
    Permission.create(Position.remove_permission, {'en': 'Allow remove position', 'ru': 'Разрешить удалять позицию'})
    Permission.create(Position.remove_all_permission, {'en': 'Allow remove all positions', 'ru': 'Разрешить удалять позиции'})

    Permission.create(PostingChat.view_permission, {'en': 'Access to "Posting chats" menu', 'ru': 'Доступ к меню "Уведомления в чатах"', 'uk': 'Доступ до меню "Повідомлення в чати"'})

    Permission.create(Strategy.view_permission, {'en': 'Access to "Strategies" menu', 'ru': 'Доступ к меню "Стратегии"', 'uk': 'Доступ до меню "Стратегії"'})
    Permission.create(Strategy.add_permission, {'en': 'Access to "Add strategy" menu', 'ru': 'Доступ к меню "Добавить стратегии"', 'uk': 'Доступ до меню "Додати стратегії"'})
    Permission.create(Strategy.view_chart_permission, {'en': 'Access to "Chart" page', 'ru': 'Доступ к странице "График"', 'uk': 'Доступ до сторінки "Графік"'})
    Permission.create(Strategy.activate_permission, {'en': 'Allow activate strategy', 'ru': 'Разрешить активировать стратегию', 'uk': 'Дозволити активувати стратегію'})
    Permission.create(Strategy.deactivate_permission, {'en': 'Allow deactivate strategy', 'ru': 'Разрешить деактивировать стратегию', 'uk': 'Дозволити деактивувати стратегії'})
    Permission.create(Strategy.delete_permission, {'en': 'Allow delete strategy', 'ru': 'Разрешить удалить стратегию', 'uk': 'Дозволити видалення стратегій'})

    Permission.create(Permission.view_permission, {'en': 'Access to "Permissions" menu', 'ru': 'Доступ к меню "Права"', 'uk': 'Доступ до меню "Права"'})
    Permission.create(Permission.add_permission, {'en': 'Allow add permission', 'ru': 'Разрешить добавлять право', 'uk': 'Дозволити додавати право'})
    Permission.create(Permission.delete_permission, {'en': 'Allow remove permission', 'ru': 'Разрешить удалять право', 'uk': 'Дозволити видаляти право'})

    Permission.create('start_menu_access', {'en': 'Access to "Start" menu', 'ru': 'Доступ к меню "Старт"', 'uk': 'Доступ до меню "Старт"'})
    Permission.create('change_language_menu_access', {'en': 'Access to "Change language" menu', 'ru': 'Доступ к меню "Сменить язык"', 'uk': 'Змінити язик'})
    Permission.create('subscribe_to_signals_menu_access', {'en': 'Access to "Subscribe to signals" menu', 'ru': 'Доступ к меню "Подписка на сигналы"', 'uk': 'Доступ до меню "Передплата за сигнали"'})
    Permission.create('referrals_menu_access', {'en': 'Access to "Referrals menu', 'ru': 'Доступ к меню "Рефералы"', 'uk': 'Доступ до меню "Реферали"'})
    Permission.create('about_menu_access', {'en': 'Access to "About" menu', 'ru': 'Доступ к меню "О нас"', 'uk': 'Доступ до меню "Про нас"'})
    Permission.create('withdraw_menu_access', {'en': 'Access to "Withdraw" menu', 'ru': 'Доступ к меню "Вывод денег"', 'uk': 'Доступ до меню "Вивід грошей"'})
    Permission.create('allow_create_binance_account', {'en': 'Allow create binance account', 'ru': 'Разрешить создавать бинанс-аккаунт', 'uk': 'Дозволити створювати бінанс-акаунт'})

    Permission.create('trade_menu_access', {'en': 'Access to "Trade" menu', 'ru': 'Доступ к меню "Торговля"', 'uk': 'Доступ до меню "Торгівля"'})
    Permission.create('admin_menu_access', {'en': 'Access to "Admin" menu', 'ru': 'Доступ к меню "Админ"', 'uk': 'Доступ до меню "Адмін"'})
    Permission.create('distribution_menu_access', {'en': 'Access to "Distribution" menu', 'ru': 'Доступ к меню "Рассылка"', 'uk': 'Доступ до меню "Розсилка"'})
    Permission.create('instant_distribution_menu_access', {'en': 'Access to "Instant distribution" menu', 'ru': 'Доступ к меню "Моментальная рассылка"', 'uk': 'Доступ до меню "Моментальна розсилка"'})
    Permission.create('admin_accounts_info_menu_access', {'en': 'Access to "Accounts info" menu in admin', 'ru': 'Доступ к админ меню "Информация о аккаунтах"', 'uk': 'Доступ до адмін-меню "Інформація про аккаунти"'})
    Permission.create('statistics_menu_access', {'en': 'Access to "Statistics" menu', 'ru': 'Доступ к меню "Статистика"', 'uk': 'Доступ до меню "Статистика"'})
    Permission.create('allow_close_account_positions', {'en': 'Allow close account positions', 'ru': 'Разрешить закрывать позиции аккаунта', 'uk': 'Дозволити закривати позиції аккаунта'})

    Permission.create('superuser', {'en': 'Superuser', 'ru': 'Суперпользователь'})

    DBSession.commit()

    users = DBSession.query(User).filter((User.chat_id == 262902863) | (User.chat_id == 37052954)).all()
    # users = DBSession.query(User).filter(User.chat_id == 262902863).all()
    # artemiy = DBSession.query(User).filter(User.chat_id == 37052954).first()
    # artemiy.permissions = []
    # artemiy.init_permissions()
    # DBSession.add(artemiy)

    permission = DBSession.query(Permission).get('superuser')
    for user in users:
        if not user.has_permission(permission.code):
            user.permissions.append(permission)
        DBSession.add(user)
    DBSession.commit()


if __name__ == '__main__':
    main()
