import datetime

import pandas as pd
import plotly.graph_objects as go
from plotly.offline import plot

from backend.controllers.bitfinex.candle_controller import CandleFormer
from indicators.hma import HMA
from models import DBSession


def create_plot(ticker, timeframe, strategy=None, num=1000):
    def to_graph_dict(candle):
        return {
            'open': candle.open,
            'close': candle.close,
            'high': candle.high,
            'low': candle.low,
            'open_date': candle.open_date_dt,
            'volume': candle.volume,
            'ticker': candle.ticker
        }

    # TODO: Change candle former depending on account provider
    cf = CandleFormer(timeframe, None, ticker, session=DBSession)
    cf.start_time = lambda: datetime.datetime.utcnow() - datetime.timedelta(minutes=num * timeframe)
    candles = cf.get_candles()
    df = pd.DataFrame.from_records([to_graph_dict(c) for c in candles])

    data = [dict(
        type='candlestick',
        open=df.open,
        high=df.high,
        low=df.low,
        close=df.close,
        x=df.open_date,
        yaxis='y2',
        name='GS',
        increasing=dict(line=dict(color="#000000", width=1), fillcolor="#FFFFFF"),
        decreasing=dict(line=dict(color="#000000", width=1), fillcolor="#000000"),
    )]

    layout = dict()

    fig = dict(data=data, layout=layout)

    fig['layout'] = dict()
    fig['layout']['plot_bgcolor'] = 'rgb(250, 250, 250)'
    fig['layout']['xaxis'] = dict(rangeselector=dict(visible=True))
    fig['layout']['yaxis'] = dict(domain=[0, 0.2], showticklabels=False, fixedrange=False)
    fig['layout']['yaxis2'] = dict(domain=[0.2, 0.8], fixedrange=False)
    fig['layout']['legend'] = dict(orientation='h', y=0.9, x=0.3, yanchor='bottom')
    fig['layout']['margin'] = dict(t=40, b=40, r=40, l=40)

    rangeselector = dict(
        visible=True,
        x=0, y=0.9,
        bgcolor='rgba(150, 200, 250, 0.4)',
        font=dict(size=13),
        buttons=list([
            dict(count=1,
                 label='reset',
                 step='all'),
            dict(count=1,
                 label='1yr',
                 step='year',
                 stepmode='backward'),
            dict(count=3,
                 label='3 mo',
                 step='month',
                 stepmode='backward'),
            dict(count=1,
                 label='1 mo',
                 step='month',
                 stepmode='backward'),
            dict(step='all')
        ]))

    fig['layout']['xaxis']['rangeselector'] = rangeselector

    if strategy in ['han_solo_jr', 'han_solo_sr', 'han_solo_nodir']:
        hma_long = HMA(144)
        hma_short = HMA(55)
        hma_long_y = []
        hma_short_y = []
        strategy_x = []
        for candle in candles:
            hma_long.add(candle.to_dict())
            hma_short.add(candle.to_dict())
            hma_long_y.append(hma_long.value())
            hma_short_y.append(hma_short.value())
            strategy_x.append(candle.open_date_dt)

        hma_long_y = hma_long_y[144:]
        hma_short_y = hma_short_y[55:]

        fig['data'].append(dict(x=strategy_x, y=hma_long_y, type='scatter', mode='lines',
                                line=dict(width=1),
                                marker=dict(color='#E377C2'),
                                yaxis='y2', name='HMA Long'))

        fig['data'].append(dict(x=strategy_x, y=hma_short_y, type='scatter', mode='lines',
                                line=dict(width=1),
                                marker=dict(color='#008000'),
                                yaxis='y2', name='HMA Short'))

    colors = []

    for i in range(len(df.close)):
        if i != 0:
            if df.close[i] > df.close[i - 1]:
                colors.append("#008000")
            else:
                colors.append("#E377C2")
        else:
            colors.append("#E377C2")

    fig['data'].append(dict(x=df.open_date, y=df.volume,
                            marker=dict(color=colors),
                            type='bar', yaxis='y', name='Volume'))


    figure = go.Figure(**fig)
    div = plot(figure, auto_open=False, output_type='div', include_plotlyjs=False)
    return div
