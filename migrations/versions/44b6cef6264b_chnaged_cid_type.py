"""chnaged cid type

Revision ID: 44b6cef6264b
Revises: 5f10326f658c
Create Date: 2019-10-09 19:34:43.347921

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '44b6cef6264b'
down_revision = '5f10326f658c'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('orders', 'cid', type_=sa.String, existing_type=sa.INTEGER)


def downgrade():
    op.alter_column('orders', 'cid', type_=sa.INTEGER, existing_type=sa.VARCHAR)
