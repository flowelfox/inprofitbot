"""updated botmanlib

Revision ID: df193318557c
Revises: 2f660151f86d
Create Date: 2019-10-21 17:08:33.345718

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'df193318557c'
down_revision = '2f660151f86d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user_sessions',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('start', sa.DateTime(), nullable=False),
    sa.Column('end', sa.DateTime(), nullable=False),
    sa.Column('actions', sa.BigInteger(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('message_history')
    op.create_unique_constraint('_bfx_opendate_ticker_uc', 'bitfinex_candles', ['open_date', 'ticker'])
    op.drop_constraint('_opendate_ticker_uc', 'bitfinex_candles', type_='unique')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint('_opendate_ticker_uc', 'bitfinex_candles', ['open_date', 'ticker'])
    op.drop_constraint('_bfx_opendate_ticker_uc', 'bitfinex_candles', type_='unique')
    op.create_table('message_history',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('message_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('chat_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('text', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('type', postgresql.ENUM('command', 'text', 'reply', 'audio', 'document', 'photo', 'sticker', 'video', 'voice', 'video_note', 'contact', 'location', 'venue', 'forwarded', 'game', name='messagetype'), autoincrement=False, nullable=False),
    sa.Column('keyboard', postgresql.JSON(astext_type=sa.Text()), autoincrement=False, nullable=True),
    sa.Column('sender', postgresql.ENUM('user', 'bot', name='sender'), autoincrement=False, nullable=False),
    sa.Column('reply_to_text', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('reply_username', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('audio_name', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('video_name', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('timestamp', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id', name='message_history_pkey')
    )
    op.drop_table('user_sessions')
    # ### end Alembic commands ###
