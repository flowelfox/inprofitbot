��         �  {  �      �     �  	   �     �     �  
   �     �     �     �                    ,     4     D  
   J  
   U     `     i     r     y     �     �     �     �     �     �     �     �     �     �     �     �               (     9     A     E     Q     Y  "   b     �     �     �     �     �     �     �               )  1   >  )   p  $   �  %   �  -   �  ,     ^   @     �     �     �     �     �     �     �     �     �       	     S        s     �     �     �     �  I   �     .     =     B     O  
   U     `     g     u     }     �     �     �     �     �     �     �     �     �     �                '     9     E  T  L  U  �  �   �     �!  �   �"     �#     �#     �#  �  �#     x%     �%     �%  
   �%     �%     �%     �%     �%     �%  $   &  	   +&     5&     D&     X&  ,   h&  1   �&  
   �&     �&     �&     �&     �&     '     '     '     ,'     9'     F'     T'     ['     j'     �'     �'     �'     �'     �'     �'     �'     �'     �'     (  4   (     F(  B   c(  #   �(  ,   �(     �(      )     8)     P)  2   e)  t   �)  	   *     *     2*     @*  	   L*  
   V*     a*     v*     �*     �*     �*     �*     �*     �*     �*     �*     +     +     %+     *+     =+     V+  #   v+  (   �+     �+     �+     �+     �+  :   ,     V,     j,     �,  
   �,     �,  
   �,     �,     �,     �,     �,     -  
   4-     ?-     U-  
   m-     x-     �-     �-     �-     �-     �-     �-     �-  v   �-  �   k.     !/  �   ;/  M   0     m0     �0     �0     �0  3   �0  >  1  y   O2  �   �2    �3  �  �5  �  v8  q   ;     x;  
   �;  W   �;     �;     �;     �;     �;     <     $<     ,<     A<     T<     \<     e<     �<     �<  
   �<     �<     �<  
   �<     �<    �<  	   >     >  
   >     >     6>     N>     k>     o>     {>     �>  x   �>  #   "?  F   F?  "   �?     �?  "   �?  $   �?  9   @     P@     e@  	   s@     }@     �@     �@     �@     �@     �@     �@     �@     �@  G   �@  �  4A     �B     �B     �B     �B     C     /C     =C     JC     gC     oC     �C     �C     �C      �C     �C     D     D     )D     5D     >D     JD  
   WD     bD  
   nD  !   yD  	   �D     �D     �D     �D  #   �D     E  ,   "E     OE  !   `E     �E     �E     �E     �E     �E     �E  K   F  %   YF  !   F     �F  &   �F  ;   �F  -   G  *   EG  %   pG  ,   �G  ,   �G  X   �G  B   IH  ;   �H  =   �H  D   I  B   KI  �   �I  /   J     FJ  
   JJ  *   UJ  -   �J     �J     �J  &   �J  ,   �J     K     +K  �   9K  )   �K  5   �K     L  :   ,L  2   gL  �   �L     8M     VM     cM     M     �M     �M     �M     �M  #   �M     �M     N      N     $N  !   5N  9   WN     �N  
   �N  C   �N     �N     �N     	O     O     1O     KO  j  ZO  �  �Q  O  [S    �T  �   �U  ;   �V     �V     W  �  W     �Y  ,   �Y  1   �Y     Z  >   9Z  A   xZ  @   �Z     �Z     �Z  R   [     [[     s[     �[     �[  i   �[  k   \     |\  4   �\     �\  !   �\  3   �\     3]     Q]     X]      v]     �]     �]     �]     �]  7   �]  9   .^      h^  +   �^     �^     �^  '   �^  !   �^      _     5_     L_  u   U_  I   �_  j   `  T   �`  f   �`  T   <a  A   �a  1   �a  1   b  h   7b  �   �b     <c  -   Kc     yc     �c  )   �c     �c  &   �c  #   d     1d  )   Qd     {d  !   �d  ,   �d     �d  /   �d  -   e  D   Je     �e     �e     �e  2   �e  A   �e  b   Af  c   �f  +   g     4g  &   Ng  9   ug  5   �g  :   �g  <    h     ]h     zh  4   �h     �h  4   �h  B   i  >   ]i  T   �i  3   �i     %j  9   :j  ;   tj     �j     �j  !   �j  )   �j     "k  #   ;k  $   _k     �k  
   �k  �   �k  Y  dl  ;   �m  �  �m  �   �o  !   Np  !   pp  A   �p  ?   �p  V   q  "  kq  �   �s  k  ct  �  �u  �  dy  �  ~  �   ��     ��     ��  �   ��     c�     p�     ��  *   ��     Ƃ     �     ��     �     �     /�  G   B�  M   ��     ؃  
   �  "   ��  i   !�     ��     ��  �  Ƅ  
   ��     ��  .   ׆  E   �  0   L�  E   }�     Ç  !   ȇ  2   �  j   �  �   ��  F   s�  �   ��  F   y�  -   ��  V   �  X   E�  �   ��  -   !�  '   O�     w�  ;   ��  	   ��  &   ˌ  
   �     ��     �     �  '   �  '   4�  X   \�           �   Q   �   D   c                  X   �   �   �   `   V                   v   �       �              %       �       �   k     �               o   !   x   F   �   �   �      E   �              �   �   �      �      �   �   �   �   �           
      �   �   j   �   �   �             �                �   9         �   ^       u       �          A       }             �   >   	  3       N   I   b   q       �      �       =   P         �   R   7   �         C   �   �   �       Y   �       �                 8           �   ?   �   (     $   )   &         �   :   m   <   �   @   �           �           �   #   �                 \       �       �   �   '   �   t   �   �   �      �       |   G   -   �   �       �   �       �       �   �   {              �   p          d       �   �   �   �       "     4   l   	   �       �   2   �   .   �   �          �       B       �              _       �              ,   �   �            ~               s   �             f           �   �   �       �      �       �   �         �   �      T      �   �   �   g   �   �       �          �         �      +   L   �   1   �       O   /       e       �   ]   �   J         M   5   ;   K       �   U   �              �               h   �       6   [   z      0   �   �   �   �   �       �              �   y      �       �   *   
   i   �   H   �   �      n       �   S   �   a   �   �   �   �   �   �   W   �   w         �   Z   r    *Address *Currency *Deposit part *Fast *Indicator *Key *Left ticker *Longer period *Name *Pyramiding *Right ticker *Secret *Shorter period *Slow *Smoothing *Timeframe 180 days 24 hours 3 days 30 days 360 days 7 days 90 days ? days API is not valid. About About InProfit Account - READ Account deleted Account name:  Accounts Accounts info Activate Active positions:  Active strategy: Active: Add Add account Add all Add coin Add new message using button below Add permission Add subscription Added Admin menu: All account positions closed. All permissions added All permissions removed All positions removed. All your coins Allow hold signals:  Are you sure about closing all account positions? Are you sure about closing all positions? Are you sure about closing position? Are you sure about removing position? Are you sure you want to delete this account? Are you sure you want to delete this object? Are you totally sure that you want to disable your account?.

THIS OPERATION CANNOT BE UNDONE! Autotrade subscription BTC Back Back to edit Back to main menu Balance: Buttons: Buy message:  Change dep. part Chart Chat ID:  Chat connected to InProfit Bot

<i>This message will be deleted after 5 seconds</i> Choose distribution type: Choose what you'd like to do: Close Close account positions Close distribution message Close distribution message. Do not use if you have more than one message! Close position Coin Coin deleted Coins Deactivate Delete Deposit part: Details Disable subscription Disabled Distribution ETH Edit Edit caption Edit posting chat Email:  Empty Enter username, id or name: Error Exchange balance Exchange balance info Exchange balance: Exit search Friday Here you see the list of coins connected for autotrading on chosen account.
{line}
1. The "<b>All coins</b>" button opens a list of coins connected to trading on the selected account.

2. The arrows move you through the coins. On each coin page you will see opened position on the coin, balance of the coin on exchange and margin wallets.

 Hi!

Your account <b>"{account_name}"</b> subscription to autotrading on {sub_type} strategy expired.

Unfortunately we have to deactivate autotrading for this account :(

You can subscribe to autotrading anytime you want by "Subscribe to autorading" button in Account menu.

We're waiting for you to connect to InProfit Bot back :red_heart: Hi!

Your account <b>"{account_name}"</b> subscription to autotrading on {sub_type} strategy expires in {days_text}, <code>{expire_date}</code>.

You can renew subscription in Account menu of InProfit Bot.

Thank you :slightly_smiling_face: Hi!

Your subscription to signals and analytics expired, so we have to remove you from &lt;Private&gt; group :(

You can join InProfit &lt;Private&gt; anytime you want by "Subscribe to signals" button in Main menu.

We're waiting for you to come back :red_heart: Hi!

Your subscription to signals and analytics expires in {days_text}, {expire_date}.

You can renew subscription in Main menu of InProfit Bot.

Thank you :slightly_smiling_face: Hold message:  I've sent {} ID:  In this is menu of subscribing to signals and analytics.
{line}
1. Choose the subscription term.

2. Next, choose a cryptocurrency, which is convenient for you to pay for subscription.

3. You will see the subscription price in the amount of the chosen cryptocurrency and the dollar equivalent. The course for calculation is taken realtime from Bitfinex.

4. When you send the required amount, click on the payment confirmation button. Instant distribution Invite link:  Invited users Is valid:  Joined in past 1 day: Joined in past 30 days: Joined in past 7 days: LTC Language Longer must be greater than shorter. Main menu Margin balance Margin balance info Margin balance: Message was sent to all bot users except you Messages will be sent to all bot users. Continue? Messages:  Mid-term autotrading Monday Name:  New posting chat New strategy No No, I changed my mind Nobody found Not uploaded Nothing found Object Object deleted Open position message Open position message:  Open position: Orders - READ and WRITE Page Periods: Permission added Permission removed Permissions Permissions:  Photo Please enter text which would be displayed on button Please fill required fields. Please forward any message to me from channel you want to connect. Please select autotrading strategy: Please select callback that you want to add: Please select payment currency: Please select subscription term: Please send me document Please send me photo Please send txid, so we can track you transaction. Please send {amount:.4f} {currency} ({usd_price:.0f} USD) to this wallet:

<b>{currency}</b>: <code>{address}</code> Positions Positions - READ and WRITE Posting chats Pyramiding: Referrals Remove all Remove all positions Remove permission Remove position Rename account Renew Renew subscription Required permissions: Reset Return to account Return to main menu Return user to start menu Saturday Save Select a language: Select autotrading type: Select days using buttons below Select permission to add to {user}. Select permission to remove from {user}. Select subscription term: Selected days:
 Sell message:  Send me new caption for photo. Send me text message or photo with or without description. Send position ratio Send positions ratio:  Send withdraw request Short term Short term autotrading Short-term Short-term autotrading Show only invalid Show only valid Slow must be greater than fast. Sorry unknown command. Statistics Stop position message Stop position message:  Strategies Strategy Strategy deleted Subscription added Subscription inactive Subscription renewed Subscription to signals Sunday Text Thank you!

We will check your transaction and contact you soon.
For any support contact @inprofit_admin or @artemium. Thank you!

We withdraw money manually, so it may take a while. As soon as transaction reaches blockchain we will contact you.

For any support contact @inprofit_admin or @artemium. That pair already exists. The referral program menu shows your referral link, a list of invited users and earnings from their subscriptions.
{line}
To invite a friend, send him your referral link. You will receive 20% of the money paid by your invite.

 There is no help for this menu. If you need help here, write @inprofit_admin. There is nothing to list There is nothing to show This field allow only document. This field allow only photo. This field does not allow upload photo or document. This is Accounts menu of InProfit Bot.
{line}
1. The "<b>Add Account</b>" button opens the menu for adding a Bitfinex exchange account.

2. Next are your accounts if you have one. Click the button with the name of the account to open the menu of viewing the account and activate the subscription for automatic trading. This is first Beta release. Everything works ok, but if you find any bugs, please report to @artemium or @inprofit_admin. This is first Beta release. Everything works ok, but if you find any bugs, please report to @artemium or @inprofit_admin.

Almost every menu has a description. When you enter somewhere you need help, use /help command. This is menu of autotrading subscription.
{line}
1. You see the buttons with the choice of subscription type. Short-term works on the hourly chart.

2. Next you choose the term of the subscription.

3. Next you choose the cryptocurrency, which you'd like to use to pay for the subscription.

4. You will see the subscription price in the amount of the chosen cryptocurrency and its dollar equivalent. The course for calculation takes from Bitfinex.

5. When you send the required amount, click on the payment confirmation button. This is menu of viewing your account.
{line}
1. The "<b>Exchange Wallet</b>" and "<b>Margin Wallet</b>" buttons open the menu to view more detailed information about your exchange wallets of the selected account.

2. The "<b>Yield Curve</b>" button shows the changes of the balance on the Margin wallet of the selected account.

3. The "<b>Subscription</b>" button opens the automatic trading subscription menu in the selected account.

4. The "<b>Positions</b>" button opens the list of current positions opened by the bot on the selected account.

5. The "<b>Rename account</b>" button allows you to change the name of the selected account.

6. The "<b>Delete</b>" button deletes the selected account. This is the main menu of InProfit Bot.
{line}
1. The "<b>Accounts</b>" button opens the menu of viewing and adding Bitfinex accounts.

2. The "<b>Subscribe to signals</b>" button opens the menu of viewing and activating subscription to the &lt;Private&gt; channel and chat.

3. The "<b>Referral program</b>" button opens the menu for viewing your referral statistics. There is also a button to request the withdrawal of funds earned through invitations.

4. The "<b>About project</b>" button opens the InProfit info menu.

5. The "<b>Language</b>" button opens the menu of changing the interface language. Currently available: Russian, English, Ukrainian. This will delete all your accounts and active subscriptions.

Are you sure that you want to disable your account? Thursday Timeframe: To buy short-term autotrade subscription you need to have 500-6000 USD on your account. Total:  Totally sure Trade Trailing stop percent Trailing stop: Tuesday USD Margin balance:  USDT Tether (Omni) Unknown Uploaded User {user} has all permissions User {user} has no permissions User:  Username:  Users in bot: Users that stop or block bot: Waiting... Wallets - READ We created profitable algorithms for crypto-trading.

You can work manually by following InProfit signals or connect your Bitfinex account to autotrading.

Short-term bot earns from +15% monthly.

You can read more about us by clicking "About InProfit" button. Wednesday Withdraw Wrong date Wrong input, try again Wrong subscription type Wrong txid, please try again Yes Yield curve You are invited by You are invited by Nobody. You disabled your account.
You can send me /start command to enter bot or use referral link to become someones referral. You don't have signals subscription You give withdraw permissions to account key. It's not needed for bot. You must add at least one message. You subscription active until You were restricted from using bot You were restricted to use this menu Your balance is not updated yet.

Try again in 5 minutes. \*Allow hold signals \*Buy message \*Chat ID \*Hold message \*Name \*Sell message found from of out of {ticker} position closed. {ticker} position removed. {type} subscription active until <code>{date}</code> ({days} days left) Project-Id-Version: 
PO-Revision-Date: 2019-10-04 16:20+0300
Last-Translator: Flowelcat <Flowelcat@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
 *Адрес *Валюта *Часть депозита *Быстрый период *Индикатор *Ключ API *Left ticker *Длинный период *Имя *Пирамидинг *Right ticker *Секретный ключ *Короткий период *Медленный период *Сглаживание *Таймфрейм 180 дней 24 часа 3 дня 30 дней 360 дней 7 дней 90 дней ? дней API недействителен. О нас Об InProfit Аккаунт - ЧТЕНИЕ Аккаунт удален Названия аккаунта:  Аккаунты Информация об аккаунтах Включить Открытые позиции:  Стратегия: Активно: Добавить Добавить аккаунт Добавить все Добавить монету Введите сообщение с помощью кнопок внизу Добавить привилегию Добавить подписку Добавлено Меню администратора: Все позиции на аккаунте закрыты. Я добавил все привилегии Все привилегии удалены Все позиции очищены. Все подключенные монеты Разрешить ХОЛД-сигналы:  Вы точно хотите закрыть все позиции на аккаунте? Вы точно хотите закрыть все позиции? Вы точно хотите закрыть позицию? Вы точно хотите очистить позицию? Вы точно хотите удалить этот аккаунт? Вы точно хотите удалить этот объект? Вы точно уверены, что хотите отключить аккаунт?

Это необратимое действие! Подписка на авто-торговлю BTC Назад Назад к редактированию Вернуться в главное меню Баланс: Кнопки: Сообщение о покупке:  Изменить часть депозита График ID чата:  Чат присоединен к InProfit Bot

<i>Это сообщение будет удалено через 5 секунд</i> Выберите тип рассылки: Выберите, что хотите сделать: Закрыть Закрыть все позиции на аккаунте Закрыть сообщение рассылки Закрыть сообщение рассылки. Не используйте, если вы добавили больше одного сообщения! Закрыть позицию Монета Монета удалена Монеты Отключить Удалить Часть депозита: Детали Отключить подписку Отключено Рассылка ETH Изменить Изменить описание Изменить канал для уведомлений Email:  Пусто Введите Username, ID или Имя пользователя: Ошибка :( Exchange balance Exchange balance info Exchange balance: Закрыть поиск Пятница В этом меню отображаются монеты, подключенные к автоматической торговле на выбранном аккаунте.
{line}
1. Кнопка "<b>Все монеты</b>" открывает список монет, подключенных к автоматической торговле на выбранном аккаунте.

2. Стрелки перемещают вас между монетами. На странице каждой монеты отображается позиция и баланс монеты на ваших кошельках.

 Привет!

На вашем аккаунте <b>"{account_name}"</b> закончилась подписка на {sub_type} авто-торговлю.

К сожалению нам придется отключить торговлю на этом аккаунте :(

Обновить подписку можно в меню Аккаунта.

Ждем вашего возвращения :red_heart: Привет!

На вашем аккаунте <b>"{account_name}"</b> через {days_text} заканчивается срок подписки на {sub_type} авто-торговлю , <code>{expire_date}</code>.

Обновить подписку можно в меню Аккаунта.

Спасибо :slightly_smiling_face: Привет!

Ваша подписка на сигналы закончилась, придется удалить вас из &lt;Private&gt; группы :(

Обновить подписку можно в главном меню.

Ждем вас обратно :red_heart: Привет!

Ваша подписка на сигналы заканчивается через {days_text}, {expire_date}.

Обновить подписку можно через главное меню.

Спасибо :slightly_smiling_face: Сообщение об удержании позиции:  Отправил {} ID:  Это меню подписки на сигналы и аналитику.
{line}
1. Выберите срок подписки.

2. Выберите криптовалюту, в которой удобнее провести оплату.

3. Дальше вы увидите цену подписки в количестве выбранной криптовалюты и эквивалент цены в долларе. Курс подтягивается в реальном времени с Bitfinex.

4. Когда отправите необходимую сумму, нажмите на кнопку подтверждения оплаты. Срочная рассылка Ссылка для приглашения:  Приглашенные пользователи Действительный:  Присоединились за последний день: Присоединились за последние 30 дней: Присоединились за последние 7 дней: LTC Язык Длинный период должен быть больше короткого. Главное меню Margin balance Margin balance info Margin balance: Сообщение отправилось всем пользователям бота, кроме вас Сообщение отправится всем пользователям бота. Продолжаем? Сообщения:  Среднесрочная авто-торговля Понедельник Имя пользователя:  Новый канал для уведомлений Новая стратегия Нет Нет, я передумал Никого не найдено Не загружено Ничего не нашел :( Объект Объект удален Сообщение об открытии позиции Сообщение об открытии позиции:  Открытая позиция: Ордера - ЧТЕНИЕ и ЗАПИСЬ Страница Периоды: Привилегия добавлена Удалил привилегию Привилегии Привилегии:  Фото Пожалуйста, введите текст, который будет отображаться на кнопке Пожалуйста, заполните необходимые поля. Перешлите сообщение из канала, который хотите подключить. Пожалуйста, выберите стратегию авто-торговли: Пожалуйста, выберите действие, которое хотите добавить: Пожалуйста, выберите криптовалюту для оплаты: Пожалуйста, выберите срок подписки: Пожалуйста, отправьте файл Пожалуйста, отправьте фото Пожалуйста, отправьте TxID, чтобы мы проверили вашу оплату. Пожалуйста, отправьте {amount:.4f} {currency} ({usd_price:.0f} USD) на этот адрес:

<b>{currency}</b>: <code>{address}</code> Позиции Позиции - ЧТЕНИЕ и ЗАПИСЬ Уведомления Пирамидинг: Реферальная программа Удалить все Очистить все позиции Удалить привилегию Очистить позицию Переименовать аккаунт Обновить Обновить подписку Необходимые привилегии: Очистить Вернуться в меню аккаунта Вернуться в главное меню Возвращает пользователя в старт-меню Суббота Сохранить Выберите язык: Выберите тип авто-торговли: Выберите дни с помощью кнопок внизу Выберите привилегию для добавления пользователю {user}. Выберите привилегию для удаления от пользователя {user}. Выберите срок подписки: Выберите дни:
 Сообщение о продаже:  Пришлите мне описание для фото. Отправьте мне текст или фото. Отправлять соотношение позиций Отправлять соотношение позиций:  Запрос на вывод Краткосрочная Краткосрочная авто-торговля Краткосрочную Краткосрочная авто-торговля Показывать только недействительные Показывать только действительные Медленный период должен быть больше быстрого. Извините, такой команды нет. Статистика Сообщение о срабатывании стопа Сообщение о срабатывании стопа:  Стратегии Стратегия Стратегия удалена Подписка активирована Нет  подписки Подписка обновлена Подписка на сигналы Воскресенье Текст Спасибо!

Мы проверим транзакцию и сразу напишем вам.
По любым вопросам обращайтесь к @inprofit_admin или @artemium. Спасибо!

Мы отправляем деньги вручную, поэтому придется немного подождать. Как только транзакция попадет в блокчейн, мы пришлем вам ее ID.

По любым вопросам обращайтесь к @inprofit_admin или @artemium. Эта торговая пара уже добавлена. Меню реферальной программы показывает вашу реферальную ссылку, список приглашенных пользователей и ваш заработок от приглашений.
{line}
Чтобы пригласить друга, отправьте ему свою реферальную ссылку. Вы получите 20% стоимости его подписок.

 Для этого меню нет дополнительной информации. Если вам нужна помощь, напишите @inprofit_admin. Здесь ничего нет :( Здесь ничего нет :( Это поле поддерживает только файлы. Это поле поддерживает только фото. Это поле не позволяет загружать фото или файлы. Это меню-список подключенных аккаунтов.
{line}
1. Кнопка "<b>Добавить аккаунт</b>" открывает меню подключения аккаунта Bitfinex.

2. Дальше отображаются названия подключенных аккаунтов, если они у вас есть. Нажмите на кнопку с именем аккаунта, чтобы открыть меню просмотра аккаунта и подключения авто-торговли. Это наша первая Бета-версия. Все работает, но если вы все-таки найдете ошибки, напишите пожалуйста @inprofit_admin или @artemium. Это наша первая Бета-версия. Все работает, но если вы все-таки найдете ошибки, напишите пожалуйста @inprofit_admin или @artemium.

Почти для каждого меню есть описание. Если вы запутались, используйте команду /help. Это меню подписки на авто-торговлю.
{line}
1. Выберите тип подписки. Среднесрочная работает на дневном графике, краткосрочная работает на часовом графике.

2. Выберите срок подписки. Сколько дней бот будет торговать на вашем аккаунте.

3. Выберите криптовалюту, которой удобнее всего провести оплату.

4. Вы увидите цену подписки в количестве выбранной криптовалюты и эквивалент в долларе. Курс подтягивается в реальном времени с Bitfinex.

5. Когда отправите нужную сумму, нажмите на кнопку подтверждения оплаты. Это меню просмотра вашего аккаунта.
{line}
1. Кнопки "<b>Exchange Wallet</b>" и "<b>Margin Wallet</b>" открывают меню просмотра подробной информации о биржевых кошельках на выбранном аккаунте. 

2. Кнопка "<b>Кривая доходности</b>" показывает изменения баланса на Маржинальном кошельке выбранного аккаунта.

3. Кнопка "<b>Подписка на авто-торговлю</b>" открывает меню подписки на автоматическую торговлю на выбранном аккаунте.

4. Кнопка "<b>Позиции</b>" открывает список позиций, открытых в данный момент на выбранном аккаунте.

5. Кнопка "<b>Переименовать аккаунт</b>" позволяет вам изменить название аккаунта.

6. Кнопка "<b>Удалить аккаунт</b>" закрывает все позиции и удаляет выбранный аккаунт. Это меню подписки на авто-торговлю.
{line}
1. Выберите тип подписки. Краткосрочная подписка активирует бота для работы на часовом графике.

2. Выберите срок подписки.

3. Выберите криптовалюту, удобную для оплаты подписки.

4. Вы увидите цену подписки в выбранной криптовалюте и ее долларовый эквивалент. Курс для рассчитывается с Bitfinex.

5. Когда вы отправите необходимую сумму, нажмите на кнопку подтверждения оплаты. Мы удалим из базы данных все ваши аккаунты и подписки.

Уверены, что хотите продолжить? Четверг Таймфрейм: Чтобы активировать подписку на краткосрочную авто-торговлю нужно от 500 до 6000 USD на балансе. Всего:  Точно уверен Торговать Процент трейлинг-стопа Трейлинг-стоп: Вторник Margin balance в USD:  USD Tether (Omni) Неизвестно Загружено У пользователя {user} есть все привилегии У пользователя {user} нет никаких привилегий Пользователь:  Username:  Пользователи бота: Пользователи, который остановили или заблокировали бота: Ожидаем ввод... Кошельки - ЧТЕНИЕ Мы создали прибыльные агоритмы для торговли на крипто-рынке.

Вы можете работать вручную по сигналам InProfit или подключить свой Bitfinex-аккаунт для авто-торговли.

Краткосрочный алгоритм зарабатывает от +15% к депозиту в месяц.

Прочитайте о нас подробнее по кнопке "Об InProfit". Среда Вывести деньги Неправильный формат даты Некорректный ввод, попробуйте еще раз Некорректный тип подписки Некорректный ввод, попробуйте еще раз Да Кривая доходности Вас пригласил пользователь Вы запустили бота напрямую, без реферального приглашения. Ваш аккаунт отключен.

Можете отправить команду /start или перейти по реферальной ссылке вашего друга чтобы запустить бота заново. У вас нет активной подписки на сигналы Вы дали разрешение на вывод денег. Пожалуйста, не рискуйте. Создайте новый ключ API без лишних разрешений. Нужно добавить хотя бы одно сообщение. Ваша подписка активна до У вас недостаточно прав для использования бота У вас недостаточно прав для доступа к этому меню Информация о вашем балансе еще не обновилась.

Попробуйте через 5 минут. \* Разрешить ХОЛД-сигналы \* Сообщение о покупке \*ID чата \*Сообщение об удержании позиции \ *Имя \*Сообщение о продаже нашел из из из {ticker} позиция закрыта. {ticker} позиция очищена. {type} подписка активна до <code>{date}</code> (еще {days} дней) 