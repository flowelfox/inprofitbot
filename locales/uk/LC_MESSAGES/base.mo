��         �  {  �      �     �  	   �     �     �  
   �     �     �     �                    ,     4     D  
   J  
   U     `     i     r     y     �     �     �     �     �     �     �     �     �     �     �     �               (     9     A     E     Q     Y  "   b     �     �     �     �     �     �     �               )  1   >  )   p  $   �  %   �  -   �  ,     ^   @     �     �     �     �     �     �     �     �     �       	     S        s     �     �     �     �  I   �     .     =     B     O  
   U     `     g     u     }     �     �     �     �     �     �     �     �     �     �                '     9     E  T  L  U  �  �   �     �!  �   �"     �#     �#     �#  �  �#     x%     �%     �%  
   �%     �%     �%     �%     �%     �%  $   &  	   +&     5&     D&     X&  ,   h&  1   �&  
   �&     �&     �&     �&     �&     '     '     '     ,'     9'     F'     T'     ['     j'     �'     �'     �'     �'     �'     �'     �'     �'     �'     (  4   (     F(  B   c(  #   �(  ,   �(     �(      )     8)     P)  2   e)  t   �)  	   *     *     2*     @*  	   L*  
   V*     a*     v*     �*     �*     �*     �*     �*     �*     �*     �*     +     +     %+     *+     =+     V+  #   v+  (   �+     �+     �+     �+     �+  :   ,     V,     j,     �,  
   �,     �,  
   �,     �,     �,     �,     �,     -  
   4-     ?-     U-  
   m-     x-     �-     �-     �-     �-     �-     �-     �-  v   �-  �   k.     !/  �   ;/  M   0     m0     �0     �0     �0  3   �0  >  1  y   O2  �   �2    �3  �  �5  �  v8  q   ;     x;  
   �;  W   �;     �;     �;     �;     �;     <     $<     ,<     A<     T<     \<     e<     �<     �<  
   �<     �<     �<  
   �<     �<    �<  	   >     >  
   >     >     6>     N>     k>     o>     {>     �>  x   �>  #   "?  F   F?  "   �?     �?  "   �?  $   �?  9   @     P@     e@  	   s@     }@     �@     �@     �@     �@     �@     �@     �@     �@  G   �@  ?  4A     tC     �C      �C     �C     �C  	   �C     �C     D     D     'D     =D     VD     sD      �D     �D     �D     �D     �D     �D     E     E  
    E     +E  
   7E     BE     ZE     hE  %   xE  !   �E     �E     �E  ,   �E     F  !   /F  $   QF     vF     �F     �F     �F     �F  `   �F     >G  !   XG     zG  &   �G  ;   �G  "   �G  &   H  '   4H  '   \H  (   �H  _   �H  N   I  ?   \I  A   �I  4   �I  M   J     aJ  3   �J     K  
   K  &   $K  7   KK     �K     �K  0   �K  .   �K     �K     L  �   L  )   �L  ;   �L     M  5   M  8   MM  �   �M     -N     KN     XN     vN     �N     �N      �N     �N  %   �N     �N     O     #O     'O     6O  7   VO     �O  
   �O  B   �O     �O     �O     P     P     ,P     FP  j  VP  �  �R  �   dT  (  dU  �   �V  C   �W     �W     �W  �  �W  #   �Z  0   �Z  )   �Z     [  7   [  8   L[  7   �[     �[     �[  O   �[     \     2\     A\     U\  v   e\  o   �\     L]  :   g]     �]  	   �]  3   �]     �]     ^     ^  "   *^     M^  %   i^     �^     �^  ?   �^  A   �^      :_  3   [_     �_     �_     �_     �_     �_     �_     
`  n   `  D   �`  l   �`  R   4a  O   �a  K   �a  3   #b  2   Wb  2   �b  e   �b  �   #c     �c  5   �c  *   d     1d  %   Gd     md  &   �d     �d     �d  )   �d     e  #   #e  "   Ge     je  ,   {e  7   �e  /   �e     f     f     .f  2   If  G   |f  A   �f  ?   g  3   Fg     zg  .   �g  =   �g  5   �g  B   5h  F   xh  '   �h     �h  8   i     ?i  8   ^i  4   �i  0   �i  S   �i  4   Qj     �j  N   �j  P   �j     ;k     Nk  #   ak  !   �k     �k  %   �k  (   �k     l  
   #l  �   .l  �   m  3   �m  �  ,n  �   �o     �p  &   �p  9   �p  7   q  \   Eq  �  �q  �   es  k  ;t  �  �u  e  |x  �  �|  �   ـ     ��     ��  �   ��     f�  #   u�     ��  *   ��     ׂ     �     �     �     .�     ?�  8   V�  E   ��     Ճ  
   �      ��  U   �     n�  %   ��  �  ��     ��     ��  &   ؆  E   ��  4   E�  E   z�     ��     Ǉ  ,   �  d   �  �   y�  N   D�  �   ��  V   Q�  1   ��  M   ڊ  X   (�  r   ��  (   �  .   �     L�  C   Z�     ��  .   ��     ڌ     �     �     �     ��  +   �  [   A�           �   Q   �   D   c                  X   �   �   �   `   V                   v   �       �              %       �       �   k     �               o   !   x   F   �   �   �      E   �              �   �   �      �      �   �   �   �   �           
      �   �   j   �   �   �             �                �   9         �   ^       u       �          A       }             �   >   	  3       N   I   b   q       �      �       =   P         �   R   7   �         C   �   �   �       Y   �       �                 8           �   ?   �   (     $   )   &         �   :   m   <   �   @   �           �           �   #   �                 \       �       �   �   '   �   t   �   �   �      �       |   G   -   �   �       �   �       �       �   �   {              �   p          d       �   �   �   �       "     4   l   	   �       �   2   �   .   �   �          �       B       �              _       �              ,   �   �            ~               s   �             f           �   �   �       �      �       �   �         �   �      T      �   �   �   g   �   �       �          �         �      +   L   �   1   �       O   /       e       �   ]   �   J         M   5   ;   K       �   U   �              �               h   �       6   [   z      0   �   �   �   �   �       �              �   y      �       �   *   
   i   �   H   �   �      n       �   S   �   a   �   �   �   �   �   �   W   �   w         �   Z   r    *Address *Currency *Deposit part *Fast *Indicator *Key *Left ticker *Longer period *Name *Pyramiding *Right ticker *Secret *Shorter period *Slow *Smoothing *Timeframe 180 days 24 hours 3 days 30 days 360 days 7 days 90 days ? days API is not valid. About About InProfit Account - READ Account deleted Account name:  Accounts Accounts info Activate Active positions:  Active strategy: Active: Add Add account Add all Add coin Add new message using button below Add permission Add subscription Added Admin menu: All account positions closed. All permissions added All permissions removed All positions removed. All your coins Allow hold signals:  Are you sure about closing all account positions? Are you sure about closing all positions? Are you sure about closing position? Are you sure about removing position? Are you sure you want to delete this account? Are you sure you want to delete this object? Are you totally sure that you want to disable your account?.

THIS OPERATION CANNOT BE UNDONE! Autotrade subscription BTC Back Back to edit Back to main menu Balance: Buttons: Buy message:  Change dep. part Chart Chat ID:  Chat connected to InProfit Bot

<i>This message will be deleted after 5 seconds</i> Choose distribution type: Choose what you'd like to do: Close Close account positions Close distribution message Close distribution message. Do not use if you have more than one message! Close position Coin Coin deleted Coins Deactivate Delete Deposit part: Details Disable subscription Disabled Distribution ETH Edit Edit caption Edit posting chat Email:  Empty Enter username, id or name: Error Exchange balance Exchange balance info Exchange balance: Exit search Friday Here you see the list of coins connected for autotrading on chosen account.
{line}
1. The "<b>All coins</b>" button opens a list of coins connected to trading on the selected account.

2. The arrows move you through the coins. On each coin page you will see opened position on the coin, balance of the coin on exchange and margin wallets.

 Hi!

Your account <b>"{account_name}"</b> subscription to autotrading on {sub_type} strategy expired.

Unfortunately we have to deactivate autotrading for this account :(

You can subscribe to autotrading anytime you want by "Subscribe to autorading" button in Account menu.

We're waiting for you to connect to InProfit Bot back :red_heart: Hi!

Your account <b>"{account_name}"</b> subscription to autotrading on {sub_type} strategy expires in {days_text}, <code>{expire_date}</code>.

You can renew subscription in Account menu of InProfit Bot.

Thank you :slightly_smiling_face: Hi!

Your subscription to signals and analytics expired, so we have to remove you from &lt;Private&gt; group :(

You can join InProfit &lt;Private&gt; anytime you want by "Subscribe to signals" button in Main menu.

We're waiting for you to come back :red_heart: Hi!

Your subscription to signals and analytics expires in {days_text}, {expire_date}.

You can renew subscription in Main menu of InProfit Bot.

Thank you :slightly_smiling_face: Hold message:  I've sent {} ID:  In this is menu of subscribing to signals and analytics.
{line}
1. Choose the subscription term.

2. Next, choose a cryptocurrency, which is convenient for you to pay for subscription.

3. You will see the subscription price in the amount of the chosen cryptocurrency and the dollar equivalent. The course for calculation is taken realtime from Bitfinex.

4. When you send the required amount, click on the payment confirmation button. Instant distribution Invite link:  Invited users Is valid:  Joined in past 1 day: Joined in past 30 days: Joined in past 7 days: LTC Language Longer must be greater than shorter. Main menu Margin balance Margin balance info Margin balance: Message was sent to all bot users except you Messages will be sent to all bot users. Continue? Messages:  Mid-term autotrading Monday Name:  New posting chat New strategy No No, I changed my mind Nobody found Not uploaded Nothing found Object Object deleted Open position message Open position message:  Open position: Orders - READ and WRITE Page Periods: Permission added Permission removed Permissions Permissions:  Photo Please enter text which would be displayed on button Please fill required fields. Please forward any message to me from channel you want to connect. Please select autotrading strategy: Please select callback that you want to add: Please select payment currency: Please select subscription term: Please send me document Please send me photo Please send txid, so we can track you transaction. Please send {amount:.4f} {currency} ({usd_price:.0f} USD) to this wallet:

<b>{currency}</b>: <code>{address}</code> Positions Positions - READ and WRITE Posting chats Pyramiding: Referrals Remove all Remove all positions Remove permission Remove position Rename account Renew Renew subscription Required permissions: Reset Return to account Return to main menu Return user to start menu Saturday Save Select a language: Select autotrading type: Select days using buttons below Select permission to add to {user}. Select permission to remove from {user}. Select subscription term: Selected days:
 Sell message:  Send me new caption for photo. Send me text message or photo with or without description. Send position ratio Send positions ratio:  Send withdraw request Short term Short term autotrading Short-term Short-term autotrading Show only invalid Show only valid Slow must be greater than fast. Sorry unknown command. Statistics Stop position message Stop position message:  Strategies Strategy Strategy deleted Subscription added Subscription inactive Subscription renewed Subscription to signals Sunday Text Thank you!

We will check your transaction and contact you soon.
For any support contact @inprofit_admin or @artemium. Thank you!

We withdraw money manually, so it may take a while. As soon as transaction reaches blockchain we will contact you.

For any support contact @inprofit_admin or @artemium. That pair already exists. The referral program menu shows your referral link, a list of invited users and earnings from their subscriptions.
{line}
To invite a friend, send him your referral link. You will receive 20% of the money paid by your invite.

 There is no help for this menu. If you need help here, write @inprofit_admin. There is nothing to list There is nothing to show This field allow only document. This field allow only photo. This field does not allow upload photo or document. This is Accounts menu of InProfit Bot.
{line}
1. The "<b>Add Account</b>" button opens the menu for adding a Bitfinex exchange account.

2. Next are your accounts if you have one. Click the button with the name of the account to open the menu of viewing the account and activate the subscription for automatic trading. This is first Beta release. Everything works ok, but if you find any bugs, please report to @artemium or @inprofit_admin. This is first Beta release. Everything works ok, but if you find any bugs, please report to @artemium or @inprofit_admin.

Almost every menu has a description. When you enter somewhere you need help, use /help command. This is menu of autotrading subscription.
{line}
1. You see the buttons with the choice of subscription type. Short-term works on the hourly chart.

2. Next you choose the term of the subscription.

3. Next you choose the cryptocurrency, which you'd like to use to pay for the subscription.

4. You will see the subscription price in the amount of the chosen cryptocurrency and its dollar equivalent. The course for calculation takes from Bitfinex.

5. When you send the required amount, click on the payment confirmation button. This is menu of viewing your account.
{line}
1. The "<b>Exchange Wallet</b>" and "<b>Margin Wallet</b>" buttons open the menu to view more detailed information about your exchange wallets of the selected account.

2. The "<b>Yield Curve</b>" button shows the changes of the balance on the Margin wallet of the selected account.

3. The "<b>Subscription</b>" button opens the automatic trading subscription menu in the selected account.

4. The "<b>Positions</b>" button opens the list of current positions opened by the bot on the selected account.

5. The "<b>Rename account</b>" button allows you to change the name of the selected account.

6. The "<b>Delete</b>" button deletes the selected account. This is the main menu of InProfit Bot.
{line}
1. The "<b>Accounts</b>" button opens the menu of viewing and adding Bitfinex accounts.

2. The "<b>Subscribe to signals</b>" button opens the menu of viewing and activating subscription to the &lt;Private&gt; channel and chat.

3. The "<b>Referral program</b>" button opens the menu for viewing your referral statistics. There is also a button to request the withdrawal of funds earned through invitations.

4. The "<b>About project</b>" button opens the InProfit info menu.

5. The "<b>Language</b>" button opens the menu of changing the interface language. Currently available: Russian, English, Ukrainian. This will delete all your accounts and active subscriptions.

Are you sure that you want to disable your account? Thursday Timeframe: To buy short-term autotrade subscription you need to have 500-6000 USD on your account. Total:  Totally sure Trade Trailing stop percent Trailing stop: Tuesday USD Margin balance:  USDT Tether (Omni) Unknown Uploaded User {user} has all permissions User {user} has no permissions User:  Username:  Users in bot: Users that stop or block bot: Waiting... Wallets - READ We created profitable algorithms for crypto-trading.

You can work manually by following InProfit signals or connect your Bitfinex account to autotrading.

Short-term bot earns from +15% monthly.

You can read more about us by clicking "About InProfit" button. Wednesday Withdraw Wrong date Wrong input, try again Wrong subscription type Wrong txid, please try again Yes Yield curve You are invited by You are invited by Nobody. You disabled your account.
You can send me /start command to enter bot or use referral link to become someones referral. You don't have signals subscription You give withdraw permissions to account key. It's not needed for bot. You must add at least one message. You subscription active until You were restricted from using bot You were restricted to use this menu Your balance is not updated yet.

Try again in 5 minutes. \*Allow hold signals \*Buy message \*Chat ID \*Hold message \*Name \*Sell message found from of out of {ticker} position closed. {ticker} position removed. {type} subscription active until <code>{date}</code> ({days} days left) Project-Id-Version: 
PO-Revision-Date: 2019-09-30 20:25+0300
Last-Translator: Flowelcat <Flowelcat@gmail.com>
Language-Team: 
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.2.3
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
 *Адреса *Валюта *Частина депозиту *Швидкий період *Індикатор *Ключ *Лівий тікер *Довгий період *Ім'я *Пірамідінг *Правий тікер *Секретний ключ *Короткий період *Повільний період *Згладжування *Таймфрейм 180 днів 24 години 3 дні 30 днів 360 днів 7 днів 90 днів ? днів API недійсний. Про нас Про InProfit Аккаунт - ЗЧИТУВАННЯ Аккаунт видалений Назва аккаунта:  Аккаунти Інформація об аккаунтах Запустити Відкриті позиції:  Активная стратегія: Активно: Додати Додати аккаунт Додати все Додати монету Введіть нове повідомлення за допомогою кнопки нижче Додати дозвіл Додати передплату Додано Меню адміністратора: Всі позиції на аккаунті закриті. Всі дозволи надані Всі дозволи видалені Усі позиції видалено. Усі під'єднані монети Дозволити HOLD-сигнали:  Ви впевнені, що хочете закрити всі позиції аккаунту? Ві впевнені, що хочете закрити усі позиції? Ви впевнені щодо закриття позиції? Ви впевнені щодо видалення позиції? Дійсно видалити цей аккаунт? Ви впевнені, що хочете видалити цей об'єкт? Ви точно впевнені, що хочете відключити аккаунт?

Ця дія невідворотна! Передплата за авто-торгівлю BTC Назад Назад до редагування Повернутися до головного меню Баланс: Кнопки: Повідомлення про покупку:  Змінити частину депозита Графік ID чату:  Чат приєднаний до InProfit Bot

<i>Це повідомлення буде видалено через 5 секунд</i> Виберіть тип розсилки: Виберіть, що б ви хотіли зробити: Закрити Закрити всі позиції аккаунта Закрити повідомлення розсилки Закрити повідомлення розсилки. Не використовуйте, якщо додали більше одного повідомлення! Закрити позицію Монета Монету видалено Монети Відключити Видалити Частина депозиту: Деталі Відключити підписку Відключено Розсилка ETH Змінити Змінити описання Змінити канал для повідомлень Email:  Пусто Введіть Username, ID або Ім'я користувача: Помилка Exchange balance Exchange balance info Exchange balance: Закрити пошук П'ятниця Тут відображаються монети, під'єднані до автоматичної торгівлі на обраному аккаунті.
{line}
1. Кнопка "<b>Усі монети</b>" відкриває список монет, під'єднаних до автоматичної торгівлі на обраному аккаунті.

2. Стрілки переміщують вас меж сторінками монет. На сторінці кожної монети відображено поточну позицію та баланс монети на ваших гаманцях.

 Вітаю!

На вашому аккаунті <b>"{account_name}"</b> закінчилась передплата за {sub_type} авто-торгівлю.

Нам доведеться відключити авто-торгівлю на цьому аккаунті :(

Оновити передплату можна у меню Аккаунтів.

Ми чекаємо на ваше повернення :red_heart: Вітаю!

Ваша передплата за сигнали закінчується через {days_text}, {expire_date}.

Оновити передплату можна у головному меню бота.

Дякуємо :slightly_smiling_face: Вітаю!

Ваша передплата за сигнали закінчилась, і нам доведеться видали вас з групи :(

Оновити передплату можна у головному меню.

Чекаємо вашого повернення :red_heart: Вітаю!

Ваша передплата за сигнали закінчується через {days_text}, {expire_date}.

Оновити передплату можна у головному меню бота.

Дякуємо :slightly_smiling_face: Повідомлення про утримання позиції:  Я надіслав {}. ID:  Ви знаходитесь у меню передплати за сигнали та аналітику.
{line}
1. Виберіть термін передплати.

2. Виберіть криптовалюту, зручну для оплати.

3. Далі ви побачите ціну передплати у кількості вибраної криптовалюти та еквівалент в доларах. Курс для розрахунку береться в реальному часі з Bitfinex.

4. Коли відправите необхідну суму, натисніть на кнопку підтвердження оплати. Термінова розсилка Посилання для запрошення:  Запрошені користувачі Дійсний:  Під'єднались за останній день: Під'єднались за останні 30 днів: Під'єднались за останні 7 днів: LTC Мова Довгий період має бути більшим за короткий. Головне меню Margin balance Margin balance info Margin balance: Повідомлення було відправлено всім користувачам бота, окрім вас Повідомлення відправиться всім користувачам бота. Впевнені? Повідомлення:  Середньострокова авто-торгівля Понеділок Ім'я:  Новий канал для повідомлень Нова стратегія Ні Ні, не треба Нікого не знайдено Не завантажено Нічого не знайдено :( Об'єкт Об'єкт видалено Повідомлення про відкриту позицію Повідомлення про відкриту позицію:  Відкрита позиція: Ордери - ЗЧИТУВАННЯ та ЗАПИС Сторінка Періоди: Дозвіл надано Дозвіл видалений Дозволи Дозволи:  Фото Будь ласка, введіть текст, який буде відображатися на кнопці Будь ласка, заповніть необхідні поля. Перешліть повідомлення з каналу, який ви хочете під'єднати. Будь ласка виберіть стратегію авто-торгівлі: Будь ласка, виберіть дію, яку хочете додати: Виберіть зручну криптовалюту для оплати: Виберіть термін передплати: Будь ласка, завантажте файл Будь ласка, завантажте фото Будь-ласка, відправте TxID, щоб ми перевірили вашу оплату. Надішліть будь ласка {amount:.4f} {currency} ({usd_price:.0f} USD) на цей гаманець:

<b>{currency}</b>: <code>{address}</code> Позиції Позиції - ЗЧИТУВАННЯ та ЗАПИС Канали для повідомлень Пірамідінг: Реферальна програма Видалити все Видалити усі позиції Видалити дозвіл Видалити позицію Перейменувати аккаунт Оновити Оновити передплату Необхідні дозволи: Очистити Повернутися до аккаунту Повернутися до головного меню Повернутися до старт-меню Субота Зберегти Виберіть мову: Виберіть тип авто-торгівлі: Виберіть дні за допомогою кнопки нижче Вибрати дозвіл для користувача {user}. Виберіть дозвіл, що видалити у {user}. Виберіть термін передплати: Вибрані дні:
 Повідомлення про продаж:  Надішліть мені описання для фото. Надішліть мені текст чи фото. Відправляти співвідношення позицій Відправляти співвідношенная позицій:  Запит на вивід коштів Короткострокова Короткострокова авто-торгівля Короткострокова Короткострокова авто-торгівля Відображати тільки недійсні Відображати тільки дійсні Повільний період має бути більшим за швидкий. На жаль, ця команда невідома. Статистика Повідомлення про спрацьовування стоп-лосу Повідомлення про спрацьовування стоп-лосу:  Стратегії Стратегія Стратегію видалено Передплату додано Немає передплати Передплата оновлена Передплата за сигнали Неділя Текст Дякую!

Мі перевіримо вашу транзакцію та зв'яжемося якомога скоріше.
За будь-якою допомогою звертайтеся до @inprofit_admin або @artemium. Дякую!

Мі перевіримо вашу транзакцію та зв'яжемося якомога скоріше.
За будь-якою допомогою звертайтеся до @inprofit_admin або @artemium. Така торгова пара вже інсує. Меню реферальної програми відображає ваше реферальне посилання, список запрошених користувачів та ваш прибуток від запрошень.
{line}
Щоб запросити друга, відправте йому своє реферальне посилання. Ви отримаєте 20% від його оплати.
 Для цього меню немає додаткової інформації. Якщо вам потрібна допомога, зверніться до @inprofit_admin або @artemium. Тут нічого немає Тут ще нічого немає :( Це поле підтримує тільки файли. Це поле підтримує тільки фото. Це поле не підтримує завантаження фото або файлів. Це меню Аккаунтів.
{line}
1. Кнопка "<b>Додати аккаунт</b>" відкриває меню під'єднання аккаунта Bitfinex.

2. Далі на кнопках показано ваші аккаунти, якщо вони є. Натискання кнопки з назвою аккаунту відкриє меню перегляду аккаунту та підписки на авто-торгівлю. Це наша перша Бета-версія. Все працює, але якщо ви знайдете якісь помилки, зверніться будь ласка до @inprofit_admin або @artemium. Це наша перша Бета-версія. Все працює, але якщо ви знайдете якісь помилки, зверніться будь ласка до @inprofit_admin або @artemium.

Для більшості меню є тлумачення. Якщо ви заплутались, скористайтеся командою /help. 1. Оберіть тип авто-торгівлі. Короткостроковий алгоритм працює на годинному графіку.

2. Далі оберіть термін передплати.

3. Далі оберіть криптовалюту, зручну для оплати.

4. Ви побачите ціну передплати у кількості обраної криптовалюти та еквівалент у доларах. Курс для розрахунку підтягується у реальному часі з Bitfinex.

5. Коли ви відправите необхідні монети, натисніть на кнопку підтвердження оплати. Це меню перегляду вашого аккаунта.
{line}
1. Кнопки "<b>Exchange Wallet</b>" та "<b>Margin Wallet</b>" відкривають меню перегляду детальної інформації про ваші гаманці на обраному аккаунті.

2. Кнопка "<b>Крива прибутковості</b>" відображае зміни на Маржинальному гаманці обраного аккаунта.

3. Кнопка "<b>Передплата за авто-торгівлю</b>" відкриває меню передплати за авто-торгівлю на обраному аккаунті.

4. Кнопка "<b>Позиції</b>" відкриває список поточних позицій, відкритих ботом на обраному аккаунті.

5. Кнопка "<b>Перейменувати аккаунт</b>" дозволяє вам змінити назву аккаунта.

6. Кнопка "<b>Видалити</b>" закриває позиції бота і видаляє аккаунт з системи. Ви у головному меню InProfit Bot.
{line}
1. Кнопка "<b>Аккаунти</b>"відкриває меню перегляду та підключення аккаунтів Bitfinex.

2. Кнопка "<b>Сигнали</b>" відкриває меню перегляду та оплати передплати за доступ до закритого каналу та чату.

3. Кнопка "<b>Реферальная программа</b>" открывает меню просмотра вашей реферальной статистики. Там же кнопка вывода денег, заработанных благодаря приглашениям.

4. Кнопка "<b>О нас</b>" открывает меню просмотра информации об InProfit.

5. Кнопка "<b>Язык</b>" открывает меню изменения языка интерфейса. Сейчас доступны: английский, русский, украинский. Ми видалимо усі ваші аккаунти та передплати з бази даних.

Впевнені, що хочете відключити аккаунт? Четвер Таймфрейм: Щоб активувати переплату на короткострокову авто-торгівлю необхідно мати на аккаунті від 500 до 6000 USD. Всього:  Повністю впевнений Торгувати Процент трейлінг-стопа Трейлінг-стоп: Вівторок Margin balance в USD:  USDT Tether (Omni) Невідомо Завантажено Користувач {user} має всі дозволи Користувач {user} не має жодних дозволів Користувач:  Username:  Користувачі бота: Користувачі, що зупинили або заблокували бота: Чекаю введення... Гаманці - ЗЧИТУВАННЯ Ми розробили алгоритми, що заробляють на торгівлі криптовалютами. У вас є можливість працювати вручну, використовуючи сигнали InProfit, або під'єднати свій Bitfinex-аккаунт до авто-торгівлі.

Короткостроковий бот заробляє від +15% за місяць.

Прочитайте більше про нас по кнопці "Про InProfit". Середа Вивести гроші Невірний формат дати Введення некоректне, спробуйте ще раз Неправильний тип передплати Введення некоректне, спробуйте ще раз Так Крива дохідності Вас запросив користувач Ви запустили бота напряму, без реферального посилання. Ваш аккаунт відключено.
Можете відправити команду /start або скористайтеся реферальним посиланням вашого друга. У вас немає активної передплати за сигнали Ви дали дозвіл на виведення грошей. Будь ласка, не ризикуйте. Створіть новий ключ API без зайвих дозволів. Ви повинні додати принаймні одне повідомлення. Ваша передплата активна до У вас недостатньо прав для доступу до боту У вас недостатньо прав для доступу до цього меню Ваш баланс ще не оновлено.

Спробуйте будь ласка через 5 хвилин. \*Дозволити HOLD-сигнали \*Повідомлення по покупку \*ID чату \*Повідомлення про утримання позиції \*Назва \*Повідомлення про продаж знайдено з з із позицию закрито. {ticker} позициію видалено. {type} передплата активна до <code>{date}</code> ( ще {days} днів) 