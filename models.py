import datetime
import enum
import json
import logging
from time import sleep

import pytz
from binance.client import Client as BinanceClient
from binance.exceptions import BinanceAPIException
from bitfinex.rest.restv1 import Client as ClientV1, BitfinexException as BitfinexExceptionV1
from bitfinex.rest.restv2 import Client as ClientV2, BitfinexException as BitfinexExceptionV2
from sqlalchemy import Column, Integer, String, Boolean, DateTime, Float, ForeignKey, ARRAY, Enum, UniqueConstraint, event, Text, BigInteger, or_, func
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, object_session
from sqlalchemy.util import classproperty
from telegram import TelegramError

from settings import SETTINGS_FILE

logger = logging.getLogger(__name__)

# Testing connection
for i in range(1, 10):
    try:
        from botmanlib.models import ModelPermissionsBase, Database, UserSessionsMixin, BaseUserSession

        break
    except OperationalError:
        logger.warning(f"SQL server not running, retrying, attempt {i}...")
        sleep(4)

from botmanlib.models import BaseUser, BasePermission, UserPermissionsMixin
from botmanlib.menus.helpers import generate_underscore, get_settings

database = Database()
Base = database.Base


class StrategyType(enum.Enum):
    ao = 'ao'
    ma2oc = 'ma2oc'
    han_solo_jr = 'han_solo_jr'
    han_solo_sr = 'han_solo_sr'
    han_solo_nodir = 'han_solo_nodir'

    def get_name(self):
        if self == StrategyType.ao:
            return "AO"
        elif self == StrategyType.ma2oc:
            return "MA2OC"
        elif self == StrategyType.han_solo_jr:
            return "Han Solo JR"
        elif self == StrategyType.han_solo_sr:
            return "Han Solo SR"
        elif self == StrategyType.han_solo_nodir:
            return "Han Solo NoDir"
        else:
            return "Unknown"


class SignalAction(enum.Enum):
    buy = 'buy'
    sell = 'sell'
    hold = 'hold'


class Side(enum.Enum):
    BUY = 'BUY'
    SELL = 'SELL'


class PositionType(enum.Enum):
    real = 'real'
    fake = 'fake'


class PositionStatus(enum.Enum):
    pending = 'pending'
    partially_opened = 'partially_opened'
    opened = 'opened'
    closed = 'closed'
    stopped = 'stopped'


class OrderOwner(enum.Enum):
    bot = 'bot'
    bitfinex = 'bitfinex'
    binance = 'binance'


class SubscriptionType(enum.Enum):
    short_term = 'short-term'


class SignalSubscriptionType(enum.Enum):
    bitfinex_short_term = 'bitfinex_short_term'
    binance_short_term = 'binance_short_term'


class Provider(enum.Enum):
    BITFINEX = 'BITFINEX'
    BINANCE = 'BINANCE'


class User(Base, BaseUser, UserPermissionsMixin, UserSessionsMixin, ModelPermissionsBase):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    banned = Column(Boolean, default=False)
    accounts = relationship("Account", back_populates="user", cascade='all, delete')
    referrer_id = Column(Integer, ForeignKey('users.id'))
    referrer = relationship("User", back_populates="referrals", remote_side=[id], uselist=False)
    referrals = relationship("User", back_populates="referrer", remote_side=[referrer_id])
    balance = Column(Float, default=0, nullable=False)
    subscription_id = Column(Integer, ForeignKey('signals_subscriptions.id'))
    subscription = relationship("SignalsSubscription", back_populates='user', cascade='all,delete')

    @property
    def disabled(self):
        return not self.active and not self.accounts

    def ban(self, bot):
        _ = generate_underscore(self)
        data = get_settings(SETTINGS_FILE)
        for chat_id in [c['id'] for c in data['signals_chats']]:
            try:
                bot.kick_chat_member(chat_id=chat_id, user_id=self.chat_id)
            except TelegramError as e:
                logger.error(e)

        message_text = _("Your signals subscription expired.")
        self.banned = True
        bot.send_message(chat_id=self.chat_id, text=message_text)

    def unban(self, bot):
        data = get_settings(SETTINGS_FILE)
        for chat_id in [c['id'] for c in data['signals_chats']]:
            try:
                bot.unban_chat_member(chat_id=chat_id, user_id=self.chat_id)
            except TelegramError as e:
                logger.error(e)

        self.banned = False

    def has_permission(self, code):
        permission_codes = [p.code for p in self.permissions]
        return code in permission_codes or 'superuser' in permission_codes

    def init_permissions(self):
        for permission in ['start_menu_access',
                           'change_language_menu_access',
                           Account.edit_permission,
                           Account.list_permission,
                           Account.view_permission,
                           Account.add_permission,
                           AutotradeSubscription.view_permission,
                           Account.yield_curve_permission,
                           'subscribe_to_signals_menu_access',
                           'referrals_menu_access',
                           'about_menu_access',
                           Position.view_permission,
                           Position.close_permission,
                           Position.close_all_permission,
                           Position.remove_permission,
                           Position.remove_all_permission,
                           Coin.view_permission,
                           Coin.list_permission,
                           Account.disable_permission,
                           'withdraw_menu_access']:
            perm = DBSession.query(Permission).get(permission)
            self.permissions.append(perm)


class Permission(BasePermission, Base):
    __tablename__ = 'permissions'


class UserSession(BaseUserSession, Base):
    __tablename__ = 'user_sessions'


class PostingChat(Base, ModelPermissionsBase):
    __tablename__ = 'posting_chats'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    chat_id = Column(String, nullable=False)
    allow_hold_signals = Column(Boolean, default=False)
    send_positions_ratio = Column(Boolean, default=False, nullable=False)
    stop_position_message = Column(Text)
    open_position_message = Column(Text)
    buy_message = Column(Text, nullable=False)
    sell_message = Column(Text, nullable=False)
    hold_message = Column(Text)

    account_id = Column(Integer, ForeignKey('accounts.id'), nullable=False)
    account = relationship('Account', back_populates='posting_chats', uselist=False)


class SignalsSubscription(Base):
    __tablename__ = 'signals_subscriptions'

    id = Column(Integer, primary_key=True)
    type = Column(Enum(SignalSubscriptionType), nullable=False)
    expire_date = Column(DateTime, nullable=False)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    price = Column(Float, default=0, nullable=False)

    user = relationship('User', back_populates='subscription', uselist=False)

    @property
    def is_active(self):
        return self.expire_date > datetime.datetime.utcnow()

    @property
    def days(self):
        return (self.expire_date - datetime.datetime.utcnow()).days


class AutotradeSubscription(Base, ModelPermissionsBase):
    __tablename__ = 'autotrade_subscriptions'

    id = Column(Integer, primary_key=True)
    expire_date = Column(DateTime, nullable=False)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    type = Column(Enum(SubscriptionType), nullable=False)
    price = Column(Float, default=0, nullable=False)

    account = relationship('Account', back_populates='subscription', uselist=False)

    @property
    def is_active(self):
        return self.expire_date > datetime.datetime.utcnow()

    @property
    def days(self):
        return (self.expire_date - datetime.datetime.utcnow()).days


class Account(Base, ModelPermissionsBase):
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True)
    api_key = Column(String)
    secret = Column(String)
    name = Column(String)
    email = Column(String)
    is_valid = Column(Boolean, default=False)
    creation_date = Column(DateTime, default=datetime.datetime.utcnow)

    coins = relationship("Coin", back_populates='account', cascade='all,delete')
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", back_populates="accounts")
    wallets = relationship("Wallet", back_populates='account', cascade='all,delete')
    posting_chats = relationship("PostingChat", back_populates='account', cascade='all,delete')
    subscription_id = Column(Integer, ForeignKey('autotrade_subscriptions.id'))
    subscription = relationship("AutotradeSubscription", back_populates='account', cascade='all,delete')
    orders = relationship("Order", back_populates='account', cascade='all,delete')
    positions = relationship("Position", back_populates='account', cascade='all,delete')
    provider = Column(Enum(Provider), nullable=False)

    @classproperty
    def yield_curve_permission(cls):
        return f"{cls.__tablename__}_yield_curve_menu_access"

    @classproperty
    def disable_permission(cls):
        return f"allow_disable_{cls.__tablename__}"

    def check_permissions(self):
        if self.provider is Provider.BITFINEX:
            permissions = self.get_bitfinex_key_permissions()
            if not permissions['account']['read'] or \
                    not permissions['orders']['read'] or \
                    not permissions['orders']['write'] or \
                    not permissions['positions']['read'] or \
                    not permissions['positions']['write'] or \
                    not permissions['wallets']['read']:
                return False
            return True
        elif self.provider is Provider.BINANCE:
            permissions = self.get_binance_key_permissions()
            if not permissions['trading']:
                return False
            return True

    def get_bitfinex_key_permissions(self):
        try:
            clientv1 = ClientV1(key=self.api_key, secret=self.secret)
            permissions = clientv1.key_permissions()

            return permissions
        except BitfinexExceptionV1:
            return {'account': {'read': False, 'write': False}, 'history': {'read': False, 'write': False}, 'orders': {'read': False, 'write': False}, 'positions': {'read': False, 'write': False}, 'funding': {'read': False, 'write': False}, 'wallets': {'read': False, 'write': False},
                    'withdraw': {'read': False, 'write': False}}

    def get_binance_key_permissions(self):
        try:
            client = BinanceClient(self.api_key, self.secret)
            account_info = client.get_margin_account()
            return {"trading": account_info['tradeEnabled'], "transfer": account_info['transferEnabled']}
        except BinanceAPIException:
            return {"trading": False, "withdraws": False, "margin": False}

    def check_account(self):
        if self.provider is Provider.BITFINEX:
            try:
                clientv2 = ClientV2(key=self.api_key, secret=self.secret)
                response = clientv2._post("v2/auth/r/info/user", json.dumps({}), verify=True)
                if self.email is None:
                    self.email = response[1]

                if not self.check_permissions():
                    self.is_valid = False
                    return

                self.is_valid = True
            except BitfinexExceptionV2:
                self.is_valid = False
        elif self.provider is Provider.BINANCE:
            try:
                if not self.check_permissions():
                    self.is_valid = False
                    return

                self.is_valid = True
            except BinanceAPIException:
                self.is_valid = False

    def get_usd_balance(self, wallet_type):
        wallets = DBSession.query(Wallet) \
            .filter(Wallet.type == wallet_type) \
            .filter(Wallet.active == True) \
            .filter(Wallet.account_id == self.id).all()

        balance = 0

        for wallet in wallets:
            balance += wallet.get_usd_balance()

        return balance

    def deactivate_all_wallets(self, session=None):
        if session is None:
            session = DBSession

        for wallet in self.wallets:
            wallet.active = False
            session.add(wallet)

        session.commit()


class Wallet(Base):
    __tablename__ = 'wallets'

    id = Column(Integer, primary_key=True)
    type = Column(String)
    currency = Column(String)
    active = Column(Boolean, default=True)

    account_id = Column(Integer, ForeignKey('accounts.id'), nullable=False)
    account = relationship('Account', back_populates='wallets', uselist=False)

    wallet_changes = relationship('WalletChange', back_populates='wallet', cascade='all,delete')

    __table_args__ = (UniqueConstraint('type', 'currency', 'account_id', name='_type_currency_account_uc'),)

    @classmethod
    def from_list(cls, wallet_l):
        wallet = Wallet()
        wallet.type = wallet_l[0]
        wallet.currency = wallet_l[1]

        return wallet

    def get_usd_balance(self):
        if self.wallet_changes:
            lwc = sorted(self.wallet_changes, key=lambda wc: wc.create_date)[-1]
            if self.currency == "USD" or self.currency == "USDT":
                return lwc.balance
            else:
                ticker_query = DBSession.query(Ticker) \
                    .filter(Ticker.provider == self.account.provider)
                if self.account.provider is Provider.BITFINEX:
                    ticker = ticker_query.filter(Ticker.symbol == f"{self.currency}USD").first()
                elif self.account.provider is Provider.BINANCE:
                    ticker = ticker_query.filter(Ticker.symbol == f"{self.currency}USDT").first()
                else:
                    ticker = None

                if ticker is None:
                    return 0
                return lwc.balance * ticker.last_price
        return 0

    def get_balance(self):
        if self.wallet_changes:
            lwc = sorted(self.wallet_changes, key=lambda wc: wc.create_date)[-1]
            return lwc.balance
        else:
            return 0

    def __repr__(self):
        return str([self.type, self.currency])


class WalletChange(Base):
    __tablename__ = 'wallet_changes'

    id = Column(Integer, primary_key=True)
    balance = Column(Float)
    unsettled_interest = Column(Float)
    balance_available = Column(Float)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    wallet_id = Column(Integer, ForeignKey('wallets.id'), nullable=False)
    wallet = relationship('Wallet', back_populates="wallet_changes", uselist=False)

    @classmethod
    def from_list(cls, wallet_l):
        wallet_change = WalletChange()
        wallet_change.balance = wallet_l[2]
        wallet_change.unsettled_interest = wallet_l[3]
        wallet_change.balance_available = wallet_l[4]
        return wallet_change


class Coin(Base, ModelPermissionsBase):
    __tablename__ = 'coins'

    id = Column(Integer, primary_key=True)
    left_ticker = Column(String)
    right_ticker = Column(String)
    deposit_part = Column(Float)
    strategies = relationship("Strategy", back_populates='coin', cascade='all,delete')
    account_id = Column(Integer, ForeignKey('accounts.id'))
    account = relationship("Account", back_populates='coins', uselist=False)

    @classproperty
    def oninvalid_permission(cls):
        return f"{cls.__tablename__}_oninvalid_menu_access"

    @classproperty
    def view_deppart_permission(cls):
        return f"allow_view_deppart_{cls.__tablename__}"

    @classproperty
    def edit_deppart_permission(cls):
        return f"allow_edit_deppart_{cls.__tablename__}"

    @hybrid_property
    def ticker(self):
        return f"{self.left_ticker}{self.right_ticker}"

    @ticker.expression
    def ticker(cls):
        return func.concat(cls.left_ticker, cls.right_ticker)

    def active_strategy(self, session=None):
        if session is None:
            session = object_session(self)

        return session.query(Strategy).filter(Strategy.coin_id == self.id).filter(Strategy.active == True).first()

    def get_balance(self, wallet_type):
        wallet = DBSession.query(Wallet) \
            .filter(Wallet.currency == self.left_ticker.upper()) \
            .filter(Wallet.account_id == self.account_id) \
            .filter(Wallet.type == wallet_type) \
            .first()
        if wallet:
            return wallet.get_balance()
        else:
            return 0

    @classmethod
    def existing_tickers(cls):
        coins = DBSession.query(Coin).all()
        return [f"{coin.ticker}" for coin in coins]

    @property
    def provider(self):
        if self.account:
            return self.account.provider


class Strategy(Base, ModelPermissionsBase):
    __tablename__ = 'strategies'
    id = Column(Integer, primary_key=True)
    indicator = Column(Enum(StrategyType))
    periods = Column(ARRAY(Integer))
    timeframe = Column(Integer)
    trailing_stop_perc = Column(Float)
    active = Column(Boolean, default=False)
    pyramiding = Column(Integer, default=1)
    coin_id = Column(Integer, ForeignKey('coins.id'))
    coin = relationship("Coin", back_populates='strategies', uselist=False)
    signals = relationship("Signal", back_populates='strategy')

    @classproperty
    def activate_permission(cls):
        return f"allow_activate_{cls.__tablename__}"

    @classproperty
    def deactivate_permission(cls):
        return f"allow_deactivate_{cls.__tablename__}"

    @classproperty
    def view_chart_permission(cls):
        return f"allow_view_chart_{cls.__tablename__}"

    @property
    def price_coef(self):
        return self.coin.deposit_part / self.pyramiding

    @property
    def trailing_stop_coef(self):
        return self.trailing_stop_perc / 100 if self.trailing_stop_perc else None

    @property
    def name(self):
        if self.periods and self.indicator and self.periods and self.timeframe:
            periods_s = [str(x) for x in self.periods]
            name = f"{self.indicator.get_name()} {'/'.join(periods_s)} {(str(self.timeframe // 60) + 'h' if self.timeframe >= 60 and self.timeframe % 60 == 0 else str(self.timeframe) + 'm')}"
            return name
        else:
            return "Unknown"

    @property
    def htimeframe(self):
        if self.timeframe:
            return str(self.timeframe // 60) + 'h' if self.timeframe >= 60 and self.timeframe % 60 == 0 else str(self.timeframe) + 'm'
        else:
            return "Unknown"

    def activate(self, session=None):
        if session is None:
            session = object_session(self)
        previous_active = session.query(Strategy).join(Strategy.coin).filter(Coin.id == self.coin_id).filter(Strategy.active == True).first()
        if previous_active:
            previous_active.deactivate(session)

        self.active = True
        session.add(self)

    def deactivate(self, session=None, deletion=False):
        if session is None:
            session = object_session(self)

        open_positions = session.query(Position) \
            .join(Position.signal) \
            .join(Signal.strategy) \
            .filter(Strategy.id == self.id) \
            .filter(Position.is_active == True).all()
        for position in open_positions:
            position.close_position(session=session)

        if not deletion:
            self.active = False
            session.add(self)

    def active_position(self, session=None):
        if session is None:
            session = object_session(self)

        return session.query(Position) \
            .join(Position.signal) \
            .join(Signal.strategy) \
            .filter(Strategy.id == self.id) \
            .filter(Position.is_active == True) \
            .first()

    @property
    def provider(self):
        if self.coin:
            return self.coin.provider


@event.listens_for(Strategy, 'before_delete')
def receive_before_delete(mapper, connection, target):
    target.deactivate(deletion=True)


class Signal(Base):
    __tablename__ = 'signals'

    id = Column(Integer, primary_key=True)
    indicator_value = Column(ARRAY(Float))
    action = Column(Enum(SignalAction))
    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    strategy_id = Column(Integer, ForeignKey('strategies.id'))
    strategy = relationship("Strategy", back_populates="signals")
    positions = relationship("Position", back_populates="signal", cascade='all,delete')


class BitfinexCandle(Base):
    __tablename__ = 'bitfinex_candles'

    id = Column(Integer, primary_key=True)
    open_date = Column(BigInteger, nullable=False)
    open = Column(Float, nullable=False)
    close = Column(Float, nullable=False)
    high = Column(Float, nullable=False)
    low = Column(Float, nullable=False)
    volume = Column(Float, nullable=False)
    ticker = Column(String, nullable=False)
    __table_args__ = (UniqueConstraint('open_date', 'ticker', name='_bfx_opendate_ticker_uc'),)

    def to_dict(self):
        return {
            'open': self.open,
            'close': self.close,
            'high': self.high,
            'low': self.low,
            'open_date': self.open_date,
            'volume': self.volume,
            'ticker': self.ticker
        }

    @property
    def open_date_dt(self):
        date = datetime.datetime.utcfromtimestamp(self.open_date / 1e3)
        return date.replace(tzinfo=pytz.utc)

    @classmethod
    def from_list(cls, candle, ticker=None):
        return BitfinexCandle(open=candle[1],
                              close=candle[2],
                              high=candle[3],
                              low=candle[4],
                              volume=candle[5],
                              open_date=candle[0],
                              ticker=ticker)

    def add_or_update(self, session=None):
        if session is None:
            logger.warning(f"Session not set for {self}")
            return

        candle_obj = session.query(BitfinexCandle).filter(BitfinexCandle.open_date == self.open_date).filter(BitfinexCandle.ticker == self.ticker).first()
        if candle_obj:
            candle_obj.open = self.open
            candle_obj.close = self.close
            candle_obj.high = self.high
            candle_obj.low = self.low
            candle_obj.volume = self.volume
            candle_obj.ticker = self.ticker
            session.add(candle_obj)
        else:
            session.add(self)

    @classmethod
    def has_candle(cls, open_date, ticker, session=None):
        if session is None:
            session = DBSession
        return session.query(session.query(BitfinexCandle).filter(BitfinexCandle.open_date == open_date).filter(BitfinexCandle.ticker == ticker).exists()).scalar()

    def __repr__(self):
        return "Candle(open={open}, close={close}, high={high}, low={low}, open_date={open_date}, volume={volume})" \
            .format(open=self.open, close=self.close, high=self.high, low=self.low, open_date=self.open_date_dt.strftime("%d.%m.%Y %H:%M:%S"), volume=self.volume)


class BinanceCandle(Base):
    __tablename__ = 'binance_candles'

    id = Column(Integer, primary_key=True)
    open_date = Column(BigInteger, nullable=False)
    close_date = Column(BigInteger, nullable=False)
    open = Column(Float, nullable=False)
    close = Column(Float, nullable=False)
    high = Column(Float, nullable=False)
    low = Column(Float, nullable=False)
    volume = Column(Float, nullable=False)
    quote_asset_volume = Column(Float, nullable=False)
    trades = Column(Integer, nullable=False)
    ticker = Column(String, nullable=False)
    __table_args__ = (UniqueConstraint('open_date', 'ticker', name='_bncc_opendate_ticker_uc'),)

    def to_dict(self):
        return {
            'open': self.open,
            'close': self.close,
            'high': self.high,
            'low': self.low,
            'open_date': self.open_date,
            'volume': self.volume,
            'ticker': self.ticker
        }

    @property
    def open_date_dt(self):
        date = datetime.datetime.utcfromtimestamp(self.open_date / 1e3)
        return date.replace(tzinfo=pytz.utc)

    @property
    def close_date_dt(self):
        date = datetime.datetime.utcfromtimestamp(self.close_date / 1e3)
        return date.replace(tzinfo=pytz.utc)

    @classmethod
    def from_list(cls, candle, ticker=None):
        return BinanceCandle(open_date=candle[0],
                             open=candle[1],
                             high=candle[2],
                             low=candle[3],
                             close=candle[4],
                             volume=candle[5],
                             close_date=candle[6],
                             quote_asset_volume=candle[7],
                             trades=candle[8],
                             ticker=ticker)

    @classmethod
    def from_dict(cls, candle, ticker=None):
        return BinanceCandle(open_date=candle['t'],
                             close_date=candle['T'],
                             open=candle['o'],
                             close=candle['c'],
                             high=candle['h'],
                             low=candle['l'],
                             volume=candle['v'],
                             quote_asset_volume=candle['q'],
                             trades=candle['n'],
                             ticker=ticker)

    def add_or_update(self, session=None):
        if not session:
            session = DBSession

        candle_obj = session.query(BinanceCandle).filter(BinanceCandle.open_date == self.open_date).filter(BinanceCandle.ticker == self.ticker).first()
        if candle_obj:
            candle_obj.open = self.open
            candle_obj.close = self.close
            candle_obj.high = self.high
            candle_obj.low = self.low
            candle_obj.volume = self.volume
            candle_obj.quote_asset_volume = self.quote_asset_volume
            candle_obj.trades = self.trades
            candle_obj.ticker = self.ticker
            session.add(candle_obj)
        else:
            session.add(self)

    @classmethod
    def has_candle(cls, open_date, ticker, session=None):
        if session is None:
            session = DBSession
        return session.query(session.query(BinanceCandle).filter(BinanceCandle.open_date == open_date).filter(BinanceCandle.ticker == ticker).exists()).scalar()

    def __repr__(self):
        return "Candle(open={open}, close={close}, high={high}, low={low}, open_date={open_date}, volume={volume}, quote_asset_volume={quote_asset_volume}, close_date={close_date}, trades={trades})" \
            .format(open=self.open,
                    close=self.close,
                    high=self.high,
                    low=self.low,
                    open_date=self.open_date_dt.strftime("%d.%m.%Y %H:%M:%S"),
                    volume=self.volume,
                    quote_asset_volume=self.quote_asset_volume,
                    close_date=self.close_date_dt.strftime("%d.%m.%Y %H:%M:%S"),
                    trades=self.trades)


class Ticker(Base):
    __tablename__ = 'tickers'

    id = Column(Integer, primary_key=True)
    symbol = Column(String)
    bid = Column(Float)
    ask = Column(Float)
    last_price = Column(Float)
    update_date = Column(DateTime, default=datetime.datetime.utcnow)
    provider = Column(Enum(Provider), nullable=False)
    __table_args__ = (UniqueConstraint('symbol', name='_symbol_uc'),)


class Position(Base, ModelPermissionsBase):
    __tablename__ = 'positions'
    id = Column(Integer, primary_key=True)
    provider_id = Column(BigInteger)
    symbol = Column(String, nullable=False)
    type = Column(Enum(PositionType))
    amount = Column(Float)
    full_amount = Column(Float)
    base_price = Column(Float)
    trailing_stop_price = Column(Float)
    level = Column(Integer, default=1)
    profit = Column(Float)
    profit_computed = Column(Float)
    profit_perc = Column(Float)
    price_liq = Column(Float)
    status = Column(Enum(PositionStatus), default=PositionStatus.pending)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    signal_id = Column(Integer, ForeignKey('signals.id'))
    signal = relationship("Signal", back_populates="positions", uselist=False)
    account_id = Column(Integer, ForeignKey('accounts.id'), nullable=False)
    account = relationship("Account", back_populates='positions', uselist=False)

    order_id = Column(Integer, ForeignKey('orders.id'))
    order = relationship("Order", back_populates="position", uselist=False)

    @classproperty
    def oninvalid_permission(cls):
        return f"{cls.__tablename__}_oninvalid_menu_access"

    @classproperty
    def remove_permission(cls):
        return f"allow_remove_{cls.__tablename__}"

    @classproperty
    def remove_all_permission(cls):
        return f"allow_remove_all_{cls.__tablename__}"

    @classproperty
    def close_permission(cls):
        return f"allow_close_{cls.__tablename__}"

    @classproperty
    def close_all_permission(cls):
        return f"allow_close_all_{cls.__tablename__}"

    @hybrid_property
    def is_active(self):
        return self.status is PositionStatus.pending or self.status is PositionStatus.partially_opened or self.status is PositionStatus.opened

    @is_active.expression
    def is_active(cls):
        return (cls.status == PositionStatus.pending) | (cls.status == PositionStatus.partially_opened) | (cls.status == PositionStatus.opened)

    def close_market(self, session=None):
        if session is None:
            session = object_session(self)

        account = session.query(Account).filter(Account.id == self.account_id).first()
        if not account:
            logger.error(f"Can't close position {self.id} because associated account does not exists")
            return
        if not account.is_valid:
            logger.error(f"Can't close position {self.id} because associated account is invalid")
            return
        if not self.provider_id:
            logger.error(f"Can't close position {self.id} because provider id is not set")
            return

        try:
            client_v1 = ClientV1(account.api_key, account.secret)
            close_message = client_v1.close_position(self.provider_id)
            logger.debug(f"Close position provider message: {close_message}")
            self.status = PositionStatus.stopped
        except BitfinexExceptionV1 as e:
            logger.error(f"Can't close position because provider error occurred:\n{str(e)}")

    def close_position(self, session=None):
        if session is None:
            session = object_session(self)
        self.status = PositionStatus.closed
        session.add(self)
        session.commit()

    def stop_position(self, session=None):
        if session is None:
            session = object_session(self)
        self.status = PositionStatus.stopped
        session.add(self)
        session.commit()

    @hybrid_property
    def side(self):
        if self.full_amount > 0:
            return Side.BUY
        if self.full_amount < 0:
            return Side.SELL

    @hybrid_property
    def actual_side(self):
        if self.amount > 0:
            return Side.BUY
        if self.amount < 0:
            return Side.SELL

    @property
    def provider(self):
        if self.account:
            return self.account.provider

    def __repr__(self):
        amount_txt = "{:.5f}".format(self.amount).replace('.', ',')
        price_txt = "{:.3f}".format(self.base_price).replace('.', ',')

        return "{ticker}: {type}{delimiter}{side}{delimiter}{amount}{delimiter}${price}{delimiter}${trailing_stop_price}{delimiter}{level}{delimiter}{status}{delimiter}{date} UTC".format(ticker=self.symbol,
                                                                                                                                                                                           type=self.type.value if self.type else " - ",
                                                                                                                                                                                           side=self.actual_side.value if self.actual_side else " - ",
                                                                                                                                                                                           status=self.status.value if self.status else " - ",
                                                                                                                                                                                           date=self.create_date.strftime('%d %b %H:%M'),
                                                                                                                                                                                           amount=amount_txt,
                                                                                                                                                                                           price=price_txt,
                                                                                                                                                                                           trailing_stop_price=str(self.trailing_stop_price),
                                                                                                                                                                                           level=self.level,
                                                                                                                                                                                           delimiter=" | ")

    @property
    def name(self):
        return str(self)


class Order(Base):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True)
    cid = Column(String, nullable=False)
    provider_id = Column(BigInteger, nullable=False)
    type = Column(String)
    price = Column(Float, nullable=False)
    price_avg = Column(Float)
    amount = Column(Float, default=0)
    amount_orig = Column(Float, default=0)
    symbol = Column(String, nullable=False)
    status = Column(String)
    owner = Column(Enum(OrderOwner), default=OrderOwner.bot)
    account_id = Column(Integer, ForeignKey('accounts.id'))
    account = relationship("Account", back_populates='orders', uselist=False)
    position = relationship("Position", back_populates='order', uselist=False)

    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    close_date = Column(DateTime)

    @hybrid_property
    def is_active(self):
        return self.status and (self.status.startswith("ACTIVE") or self.status.startswith("PARTIALLY FILLED"))

    @is_active.expression
    def is_active(cls):
        return or_(cls.status.like('ACTIVE%'), cls.status.like('PARTIALLY FILLED%'))

    @hybrid_property
    def side(self):
        if self.amount > 0:
            return Side.BUY
        if self.amount < 0:
            return Side.SELL
        return None


sessionmaker = database.sessionmaker
DBSession = database.DBSession
