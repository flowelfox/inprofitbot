import argparse
import datetime

from backend.bitfinex.candle_controller import CandleFormer
from indicators.ao import AO
from indicators.hmacci import HMACCI
from indicators.hmadir import HMADIR
from indicators.hmao import HMAO
from indicators.ma2oc import MA2OC
from models import DBSession

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test indicators')
    parser.add_argument('-t', dest="pair", type=str, help='trading pair', required=True)
    parser.add_argument('-p', dest="periods", type=int, nargs='+', help='periods', required=True)
    parser.add_argument('-tf', dest="timeframe", type=int, help='timeframe', required=True)
    parser.add_argument('-e', dest="end", type=str, help='End time')
    parser.add_argument('-u', dest="update", action='store_true', help='Update candles')

    execute_time = datetime.datetime.utcnow()
    args = parser.parse_args()
    print(args)

    pair = args.pair
    timeframe = args.timeframe
    end_time = datetime.datetime.strptime(args.end, "%d.%m.%Y %H:%M") if args.end else None
    if not isinstance(args.periods, list):
        periods = [args.periods]
    else:
        periods = args.periods

    cf = CandleFormer(timeframe, periods[0] * 2, pair, end_time=end_time, session=DBSession)
    if args.update:
        cf.request_candles(cf.start_time())
    end_time = cf.end_time
    candles = cf.get_candles()

    ao = AO(periods[0], periods[1])
    for candle in reversed(candles):
        ao.add(candle.to_dict())
    ao_indicator_value = ao.value()

    ma2oc = MA2OC(periods[0])
    for candle in reversed(candles):
        ma2oc.add(candle.to_dict())
    ma2oc_indicator_value = ma2oc.value()

    hmao = HMAO(periods[0], periods[1])
    for candle in reversed(candles):
        hmao.add(candle.to_dict())
    hmao_indicator_value = hmao.value()

    hmacci = HMACCI(periods[1])
    for candle in reversed(candles):
        hmacci.add(candle.to_dict())
    hmacci_indicator_value = hmacci.value()

    hmadir = HMADIR(periods[0])
    for candle in reversed(candles):
        hmadir.add(candle.to_dict())
    hmadir_indicator_value = hmadir.value()

    print(f"Pair: {pair}")
    print(f"Timeframe: {str(timeframe)}")
    print(f"Periods: {', '.join([str(p) for p in periods])}")
    print(f"StartTime: {cf.start_time().strftime('%d.%m.%Y %H:%M:%S')}")
    print(f"EndTime: {end_time.strftime('%d.%m.%Y %H:%M:%S')}")
    print(f"AO: {ao_indicator_value}")
    print(f"MA2OC: {ma2oc_indicator_value}")
    print(f"HMAO: {hmao_indicator_value}")
    print(f"HMACCI: {hmacci_indicator_value}")
    print(f"HMADIR: {hmadir_indicator_value}")

    print(f'\nCalculations took {(datetime.datetime.utcnow() - execute_time).total_seconds()} seconds')
