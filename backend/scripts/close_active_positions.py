import logging

from bitfinex import WssClient
from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from models import DBSession, Position, Signal, Strategy, Coin

logger = logging.getLogger(__name__)

def main():
    open_positions = DBSession.query(Position) \
            .join(Position.signal) \
            .join(Signal.strategy) \
            .join(Strategy.coin) \
            .join(Coin.account) \
            .filter(Position.pair_position_id == None).all()

    websockets = {}
    positions_closed = 0
    if not open_positions:
        logger.warning("There is no open positions.")
        return
    for position in open_positions:
        account = position.signal.strategy.coin.account
        if account.id in websockets:
            ws = websockets[account.id]
        else:
            ws = WssClient(key=account.api_key, secret=account.secret)
            websockets[account.id] = ws

        position.close_position(ws)

        positions_closed += 1

    for ws in websockets.values():
        ws.close()

    try:
        DBSession.commit()
    except (SQLAlchemyError, IntegrityError) as e:
        DBSession.rollback()
        logger.critical(f"Database error {str(e)}")
    logger.info(f"Closed {positions_closed} positions.")


if __name__ == '__main__':
    main()