import logging
from signal import signal, SIGINT, SIGTERM, SIGABRT
from time import sleep

from backend.controllers.bitfinex.candle_controller import CandleController as BFXCandleController
from backend.controllers.bitfinex.signal_controller import SignalController as BFXSignalController

from backend.controllers.binance.candle_controller import CandleController as BNCCandleController
from backend.controllers.binance.signal_controller import SignalController as BNCSignalController

from backend.helpers import get_signal_name

logger = logging.getLogger(__name__)


class Backend:
    def __init__(self):
        self.running = False
        self.is_idle = False
        self.bitfinex_signal_controller = BFXSignalController()
        self.bitfinex_candle_controller = BFXCandleController()

        self.binance_signal_controller = BNCSignalController()
        self.binance_candle_controller = BNCCandleController()

    def signal_handler(self, signum, *args, **kwargs):
        self.is_idle = False
        if self.running:
            logger.info('Received signal {} ({}), stopping...'.format(
                signum, get_signal_name(signum)))
            self.stop()
        else:
            logger.warning('Exiting immediately!')
            exit(1)

    def idle(self, stop_signals=(SIGINT, SIGTERM, SIGABRT)):
        for sig in stop_signals:
            signal(sig, self.signal_handler)

        self.is_idle = True

        while self.is_idle:
            sleep(1)

    def start(self):
        self.bitfinex_candle_controller.start()
        self.bitfinex_signal_controller.start()

        self.binance_candle_controller.start()
        self.binance_signal_controller.start()
        self.running = True

    def stop(self):
        self.bitfinex_candle_controller.stop()
        self.bitfinex_signal_controller.stop()

        self.binance_candle_controller.stop()
        self.binance_signal_controller.stop()
        sleep(2)  # wait until all websockets closes
        from twisted.internet import reactor
        reactor.stop()


def main():
    backend = Backend()
    backend.start()

    backend.idle()


if __name__ == '__main__':
    main()
