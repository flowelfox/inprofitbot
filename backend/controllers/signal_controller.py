import datetime
import logging
import traceback
from abc import ABC
from threading import Event, Lock, Thread
from time import sleep

import pytz
from math import ceil
from sqlalchemy.orm.exc import ObjectDeletedError

from backend.helpers import add_to_db, get_enum, datetime_to_minutes, thread_wrapper
from indicators.ao import AO
from indicators.hmacci import HMACCI
from indicators.hmadir import HMADIR
from indicators.hmao import HMAO
from indicators.ma2oc import MA2OC
from models import Strategy, Signal, StrategyType, SignalAction, Ticker, database, Coin, Account
from settings import ALLOW_TRADE


class BaseSignalController(ABC):
    account_controller_class = None
    signal_former_class = None
    candle_former_class = None
    MINIMUM_ORDER_SIZE = None
    PROVIDER = None
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.running = False
        self.is_idle = False
        self.strategies = []
        self.session = database.sessionmaker()
        self.account_controller = self.account_controller_class(self.session)
        self.__stop = Event()
        self.__lock = Lock()
        self.__threads = []

    def update_strategies(self):
        strategies = self.session.query(Strategy)\
            .join(Strategy.coin)\
            .join(Coin.account)\
            .filter(Account.provider == self.PROVIDER)\
            .filter(Strategy.active == True)\
            .all()
        self.strategies = strategies

    def is_time_to_check(self, strategy):
        now = datetime.datetime.utcnow()

        rule_1 = datetime_to_minutes(now) % strategy.timeframe < 20
        sig = self.session.query(Signal).filter(Signal.strategy == strategy).order_by(Signal.create_date.desc()).first()

        if sig:
            cf = self.candle_former_class(strategy.timeframe, max(strategy.periods), strategy.coin.ticker, session=self.session)
            rule_3 = sig.create_date < cf.end_time.replace(tzinfo=None)
        else:
            rule_3 = True
        result = rule_1 and rule_3

        if result:
            self.logger.debug("It's time to check")
        return result

    def wait_signals(self):
        def wait(pause):
            for i in range(pause):
                sleep(1)
                if self.__stop.is_set():
                    break

        while not self.__stop.is_set():
            for strategy in self.strategies:
                try:
                    sf = self.signal_former_class(strategy, session=self.session)
                    if self.is_time_to_check(strategy):
                        sig = sf.create_signal()
                        self.logger.info(f"Strategy {strategy.coin.account.name}-{strategy.coin.ticker}-{strategy.name} received signal {sig.action.value} with indicator value {sig.indicator_value}.")

                        if not sf.has_missing_signals():
                            balance = strategy.coin.account.get_usd_balance('margin')
                            price_coef = strategy.price_coef
                            ticker = self.session.query(Ticker)\
                                .filter(Ticker.provider == self.PROVIDER) \
                                .filter(Ticker.symbol == strategy.coin.ticker)\
                                .first()
                            if ticker is None:
                                self.logger.critical(f"Can't complete buy because can't find {strategy.coin.ticker} ticker")
                                continue

                            if sig.action is SignalAction.buy:
                                amount = (balance * price_coef) / ticker.bid
                                order_usd_size = balance * price_coef
                                if strategy.coin.account.is_valid and ALLOW_TRADE and order_usd_size >= self.MINIMUM_ORDER_SIZE:
                                    self.account_controller.buy(strategy.coin.account, strategy.coin.ticker, amount, sig=sig)
                                else:
                                    self.account_controller.fake_buy(strategy.coin.account, strategy.coin.ticker, amount, sig=sig)

                            elif sig.action is SignalAction.sell:
                                amount = (balance * price_coef) / ticker.ask
                                order_usd_size = balance * price_coef
                                if strategy.coin.account.is_valid and ALLOW_TRADE and order_usd_size >= self.MINIMUM_ORDER_SIZE:
                                    self.account_controller.sell(strategy.coin.account, strategy.coin.ticker, amount, sig=sig)
                                else:
                                    self.account_controller.fake_sell(strategy.coin.account, strategy.coin.ticker, amount, sig=sig)

                            self.account_controller.send_telegram_message(sig)

                except ObjectDeletedError:
                    self.logger.info(f"Some strategy deleted")
                except Exception as e:
                    error_text = str(e) + "\n"
                    error_text += traceback.format_exc()
                    self.logger.error(error_text)

            wait(5)
            self.update_strategies()

        self.running = False

    def _stop(self):
        self.__stop.set()
        while self.running:
            sleep(0.1)
        self.__stop.clear()

    def stop(self):
        with self.__lock:
            self.account_controller.stop()

            if self.running:
                self._stop()

            for thr in self.__threads:
                thr.join()
            self.__threads = []

    def start(self):
        with self.__lock:
            self.update_strategies()
            self.signal_former_class.generate_all_missing_signals(self.session)
            self.account_controller.start()

            if not self.running:
                self.running = True
                self._init_thread(self.wait_signals, "wait_signals")
                self.logger.info("Signal controller started.")

    def _init_thread(self, target, name, *args, **kwargs):
        thr = Thread(target=thread_wrapper, name=name, args=(target,) + args, kwargs=kwargs)
        thr.start()
        self.__threads.append(thr)


class BaseSignalFormer(ABC):
    candle_former_class = None
    PROVIDER = None
    logger = logging.getLogger(__name__)

    def __init__(self, strategy, end_time=None, session=None):
        self.strategy = strategy
        self.cf = self.candle_former_class(strategy.timeframe, max(strategy.periods) * 2, strategy.coin.ticker, end_time, session=session)
        back_days = ceil((self.strategy.timeframe * max(self.strategy.periods) + (4 * self.strategy.timeframe)) / 1440)
        if back_days < 1:
            back_days = 1
        now = datetime.datetime.utcnow().replace(second=0, microsecond=0)
        self.back_date = now - datetime.timedelta(days=back_days)
        self.session = session

    def create_signal(self):
        new_signal = Signal(indicator_value=self.compute_indicator(), action=self.create_action(), strategy=self.strategy, create_date=self.cf.end_time.replace(tzinfo=None))
        add_to_db(new_signal)
        return new_signal

    def has_missing_signals(self, shift=-1):
        try:
            signals = self.session.query(Signal).filter(Signal.strategy == self.strategy).order_by(Signal.create_date.desc()).limit(3).all()
        except ObjectDeletedError:
            return True

        if signals and len(signals) >= 3:
            cf = self.candle_former_class(self.strategy.timeframe, 3 + shift, self.strategy.coin.ticker, self.cf.end_time, session=self.session)
            start_time = cf.start_time()
            for s in signals:
                if s.create_date.replace(tzinfo=pytz.utc) < start_time:
                    result = True
                    break
            else:
                result = False
        else:
            result = True

        if result:
            self.logger.warning(f"{self.strategy.coin.ticker} has less than 3 signals")

        return result

    def compute_indicator(self):
        strategy_type = get_enum(self.strategy.indicator, StrategyType, 'strategy_type')
        candles = self.cf.get_candles()

        if strategy_type is StrategyType.ao:
            ao = AO(self.strategy.periods[0], self.strategy.periods[1], source='hl2')
            for candle in reversed(candles):
                ao.add(candle.to_dict())
            indicator_value = ao.value()
            return indicator_value,

        elif strategy_type is StrategyType.ma2oc:
            ma2oc = MA2OC(self.strategy.periods[0])
            for candle in reversed(candles):
                ma2oc.add(candle.to_dict())
            indicator_value = ma2oc.value()
            return indicator_value,

        elif strategy_type is StrategyType.han_solo_jr or strategy_type is StrategyType.han_solo_sr or strategy_type is StrategyType.han_solo_nodir:
            hmao = HMAO(self.strategy.periods[0], self.strategy.periods[1])
            for candle in reversed(candles):
                hmao.add(candle.to_dict())
            hmao_value = hmao.value()

            hmacci = HMACCI(self.strategy.periods[1])
            for candle in reversed(candles):
                hmacci.add(candle.to_dict())
            hmacci_value = hmacci.value()

            hmadir = HMADIR(self.strategy.periods[0])
            for candle in reversed(candles):
                hmadir.add(candle.to_dict())
            hmadir_value = hmadir.value()
            return hmao_value, hmacci_value, hmadir_value
        else:
            raise TypeError("Invalid strategy type")

    def create_action(self):
        if self.has_missing_signals(0):
            return SignalAction.hold
        else:
            indicator_values = self.compute_indicator()

            strategy_type = get_enum(self.strategy.indicator, StrategyType, 'strategy_type')
            action = None
            last_not_hold_signal = self.session.query(Signal)\
                .join(Signal.strategy)\
                .filter(Signal.strategy == self.strategy)\
                .filter(Signal.action != SignalAction.hold)\
                .order_by(Signal.create_date.desc())\
                .first()
            last_signal = self.session.query(Signal)\
                .join(Signal.strategy)\
                .filter(Signal.strategy == self.strategy)\
                .order_by(Signal.create_date.desc())\
                .first()

            if strategy_type is StrategyType.ao:
                indicator_value = indicator_values[0]
                if indicator_value < last_signal.indicator_value[0]:
                    action = SignalAction.sell
                elif indicator_value > last_signal.indicator_value[0]:
                    action = SignalAction.buy
                elif indicator_value == last_signal.indicator_value[0]:
                    action = SignalAction.hold

                # pyramiding logic
                if last_not_hold_signal is not None and self.strategy.active_position() is not None \
                        and action is SignalAction.hold \
                        and self.strategy.pyramiding > 1 \
                        and self.strategy.active_position().level < self.strategy.pyramiding:
                    action = last_not_hold_signal.action
                elif last_not_hold_signal is not None and self.strategy.active_position() is not None \
                        and self.strategy.active_position().level >= self.strategy.pyramiding \
                        and action == last_not_hold_signal.action:
                    action = SignalAction.hold

            elif strategy_type is StrategyType.ma2oc:
                indicator_value = indicator_values[0]
                if ((last_not_hold_signal is not None and last_not_hold_signal.action == SignalAction.buy) or last_not_hold_signal is None) and last_signal.indicator_value[0] >= 0 and indicator_value < 0:
                    action = SignalAction.sell
                elif ((last_not_hold_signal is not None and last_not_hold_signal.action == SignalAction.sell) or last_not_hold_signal is None) and last_signal.indicator_value[0] <= 0 and indicator_value > 0:
                    action = SignalAction.buy
                else:
                    action = SignalAction.hold

                # pyramiding logic
                if last_not_hold_signal is not None \
                        and action is SignalAction.hold \
                        and self.strategy.pyramiding > 1 \
                        and self.strategy.active_position().level < self.strategy.pyramiding:
                    action = last_not_hold_signal.action

            elif strategy_type is StrategyType.han_solo_jr:
                hmao, hmacci, hmadir = indicator_values

                if (hmacci > 0 and (hmao > last_signal.indicator_value[0])) or \
                        (hmacci > 0 and hmadir > 0) or \
                        ((hmao > last_signal.indicator_value[0]) and hmadir > 0) or \
                        (hmacci > 0 and (hmao > last_signal.indicator_value[0]) and hmadir > 0):
                    action = SignalAction.buy
                elif (hmacci < 0 and (hmao < last_signal.indicator_value[0])) or \
                        (hmacci < 0 and hmadir < 0) or \
                        ((hmao < last_signal.indicator_value[0]) and hmadir < 0) or \
                        (hmacci < 0 and (hmao < last_signal.indicator_value[0]) and hmadir < 0):
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

                if action is not SignalAction.hold and last_not_hold_signal is not None and action == last_not_hold_signal.action:
                    action = SignalAction.hold

                # pyramiding logic
                if last_not_hold_signal is not None \
                        and action is SignalAction.hold \
                        and self.strategy.pyramiding > 1 \
                        and self.strategy.active_position() \
                        and self.strategy.active_position().level < self.strategy.pyramiding:
                    action = last_not_hold_signal.action

            elif strategy_type is StrategyType.han_solo_sr:
                hmao, hmacci, hmadir = indicator_values

                if (hmacci > 0 and hmao > 0) or \
                        (hmacci > 0 and hmadir > 0) or \
                        (hmao > 0 and hmadir > 0) or \
                        (hmacci > 0 and hmao > 0 and hmadir > 0):
                    action = SignalAction.buy
                elif (hmacci < 0 and hmao < 0) or \
                        (hmacci < 0 and hmadir < 0) or \
                        (hmao < 0 and hmadir < 0) or \
                        (hmacci < 0 and hmao < 0 and hmadir < 0):
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

                if action is not SignalAction.hold and last_not_hold_signal is not None and action == last_not_hold_signal.action:
                    action = SignalAction.hold

                # pyramiding logic
                if last_not_hold_signal is not None \
                        and action is SignalAction.hold \
                        and self.strategy.pyramiding > 1 \
                        and self.strategy.active_position() \
                        and self.strategy.active_position().level < self.strategy.pyramiding:
                    action = last_not_hold_signal.action

            elif strategy_type is StrategyType.han_solo_nodir:
                hmao, hmacci, _ = indicator_values

                if hmacci > 0 and (hmao > last_signal.indicator_value[0]):
                    action = SignalAction.buy
                elif hmacci < 0 and (hmao < last_signal.indicator_value[0]):
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

                if action is not SignalAction.hold and last_not_hold_signal is not None and action == last_not_hold_signal.action:
                    action = SignalAction.hold

                # pyramiding logic
                if last_not_hold_signal is not None \
                        and action is SignalAction.hold \
                        and self.strategy.pyramiding > 1 \
                        and self.strategy.active_position() \
                        and self.strategy.active_position().level < self.strategy.pyramiding:
                    action = last_not_hold_signal.action

            return action

    def generate_signals(self, interrupt=Event()):
        interrupt.set()
        if self.has_missing_signals():
            self.logger.info(f"Getting missed candles for signal generation for {self.strategy.coin.ticker}, {self.strategy.name}. With back days = {datetime_to_minutes(self.back_date) // 1440}")
            self.cf.request_candles(self.back_date, interrupt=interrupt, session=self.session)

        generated_signals = 0

        end_time = None
        while interrupt.is_set() and self.has_missing_signals():
            if end_time is not None:
                self.cf.end_time = end_time
                self.cf.candles = []

            self.create_signal()
            generated_signals += 1
            candles = self.cf.get_candles()
            try:
                end_time = candles[0].open_date_dt
            except IndexError:
                return generated_signals

        self.cf.end_time = None
        if generated_signals > 0:
            self.logger.info(f"Generated {generated_signals} signals for {self.strategy.coin.ticker}, {self.strategy.name}.")
        interrupt.clear()
        return generated_signals

    @classmethod
    def generate_all_missing_signals(cls, session=None):
        strategies = session.query(Strategy).filter(Strategy.active).all()
        all_generated_signals_count = 0
        for strategy in strategies:
            sf = cls(strategy, session=session)
            all_generated_signals_count += sf.generate_signals()

        if not all_generated_signals_count:
            cls.logger.info("All needed signals already existed.")
