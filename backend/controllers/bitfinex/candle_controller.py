import datetime
import logging

import pytz
from bitfinex import WssClient

from backend.controllers.candle_controller import BaseCandleController, BaseCandleFormer
from backend.helpers import convert_timeframe_to_string, datetime_to_minutes
from models import BitfinexCandle, sessionmaker, Ticker, Provider
from settings import SLEEP_AFTER_BITFINEX_REQUEST, BITFINEX_TICKER_PAIRS, BITFINEX_AVAILABLE_PAIRS
from bitfinex import ClientV2

K = 1000


class CandleFormer(BaseCandleFormer):
    candle_class = BitfinexCandle
    SLEEP_AFTER_REQUEST = SLEEP_AFTER_BITFINEX_REQUEST
    PROVIDER = Provider.BITFINEX
    logger = logging.getLogger(__name__)

    request_start_time = 0
    request_end_time = 0

    def combine_candle(self, candle_group):
        candle_date_minutes = datetime_to_minutes(candle_group[0].open_date_dt)
        open_date = datetime.datetime.utcfromtimestamp((candle_group[0].open_date - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % self.timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)

        new_candle = self.candle_class(
            open=candle_group[-1].open,
            close=candle_group[0].close,
            high=max([candle.high for candle in candle_group]),
            low=min([candle.low for candle in candle_group]),
            volume=sum([candle.volume for candle in candle_group]),
            ticker=self.ticker,
            open_date=int(open_date.timestamp() * K),
        )
        return new_candle

    def request_raw_candles(self, newest_open_date):
        client = ClientV2()
        return client.candles('1m', f"t{self.ticker}", 'hist', limit=1000, start=int(pytz.timezone('Europe/Kiev').fromutc(datetime.datetime.utcfromtimestamp(newest_open_date // K)).timestamp() * K), sort=1)


class CandleController(BaseCandleController):
    candle_class = BitfinexCandle
    candle_former_class = CandleFormer
    ticker_pairs = BITFINEX_TICKER_PAIRS
    candles_pairs = BITFINEX_AVAILABLE_PAIRS
    PROVIDER = Provider.BITFINEX
    logger = logging.getLogger(__name__)

    def create_websocket_client(self):
        return WssClient()

    def subscribe_candle(self, ws, ticker):
        cl = CandleListener(ticker)
        ws.subscribe_to_candles(f't{ticker}', convert_timeframe_to_string(1), cl.new_candle_info)

    def subscribe_ticker(self, ws, ticker):
        tl = TickerListener(ticker)
        ws.subscribe_to_ticker(ticker, tl.new_ticker_info)


class CandleListener:
    def __init__(self, ticker):
        self.type = None
        self.timeframe = None
        self.ticker = ticker
        self.session = sessionmaker()
        self.logger = logging.getLogger(self.__class__.__name__)

    def new_candle_info(self, message):
        if isinstance(message, dict) and message.get('key'):  # subscribe info
            self.type, self.timeframe, _ = message.get('key').split(':')
        elif isinstance(message, list) and message[1] == 'hb':  # heartbeat
            pass
        elif self.type and self.timeframe and self.ticker and message:
            if isinstance(message, list) and len(message) >= 2 and isinstance(message[1][0], list):  # list of 240 last candles
                candles = [BitfinexCandle.from_list(candle_info, ticker=self.ticker) for candle_info in reversed(message[1])]
                for candle in candles:
                    candle.add_or_update(self.session)
                self.session.commit()

            elif isinstance(message, list) and len(message) >= 2 and isinstance(message[1][0], int):  # new candle
                candle = BitfinexCandle.from_list(message[1], ticker=self.ticker)
                candle.add_or_update(self.session)
                self.session.commit()


class TickerListener:
    def __init__(self, symbol):
        self.symbol = symbol
        self.logger = logging.getLogger(self.__class__.__name__)
        self.session = sessionmaker()

    def new_ticker_info(self, message):
        if isinstance(message, list) and isinstance(message[1], list):
            ticker = self.session.query(Ticker).filter(Ticker.symbol == self.symbol).first()
            if ticker is None:
                ticker = Ticker()

            ticker.provider = Provider.BITFINEX
            ticker.symbol = self.symbol
            ticker.ask = message[1][2]
            ticker.bid = message[1][0]
            ticker.last_price = message[1][6]
            ticker.update_date = datetime.datetime.utcnow()
            self.session.add(ticker)

            self.session.commit()
