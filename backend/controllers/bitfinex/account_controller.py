import datetime
import logging
from time import sleep

from bitfinex import WssClient
from bitfinex.rest.restv2 import Client as ClientV2
from backend.controllers.account_controller import BaseAccountController
from models import Position, PositionType, Account, sessionmaker, Wallet, WalletChange, Order, PositionStatus, Provider, OrderOwner
from settings import ALLOW_TRADE, BITFINEX_AVAILABLE_PAIRS


class AccountController(BaseAccountController):
    AVAILABLE_PAIRS = BITFINEX_AVAILABLE_PAIRS
    PROVIDER = Provider.BITFINEX
    logger = logging.getLogger(__name__)

    def update_orders(self, account):
        # get newest orders info
        client = ClientV2(key=account.api_key, secret=account.secret)
        active_orders = []
        orders = client.orders_history()
        for order in orders:
            order_obj = self.session.query(Order) \
                .filter(Order.cid == str(order[2])) \
                .filter(Order.account_id == account.id) \
                .first()
            if not order_obj:
                order_obj = Order(cid=order[2], account_id=account.id, owner=OrderOwner.bitfinex)
            order_obj.provider_id = order[0]
            order_obj.create_date = datetime.datetime.utcfromtimestamp(order[4] / 1e3)
            order_obj.symbol = order[3][1:]
            order_obj.amount = order[6]
            order_obj.amount_orig = order[7]
            order_obj.type = order[8]
            order_obj.status = order[13]
            order_obj.price = order[16]
            order_obj.price_avg = order[17]
            if order_obj.status.startswith("EXECUTED") or order_obj.status.startswith("CANCELED"):
                order_obj.close_date = datetime.datetime.utcnow()
            if order_obj.status == 'ACTIVE':
                active_orders.append(order_obj)

            self.session.add(order_obj)
        self.session.commit()
        return active_orders

    def create_account_ws(self, account):
        account_ws = WssClient(key=account.api_key, secret=account.secret)
        al = AccountListener(account.id, self.orders)
        account_ws.authenticate(al.new_account_info, filters=['wallet', 'trading', 'notify'])
        account_ws.start()
        self.auth_websockets[account.id] = account_ws
        return account_ws

    def create_orderbook_ws(self):
        self.orderbook_ws = WssClient()
        for ticker in BITFINEX_AVAILABLE_PAIRS:
            bl = BookListener(ticker, self.auth_websockets, self.orders)
            self.orderbook_ws.subscribe_to_orderbook(f"t{ticker}", "R0", bl.new_book_info)
        self.orderbook_ws.start()

    def close_order(self, account, provider_id):
        account_ws = self.auth_websockets[account.id]
        account_ws.cancel_order(provider_id)

    def create_buy_order(self, account, ticker, amount, price):
        account_ws = self.auth_websockets[account.id]
        cid = account_ws.new_order("LIMIT", f"t{ticker}", f"{amount:.8f}", price=f"{price:.8f}")
        return cid

    def create_sell_order(self, account, ticker, amount, price):
        account_ws = self.auth_websockets[account.id]
        cid = account_ws.new_order("LIMIT", f"t{ticker}", f"-{amount:.8f}", price=f"{price:.8f}")
        return cid


class AccountListener:
    def __init__(self, account_id, orders):
        self.session = sessionmaker()
        self.account_id = account_id
        self.account = self.session.query(Account) \
            .filter(Account.provider == Provider.BITFINEX) \
            .filter(Account.id == self.account_id) \
            .first()
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}")
        self.orders = orders

    def wallet_update(self, message):
        self.logger.debug(f"Bitfinex wallet websocket triggered for {self.account.name}")

        if message[1] == 'ws':
            wallets = message[2]
            self.account.deactivate_all_wallets(self.session)
        else:
            wallets = [message[2]]

        for wallet in wallets:
            wallet_obj = self.session.query(Wallet) \
                .filter(Wallet.type == wallet[0]) \
                .filter(Wallet.account == self.account) \
                .filter(Wallet.currency == wallet[1]).first()

            if wallet_obj is None:
                wallet_obj = Wallet.from_list(wallet)
                wallet_obj.account = self.account
                wallet_obj.active = True
                self.session.add(wallet_obj)
                wallet_change = None
            else:
                wallet_obj.active = True
                self.session.add(wallet_obj)
                wallet_change = self.session.query(WalletChange) \
                    .filter(WalletChange.wallet_id == wallet_obj.id) \
                    .filter(WalletChange.balance == wallet[2]) \
                    .filter(WalletChange.unsettled_interest == wallet[3]) \
                    .filter(WalletChange.balance_available == wallet[4]).first()

            if wallet_change is None:
                wallet_change = WalletChange.from_list(wallet)
                wallet_change.wallet = wallet_obj
                self.session.add(wallet_change)

        self.session.commit()

    def order_update(self, message):
        self.logger.debug(f"Bitfinex order websocket triggered for {self.account.name}")
        if message[1] == 'os' or message[1] == 'hos':
            orders = message[2]
        else:
            orders = [message[2]]

        for order in orders:
            retries = 0
            while retries < 3:
                order_obj = self.session.query(Order).filter(Order.cid == str(order[2])).filter(Order.close_date == None).first()
                if order_obj and (order[13].startswith("EXECUTED") or order[13].startswith("CANCELED") or order_obj.status is None):
                    order_obj.account_id = self.account_id
                    order_obj.provider_id = order[0]
                    order_obj.create_date = datetime.datetime.utcfromtimestamp(order[4] / 1e3)
                    order_obj.symbol = order[3][1:]
                    order_obj.amount = order[6]
                    order_obj.amount_orig = order[7]
                    order_obj.type = order[8]
                    order_obj.status = order[13]
                    order_obj.price = order[16]
                    order_obj.price_avg = order[17]

                    self.session.add(order_obj)
                    self.session.commit()
                    self.logger.debug(f"Order with cid {order_obj.cid} updated")

                    if order_obj.status.startswith("EXECUTED") or order_obj.status.startswith("CANCELED"):
                        # remove order from memory
                        order_obj.close_date = datetime.datetime.utcnow()
                        for ord in self.orders.get(order_obj.symbol, []):
                            if ord.cid == order_obj.cid:
                                self.orders[order_obj.symbol].remove(ord)
                                break
                    else:
                        # add oder to memory
                        self.session.expunge(order_obj)
                        self.orders[order_obj.symbol].append(order_obj)
                    break
                else:
                    retries += 1
                    sleep(0.5)

    def position_update(self, message):
        if message[1] == 'ps':
            positions = message[2]
        else:
            positions = [message[2]]

        for position in positions:
            position_query = self.session.query(Position) \
                .filter(Position.symbol == position[0][1:]) \
                .filter(Position.account_id == self.account_id) \
                .filter(Position.type == PositionType.real) \
                .order_by(Position.create_date.desc())

            # filter by side
            if position[2] > 0:
                side_position_query = position_query.filter(Position.full_amount > 0)
            elif position[2] < 0:
                side_position_query = position_query.filter(Position.full_amount < 0)
            else:
                side_position_query = position_query

            position_obj = side_position_query.first()
            # ignore side if position not found
            if position_obj is None:
                position_obj = position_query.first()

            # create position if it does not exists
            if position_obj is None:
                order_obj = self.session.query(Order) \
                    .filter(Order.symbol == position[0][1:]) \
                    .filter(Order.account_id == self.account_id) \
                    .order_by(Order.create_date.desc()) \
                    .first()
                if order_obj is None:
                    continue
                self.session.add(order_obj)
                position_obj = Position(account_id=self.account_id, type=PositionType.real, level=1, order=order_obj)

            # close previous position
            if position_obj.status == PositionStatus.pending and position_obj.amount == 0:
                last_positions = self.session.query(Position) \
                    .filter(Position.account_id == self.account_id) \
                    .filter(Position.symbol == position[0][1:]) \
                    .filter(Position.type == PositionType.real) \
                    .filter(Position.is_active == True) \
                    .order_by(Position.create_date.desc()) \
                    .limit(2) \
                    .all()
                if last_positions and len(last_positions) == 2:
                    previous_position = last_positions[-1]
                    previous_position.status = PositionStatus.closed
                    self.session.add(previous_position)

            position_obj.symbol = position[0][1:]
            position_obj.amount = position[2]
            position_obj.base_price = position[3]
            position_obj.profit = position[6]
            position_obj.profit_perc = position[7]
            position_obj.price_liq = position[8]

            # get position id
            if position_obj.provider_id is None and position_obj.status is not PositionStatus.pending and position_obj.status is not PositionStatus.partially_opened:
                client_v2 = ClientV2(self.account.api_key, self.account.secret)
                active_positions = client_v2.active_positions()
                for bitfinex_position in active_positions:
                    if bitfinex_position[0] == position[0]:
                        position_obj.provider_id = bitfinex_position[11]
                        break

            # determinate position status
            if position_obj.status is PositionStatus.stopped:
                pass
            elif position[1] == "CLOSED" or position_obj.amount == 0:
                position_obj.status = PositionStatus.closed
                self.logger.debug(f"Position {position_obj.id} closed")
            elif position_obj.order and not position_obj.order.is_active:
                position_obj.status = PositionStatus.opened
            elif position_obj.order and position_obj.order.is_active:
                position_obj.status = PositionStatus.partially_opened
            else:
                position_obj.status = PositionStatus.closed
                self.logger.debug(f"Position {position_obj.id} closed")

            self.session.add(position_obj)
            self.session.commit()

    def notification_update(self, message):
        notification = message[2]
        if notification[6] == "ERROR" or notification[6] == "FAILURE":
            if notification[1] == 'ou-req' and notification[7] == "order: invalid":  # removing invalid order
                orders = []
                [orders.extend(o) for o in self.orders.values()]
                for order in orders:
                    if order.provider_id == notification[4][0]:
                        self.orders[order.symbol].remove(order)
                        break
            else:
                self.logger.error(notification[7])

    def new_account_info(self, message):
        if self.account is None:
            return

        if isinstance(message, list) and len(message) > 2 and (message[1] == 'ws' or message[1] == 'wu'):
            self.wallet_update(message)

        if isinstance(message, list) and len(message) > 2 and (message[1] == 'os' or message[1] == 'on' or message[1] == 'ou' or message[1] == 'oc' or message[1] == 'hos'):
            self.order_update(message)

        if isinstance(message, list) and len(message) > 2 and (message[1] == 'ps' or message[1] == 'pn' or message[1] == 'pu' or message[1] == 'pc'):
            self.position_update(message)

        if isinstance(message, list) and len(message) > 2 and message[1] == 'n':
            self.notification_update(message)


class BookListener:
    def __init__(self, symbol, auth_websockets, orders):
        self.orders = orders
        self.auth_websockets = auth_websockets
        self.symbol = symbol
        self.sell_book = dict()
        self.buy_book = dict()
        self.now = int(datetime.datetime.utcnow().timestamp() * 1e3)
        self.session = sessionmaker()
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}")

    def new_book_info(self, message):
        """
        0 - order_id
        1 - price
        2 - amount
        """
        if isinstance(message, dict):
            pass
        else:
            if isinstance(message[1], list) and isinstance(message[1][0], list):
                for order in message[1][:25]:
                    self.buy_book.update({order[0]: order[1:]})

                for order in message[1][25:]:
                    self.sell_book.update({order[0]: order[1:]})

            elif isinstance(message[1], list):
                order = message[1]
                if order[1] == 0:
                    try:
                        if order[2] > 0:
                            del self.buy_book[order[0]]
                        if order[2] < 0:
                            del self.sell_book[order[0]]
                    except KeyError:
                        pass
                else:
                    if order[2] > 0:
                        self.buy_book.update({order[0]: order[1:]})
                    if order[2] < 0:
                        self.sell_book.update({order[0]: order[1:]})

                new_now = int(datetime.datetime.utcnow().timestamp() * 1e3)
                if new_now - self.now > 1000:
                    self.update_orders()
                    self.now = new_now
            else:
                pass

    def update_orders(self):
        target_position = 0
        orders = self.orders.get(self.symbol, [])
        if not orders:
            return

        sorted_buy_book = sorted(self.buy_book.values(), key=lambda x: x[0], reverse=True)
        sorted_sell_book = sorted(self.sell_book.values(), key=lambda x: x[0])
        if len(sorted_sell_book) > 20 and len(sorted_buy_book) > 20:
            for order in orders:
                top_price_buy = sorted_buy_book[target_position][0]
                top_price_sell = sorted_sell_book[target_position][0]
                if order.amount > 0:
                    price = top_price_buy - (1 / 10 ** len(str(top_price_buy).split('.')[-1]))
                    if order.price == top_price_buy:  # ignore when price is lowering
                        continue
                else:
                    price = top_price_sell + (1 / 10 ** len(str(top_price_sell).split('.')[-1]))
                    if order.price == top_price_sell:  # ignore when price is rising
                        continue

                if price != order.price and order.account_id in self.auth_websockets and ALLOW_TRADE:
                    ws = self.auth_websockets[order.account_id]
                    if ws.factories['auth'].protocol_instance is not None:  # is websocket already authenticated
                        ws.update_order(id=order.provider_id, price=f"{price:.8f}")
                        self.logger.debug(f"Changed order {order.id} from {order.price} to {price}.")
                        order.price = price
