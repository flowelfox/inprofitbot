import logging

from backend.controllers.signal_controller import BaseSignalController, BaseSignalFormer
from models import Provider
from .account_controller import AccountController
from .candle_controller import CandleFormer


class SignalFormer(BaseSignalFormer):
    candle_former_class = CandleFormer
    PROVIDER = Provider.BITFINEX
    logger = logging.getLogger(__name__)


class SignalController(BaseSignalController):
    account_controller_class = AccountController
    signal_former_class = SignalFormer
    candle_former_class = CandleFormer
    MINIMUM_ORDER_SIZE = 40
    PROVIDER = Provider.BITFINEX
    logger = logging.getLogger(__name__)
