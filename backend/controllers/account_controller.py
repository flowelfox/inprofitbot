import datetime
import logging
import os
import re
from abc import ABC
from threading import Event, Lock, Thread, Timer
from time import sleep

from bitfinex.rest.restv2 import BitfinexException
from telegram import Bot, TelegramError

from backend.helpers import thread_wrapper
from models import Signal, SignalAction, Ticker, Position, PositionType, Account, sessionmaker, Order, PositionStatus, Side, Provider

logger = logging.getLogger(__name__)


class BaseAccountController(ABC):
    AVAILABLE_PAIRS = []
    PROVIDER = None
    logger = logging.getLogger(__name__)

    def __init__(self, session):
        self.auth_websockets = {}
        self.orderbook_ws = None
        self.orders = {pair: [] for pair in self.AVAILABLE_PAIRS}

        self.bot = Bot(token=os.getenv("bot.token"))
        self.accounts_session = sessionmaker()
        self.positions_session = sessionmaker()
        self.subscribe_to_accounts_running = False
        self.position_updater_running = False
        self.session = session if session else sessionmaker()
        self._stop = Event()
        self.__lock = Lock()
        self.__threads = []
        self.__timers = {}

    def _wait(self, pause):
        for i in range(pause):
            sleep(1)
            if self._stop.is_set():
                break

    def create_account_ws(self, account):
        raise NotImplementedError

    def create_orderbook_ws(self):
        raise NotImplementedError

    def close_order(self, account, provider_id):
        raise NotImplementedError

    def create_buy_order(self, account, ticker, amount, price):
        raise NotImplementedError

    def create_sell_order(self, account, ticker, amount, price):
        raise NotImplementedError

    def update_orders(self, account):
        raise NotImplementedError

    def subscribe_to_accounts(self):
        while not self._stop.is_set():
            accounts = self.accounts_session.query(Account).filter(Account.provider == self.PROVIDER).all()
            for account in accounts:
                if account.id not in self.auth_websockets and account.is_valid:
                    active_orders = self.update_orders(account)
                    for order in active_orders:
                        self.orders[order.symbol].append(order)

                    # start account websocket
                    self.create_account_ws(account)
                    self.logger.info(f"Added account {account.name} to websockets")

            self._wait(5)

        self.subscribe_to_accounts_running = False

    def position_updater(self):
        while not self._stop.is_set():
            positions = self.positions_session.query(Position) \
                .join(Position.account) \
                .filter(Account.provider == self.PROVIDER) \
                .filter(Position.is_active == True) \
                .all()

            for position in positions:
                ticker = self.positions_session.query(Ticker) \
                    .filter(Ticker.provider == self.PROVIDER) \
                    .filter(Ticker.symbol == position.symbol).first()
                if ticker is None:
                    self.logger.warning(f"Ticker for {position.symbol} missing")
                    continue

                # update profit_computed
                if position.side is Side.BUY:
                    position.profit_computed = ((ticker.bid - position.base_price) / position.base_price * 100)
                elif position.side is Side.SELL:
                    position.profit_computed = ((position.base_price - ticker.ask) / position.base_price * 100)

                # update trailing stop price to cover possible loss
                if position.profit_computed and position.signal and position.signal.strategy and position.signal.strategy.trailing_stop_perc:
                    if position.profit_computed >= 3:
                        # update trailing stop price with 1/3 of profit
                        if position.side is Side.BUY:
                            trailing_stop_price = position.base_price + (position.base_price * ((position.profit_computed / 3) / 100))
                            if position.trailing_stop_price is None or (position.trailing_stop_price < trailing_stop_price):
                                position.trailing_stop_price = trailing_stop_price
                        if position.side is Side.SELL:
                            trailing_stop_price = position.base_price - (position.base_price * ((position.profit_computed / 3) / 100))
                            if position.trailing_stop_price is None or (position.trailing_stop_price > trailing_stop_price):
                                position.trailing_stop_price = trailing_stop_price
                    else:
                        # update trailing stop price with strategy stop perc
                        if position.side is Side.BUY:
                            trailing_stop_price = ticker.last_price - (position.signal.strategy.trailing_stop_coef * ticker.last_price)
                            if position.trailing_stop_price is None or (position.trailing_stop_price < trailing_stop_price):
                                position.trailing_stop_price = trailing_stop_price

                        if position.side is Side.SELL:
                            trailing_stop_price = ticker.last_price + (position.signal.strategy.trailing_stop_coef * ticker.last_price)
                            if position.trailing_stop_price is None or (position.trailing_stop_price > trailing_stop_price):
                                position.trailing_stop_price = trailing_stop_price

                # stop position when trailing stop price reached and there is no active/pending positions
                if ticker and position.trailing_stop_price is not None:
                    inprogress_position = self.positions_session.query(Position) \
                        .filter(Position.account_id == position.account_id) \
                        .filter(Position.symbol == ticker.symbol) \
                        .filter(Position.id != position.id) \
                        .filter(Position.is_active == True) \
                        .first()

                    if inprogress_position is None and \
                            ((position.side is Side.BUY and ticker.last_price <= position.trailing_stop_price) or
                             (position.side is Side.SELL and ticker.last_price >= position.trailing_stop_price)):
                        if position.type is PositionType.real:
                            position.close_market(self.positions_session)
                        else:
                            position.stop_position(self.positions_session)

                        self.send_position_stop_message(position)
                        self.logger.debug(f"Position {position.id} stopped with market")

                        self.positions_session.add(position)
                self.positions_session.commit()

            self._wait(2)
        self.position_updater_running = False

    def subscribe_to_orderbook(self):
        orders = self.session.query(Order).filter(Order.type == "LIMIT").filter(Order.is_active == True).all()
        for order in orders:
            if order.symbol in self.orders:
                self.orders[order.symbol].append(order)
            else:
                self.orders[order.symbol] = [order]

        self.create_orderbook_ws()

    def send_position_stop_message(self, position):
        account = position.account
        if account is None:
            logger.error("Can't send stop position message because position account is not present")
            return
        signal = position.signal
        if signal is None:
            logger.error("Can't send stop position message because position signal is not present")
            return
        strategy = signal.strategy
        if strategy is None:
            logger.error("Can't send stop position message because position strategy is not present")
            return
        ticker = self.positions_session.query(Ticker).filter(Ticker.symbol == position.symbol).first()
        bot = Bot(token=os.getenv("bot.token"))

        side = position.side.value if position.side else " - "
        message = {
            'left-ticker-upper': strategy.coin.left_ticker.upper() if signal else "",
            'left-ticker-lower': strategy.coin.left_ticker.lower() if signal else "",
            'left-ticker-capitalize': strategy.coin.left_ticker.capitalize() if signal else "",

            'right-ticker-upper': strategy.coin.right_ticker.upper() if signal else "",
            'right-ticker-lower': strategy.coin.right_ticker.lower() if signal else "",
            'right-ticker-capitalize': strategy.coin.right_ticker.capitalize() if signal else "",

            'side-upper': side.upper(),
            'side-lower': side.lower(),
            'side-capitalize': side.capitalize(),
            'base-price': position.base_price,
            'open-date': position.create_date.strftime('%d.%m.%Y %H:%M UTC'),
            'position-level': position.level,
            'profit': position.profit,
            'profit-computed': position.profit_computed,
            'profit-percent': position.profit_perc,
            'amount': position.amount,
            'amount-abs': abs(position.amount),
            'trailing-stop-price': position.trailing_stop_price,
            'price-liq': position.price_liq,
            'provider-id': position.provider_id,
            'type': position.type.value.upper(),

            'last-price': ticker.last_price,
            'strategy-name': strategy.name if signal else None,
            'strategy-pyramiding': strategy.pyramiding if signal else None,
            'indicator-value': ", ".join([f"{iv:.5f}" for iv in signal.indicator_value]),
            'account-name': account.name,
        }

        for chat in account.posting_chats:
            if not chat.stop_position_message:
                continue
            message_text = chat.stop_position_message

            message_text = message_text.replace(r"\_", "_").replace(r"\*", "*").replace(r"\[", "[").replace(r"\]", "]")
            for message_key in [m.group()[1:-1] for m in re.finditer(r'{[\w\-:+]+}', message_text)]:
                value = self._get_message_value(message, message_key)
                if value:
                    message_text = message_text.replace(f"{{{message_key}}}", value)

            try:
                chat_id = chat.chat_id if chat.chat_id.startswith("-100") else "-100" + chat.chat_id
                bot.send_message(chat_id=chat_id, text=message_text, parse_mode="MARKDOWN")

            except TelegramError as e:
                self.logger.error(f"Can't send message to channel because TelegramError occurred.\n {str(e)}")

    def send_positions_ratio(self, chat):
        del self.__timers[chat.id]
        if not chat.send_positions_ratio:
            return
        account = chat.account

        positions = []
        for coin in account.coins:
            active_strategy = coin.active_strategy(self.session)
            if active_strategy:
                active_position = active_strategy.active_position(self.session)
                if active_position:
                    positions.append(active_position)

        sell_positions = 0
        buy_positions = 0
        for position in positions:
            if position.side is Side.BUY:
                buy_positions += 1
            else:
                sell_positions += 1
        total_positions = sell_positions + buy_positions
        if total_positions == 0:
            return

        message_text = "<code>" + "BUY" + ':   </code>' + ((("▇" * buy_positions) + "<code> </code>") if buy_positions else "") + f"<code>{round(buy_positions / total_positions * 100):.0f}%</code>" + "\n"
        message_text += "<code>" + "SELL" + ':  </code>' + ((("▇" * sell_positions) + "<code> </code>") if sell_positions else "") + f"<code>{round(sell_positions / total_positions * 100):.0f}%</code>" + "\n"

        try:
            chat_id = chat.chat_id if chat.chat_id.startswith("-100") else "-100" + chat.chat_id
            self.bot.send_message(chat_id=chat_id, text=message_text, parse_mode="HTML")

        except TelegramError as e:
            self.logger.error(f"Can't send message to channel because TelegramError occurred.\n {str(e)}")

    def send_telegram_message(self, sig):
        """Sending telegram message"""

        account = sig.strategy.coin.account
        if account is None:
            logger.error("Can't send telegram message because position account is not present")
            return
        strategy = sig.strategy
        if strategy is None:
            logger.error("Can't send telegram message because position strategy is not present")
            return

        if not account.posting_chats:
            return

        old_open_position = self.session.query(Position) \
            .join(Position.account) \
            .join(Position.signal) \
            .filter(Account.provider == self.PROVIDER) \
            .filter(Signal.strategy_id == sig.strategy_id) \
            .filter(Position.is_active == False) \
            .order_by(Position.create_date.desc()) \
            .first()
        if old_open_position and old_open_position.status is PositionStatus.stopped:
            old_open_position = None

        current_open_position = self.session.query(Position) \
            .join(Position.account) \
            .join(Position.signal) \
            .filter(Account.provider == self.PROVIDER) \
            .filter(Signal.strategy_id == sig.strategy_id) \
            .filter(Position.is_active == True) \
            .order_by(Position.create_date.desc()) \
            .first()

        message = {'left-ticker-upper': strategy.coin.left_ticker.upper(),
                   'left-ticker-lower': strategy.coin.left_ticker.lower(),
                   'left-ticker-capitalize': strategy.coin.left_ticker.capitalize(),

                   'right-ticker-upper': strategy.coin.right_ticker.upper(),
                   'right-ticker-lower': strategy.coin.right_ticker.lower(),
                   'right-ticker-capitalize': strategy.coin.right_ticker.capitalize(),

                   'side-upper': " - ",
                   'side-lower': " - ",
                   'side-capitalize': " - ",
                   'base-price': " - ",
                   'open-date': " - ",
                   'position-level': " - ",
                   'profit': " - ",
                   'profit-computed': " - ",
                   'profit-percent': " - ",
                   'amount': " - ",
                   'amount-abs': " - ",
                   'trailing-stop-price': " - ",
                   'price-liq': " - ",
                   'provider-id': " - ",
                   'type-upper': " - ",
                   'type-lower': " - ",
                   'type-capitalize': " - ",

                   'o-side-upper': " - ",
                   'o-side-lower': " - ",
                   'o-side-capitalize': " - ",
                   'o-base-price': " - ",
                   'o-open-date': " - ",
                   'o-position-level': " - ",
                   'o-profit': " - ",
                   'o-profit-computed': " - ",
                   'o-profit-percent': " - ",
                   'o-amount': " - ",
                   'o-amount-abs': " - ",
                   'o-trailing-stop-price': " - ",
                   'o-price-liq': " - ",
                   'o-provider-id': " - ",
                   'o-type-upper': " - ",
                   'o-type-lower': " - ",
                   'o-type-capitalize': " - ",

                   'strategy-name': sig.strategy.name,
                   'strategy-pyramiding': f"{sig.strategy.pyramiding}",
                   'indicator-value': ", ".join([f"{iv:.5f}" for iv in sig.indicator_value]),
                   'account-name': account.name,
                   'last-price': ""}

        ticker = self.session.query(Ticker) \
            .filter(Ticker.provider == self.PROVIDER) \
            .filter(Ticker.symbol == sig.strategy.coin.ticker) \
            .first()
        if ticker:
            message['last-price'] = ticker.last_price

        if current_open_position:
            side = current_open_position.side.value if current_open_position.side else " - "
            message['side-upper'] = side.upper()
            message['side-lower'] = side.lower()
            message['side-capitalize'] = side.capitalize()
            message['base-price'] = current_open_position.base_price
            message['open-date'] = current_open_position.create_date.strftime('%d.%m.%Y %H:%M UTC')
            message['position-level'] = current_open_position.level
            message['profit'] = current_open_position.profit
            message['profit-computed'] = current_open_position.profit_computed
            message['profit-percent'] = current_open_position.profit_perc
            message['amount'] = current_open_position.amount
            message['amount-abs'] = abs(current_open_position.amount)
            message['trailing-stop-price'] = current_open_position.trailing_stop_price
            message['price-liq'] = current_open_position.price_liq
            message['provider-id'] = current_open_position.provider_id
            message['type-upper'] = current_open_position.type.value.upper()
            message['type-lower'] = current_open_position.type.value.lower()
            message['type-capitalize'] = current_open_position.type.value.capitalize()

        if old_open_position:
            side = old_open_position.side.value if old_open_position.side else " - "
            message['o-side-upper'] = side.upper()
            message['o-side-lower'] = side.lower()
            message['o-side-capitalize'] = side.capitalize()
            message['o-base-price'] = old_open_position.base_price
            message['o-open-date'] = old_open_position.create_date.strftime('%d.%m.%Y %H:%M UTC')
            message['o-position-level'] = old_open_position.level
            message['o-profit'] = old_open_position.profit
            message['o-profit-computed'] = old_open_position.profit_computed
            message['o-profit-percent'] = old_open_position.profit_perc
            message['o-amount'] = old_open_position.amount
            message['o-amount-abs'] = abs(old_open_position.amount)
            message['o-trailing-stop-price'] = old_open_position.trailing_stop_price
            message['o-price-liq'] = old_open_position.price_liq
            message['o-provider-id'] = old_open_position.provider_id
            message['o-type-upper'] = old_open_position.type.value.upper()
            message['o-type-lower'] = old_open_position.type.value.lower()
            message['o-type-capitalize'] = old_open_position.type.value.capitalize()

        for chat in account.posting_chats:
            if old_open_position is None and chat.open_position_message and sig.action is not SignalAction.hold:
                message_text = chat.open_position_message
            elif sig.action is SignalAction.buy:
                message_text = chat.buy_message
            elif sig.action is SignalAction.sell:
                message_text = chat.sell_message
            else:
                if not chat.allow_hold_signals:
                    continue
                message_text = chat.hold_message

            message_text = message_text.replace(r"\_", "_").replace(r"\*", "*").replace(r"\[", "[").replace(r"\]", "]")
            for message_key in [m.group()[1:-1] for m in re.finditer(r'{[\w\-:+]+}', message_text)]:
                value = self._get_message_value(message, message_key)
                if value:
                    message_text = message_text.replace(f"{{{message_key}}}", value)

            try:
                chat_id = chat.chat_id if chat.chat_id.startswith("-100") else "-100" + chat.chat_id
                self.bot.send_message(chat_id=chat_id, text=message_text, parse_mode="MARKDOWN")

                if chat.send_positions_ratio:
                    # start job to send positions ratio
                    if chat.id in self.__timers:
                        self.__timers[chat.id].cancel()
                        del self.__timers[chat.id]

                    timer = Timer(50, self.send_positions_ratio, [chat])
                    self.__timers.update({chat.id: timer})
                    timer.start()
            except TelegramError as e:
                self.logger.error(f"Can't send message to channel because TelegramError occurred.\n {str(e)}")

    def _get_message_value(self, message, key):
        size = 8
        plus = False
        if ':' in key:
            try:
                str_size = key[key.index(':') + 1:]
                if len(str_size) >= 1:
                    if str_size.startswith("+"):
                        plus = True
                        size = int(str_size[1:])
                    else:
                        size = int(str_size)
            except ValueError:
                pass
            finally:
                key = key[:key.index(':')]

        raw_value = message.get(key, "|Wrong key|")

        if isinstance(raw_value, float):
            value = "{}".format(round(raw_value, size))
            if plus and raw_value > 0:
                value = "+" + value
        elif raw_value is None:
            value = " - "
        else:
            value = str(raw_value)
        return value

    def fake_buy(self, account, pair, amount, sig=None):
        amount = 1 if not amount else amount
        ticker = self.session.query(Ticker) \
            .filter(Ticker.provider == self.PROVIDER) \
            .filter(Ticker.symbol == pair) \
            .first()

        last_position = self.session.query(Position) \
            .join(Position.account) \
            .filter(Account.provider == self.PROVIDER) \
            .filter(Position.symbol == pair) \
            .filter(Position.account_id == account.id) \
            .filter(Position.type == PositionType.fake) \
            .filter(Position.is_active == True) \
            .order_by(Position.create_date.desc()) \
            .first()

        if last_position:
            if last_position.side is Side.SELL:
                last_position.status = PositionStatus.closed
                new_position = Position(account_id=account.id, symbol=pair, type=PositionType.fake, amount=amount, full_amount=amount, base_price=ticker.bid, signal=sig, status=PositionStatus.opened)
                self.session.add(new_position)
            else:
                if last_position.level >= sig.strategy.pyramiding:
                    self.logger.warning(f"Can't create order and position for {account.name} {pair}. Maximum position level reached.")
                    return
                last_position.level += 1
                self.session.add(last_position)
        else:
            new_position = Position(account_id=account.id, symbol=pair, type=PositionType.fake, amount=amount, full_amount=amount, base_price=ticker.bid, signal=sig, status=PositionStatus.opened)
            self.session.add(new_position)

        self.session.commit()
        self.logger.debug(f"Created fake BUY {pair} {amount} position for {account.name}")

    def fake_sell(self, account, pair, amount, sig=None):
        amount = -1 if not amount else amount
        ticker = self.session.query(Ticker) \
            .filter(Ticker.provider == self.PROVIDER) \
            .filter(Ticker.symbol == pair) \
            .first()

        last_position = self.session.query(Position) \
            .join(Position.account) \
            .filter(Account.provider == self.PROVIDER) \
            .filter(Position.symbol == pair) \
            .filter(Position.account_id == account.id) \
            .filter(Position.type == PositionType.fake) \
            .filter(Position.is_active == True) \
            .order_by(Position.create_date.desc()) \
            .first()

        if last_position:
            if last_position.side is Side.BUY:
                last_position.status = PositionStatus.closed
                new_position = Position(account_id=account.id, symbol=pair, type=PositionType.fake, amount=amount, full_amount=amount, base_price=ticker.ask, signal=sig, status=PositionStatus.opened)
                self.session.add(new_position)
            else:
                if last_position.level >= sig.strategy.pyramiding:
                    self.logger.warning(f"Can't create order and position for {account.name} {pair}. Maximum position level reached.")
                    return
                last_position.level += 1
                self.session.add(last_position)
        else:
            new_position = Position(account_id=account.id, symbol=pair, type=PositionType.fake, amount=amount, full_amount=amount, base_price=ticker.ask, signal=sig, status=PositionStatus.opened)
            self.session.add(new_position)

        self.session.commit()
        self.logger.debug(f"Created fake SELL {pair} {amount} position for {account.name}")

    def buy(self, account, pair, amount, sig=None):
        ticker = self.session.query(Ticker) \
            .filter(Ticker.provider == self.PROVIDER) \
            .filter(Ticker.symbol == pair) \
            .first()

        last_position = self.session.query(Position) \
            .join(Position.account) \
            .filter(Account.provider == self.PROVIDER) \
            .filter(Position.symbol == pair) \
            .filter(Position.account_id == account.id) \
            .filter(Position.type == PositionType.real) \
            .filter(Position.is_active == True) \
            .order_by(Position.create_date.desc()) \
            .first()

        if last_position:
            if last_position.side is Side.SELL or last_position.side is None:
                buy_amount = amount + abs(last_position.amount)
                new_position = Position(account_id=account.id, symbol=pair, type=PositionType.real, amount=0, full_amount=buy_amount, base_price=ticker.bid, signal=sig, status=PositionStatus.pending)
            else:
                if last_position.level >= sig.strategy.pyramiding:
                    self.logger.warning(f"Can't create order and position for {account.name} {pair}. Maximum position level reached.")
                    return
                new_position = None
                last_position.level += 1
                if last_position.status is PositionStatus.opened:
                    last_position.status = PositionStatus.partially_opened
                self.session.add(last_position)
                buy_amount = amount

        else:
            buy_amount = amount
            new_position = Position(account_id=account.id, symbol=pair, type=PositionType.real, amount=0, full_amount=buy_amount, base_price=ticker.bid, signal=sig, status=PositionStatus.pending)

        try:
            if new_position and last_position and last_position.order and last_position.order.is_active and last_position.order.provider_id:
                self.close_order(account, last_position.order.provider_id)
                self.logger.debug(f"Canceled order with id:{last_position.order.provider_id}")

            cid = self.create_buy_order(account, pair, buy_amount, ticker.bid - ticker.bid / 2)
            order = Order(provider_id=0,
                          type="LIMIT",
                          cid=cid,
                          account_id=account.id,
                          symbol=pair,
                          amount_orig=buy_amount,
                          price=ticker.bid - ticker.bid / 2,
                          create_date=datetime.datetime.utcnow())

            self.session.add(order)
            self.logger.debug(f"Created new order with cid:{cid}")
            if new_position is not None:
                new_position.order = order
                self.session.add(new_position)
        except (BitfinexException, AttributeError) as e:
            self.logger.error(f"And error occurred while creating order, order and position not created:\n{str(e)}")

        self.session.commit()
        self.logger.debug(f"Buying for strategy {sig.strategy.name} with amount {buy_amount} complete")

    def sell(self, account, pair, amount, sig=None):
        ticker = self.session.query(Ticker) \
            .filter(Ticker.provider == self.PROVIDER) \
            .filter(Ticker.symbol == pair) \
            .first()

        last_position = self.session.query(Position) \
            .join(Position.account) \
            .filter(Account.provider == self.PROVIDER) \
            .filter(Position.symbol == pair) \
            .filter(Position.account_id == account.id) \
            .filter(Position.type == PositionType.real) \
            .filter(Position.is_active == True) \
            .order_by(Position.create_date.desc()) \
            .first()

        if last_position:
            if last_position.side is Side.BUY or last_position.side is None:
                sell_amount = amount + abs(last_position.amount)
                new_position = Position(account_id=account.id, symbol=pair, type=PositionType.real, amount=0, full_amount=-sell_amount, base_price=ticker.ask, signal=sig, status=PositionStatus.pending)
            else:
                if last_position.level >= sig.strategy.pyramiding:
                    self.logger.warning(f"Can't create order and position for {account.name} {pair}. Maximum position level reached.")
                    return
                new_position = None
                last_position.level += 1
                if last_position.status is PositionStatus.opened:
                    last_position.status = PositionStatus.partially_opened
                self.session.add(last_position)
                sell_amount = amount

        else:
            sell_amount = amount
            new_position = Position(account_id=account.id, symbol=pair, type=PositionType.real, amount=0, full_amount=-sell_amount, base_price=ticker.ask, signal=sig, status=PositionStatus.pending)

        try:
            if new_position and last_position and last_position.order and last_position.order.is_active and last_position.order.provider_id:
                self.close_order(account, last_position.order.provider_id)
                self.logger.debug(f"Canceled order with id:{last_position.order.provider_id}")

            cid = self.create_sell_order(account, pair, sell_amount, ticker.ask + ticker.ask / 2)
            order = Order(provider_id=0,
                          type="LIMIT",
                          cid=cid,
                          account_id=account.id,
                          symbol=pair,
                          amount_orig=-sell_amount,
                          price=ticker.ask + ticker.ask / 2,
                          create_date=datetime.datetime.utcnow())
            self.session.add(order)
            self.logger.debug(f"Created new order with cid:{cid}")
            if new_position is not None:
                new_position.order = order
                self.session.add(new_position)
        except (BitfinexException, AttributeError) as e:
            self.logger.error(f"And error occurred while creating order, order and position not created:\n{str(e)}")

        self.session.commit()
        self.logger.debug(f"Selling for strategy {sig.strategy.name} with amount {-sell_amount} complete")

    def start(self):
        with self.__lock:
            self.subscribe_to_orderbook()
            # run subscribe to accounts thread
            if not self.subscribe_to_accounts_running:
                self.subscribe_to_accounts_running = True
                self._init_thread(self.subscribe_to_accounts, f"subscribe_to_accounts_{self.PROVIDER.value}")

            # run positions updater thread
            if not self.position_updater_running:
                self.position_updater_running = True
                self._init_thread(self.position_updater, f"position_updater_{self.PROVIDER.value}")

            logger.info(f"{self.PROVIDER.value if self.PROVIDER else 'None'} account controller started.")

    def stop(self):
        with self.__lock:
            # close all websockets
            for ws in list(self.auth_websockets.values()) + [self.orderbook_ws]:
                if ws:
                    ws.close()

            # wait until threads finish their work
            if self.subscribe_to_accounts_running or self.position_updater_running:
                self._stop.set()
                while self.subscribe_to_accounts_running or self.position_updater_running:
                    sleep(0.1)
                self._stop.clear()

            for thr in self.__threads:
                thr.join()
            self.__threads = []

    def _init_thread(self, target, name, *args, **kwargs):
        thr = Thread(target=thread_wrapper, name=name, args=(target,) + args, kwargs=kwargs)
        thr.start()
        self.__threads.append(thr)
