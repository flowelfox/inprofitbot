import logging

# from .account_controller import AccountController
from backend.controllers.binance.account_controller import AccountController
from backend.controllers.signal_controller import BaseSignalController, BaseSignalFormer
from models import Provider
from .candle_controller import CandleFormer

logger = logging.getLogger(__name__)


class SignalFormer(BaseSignalFormer):
    candle_former_class = CandleFormer
    PROVIDER = Provider.BINANCE
    logger = logging.getLogger(__name__)


class SignalController(BaseSignalController):
    account_controller_class = AccountController
    signal_former_class = SignalFormer
    candle_former_class = CandleFormer
    MINIMUM_ORDER_SIZE = 10
    PROVIDER = Provider.BINANCE
    logger = logging.getLogger(__name__)
