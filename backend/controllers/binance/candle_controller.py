import datetime
import logging

import pytz
from binance.client import Client
from binance.websockets import BinanceSocketManager

from backend.controllers.candle_controller import BaseCandleController, BaseCandleFormer
from backend.helpers import datetime_to_minutes
from models import BinanceCandle, sessionmaker, Ticker, Provider
from settings import BINANCE_AVAILABLE_PAIRS, BINANCE_TICKER_PAIRS, SLEEP_AFTER_BINANCE_REQUEST

logger = logging.getLogger(__name__)

K = 1000


class CandleFormer(BaseCandleFormer):
    candle_class = BinanceCandle
    SLEEP_AFTER_REQUEST = SLEEP_AFTER_BINANCE_REQUEST
    PROVIDER = Provider.BINANCE
    logger = logging.getLogger(__name__)

    request_start_time = 0
    request_end_time = 0

    def combine_candle(self, candle_group):
        candle_date_minutes = datetime_to_minutes(candle_group[0].open_date_dt)
        open_date = datetime.datetime.utcfromtimestamp((candle_group[0].open_date - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % self.timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)
        close_date = datetime.datetime.utcfromtimestamp((candle_group[0].close_date - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % self.timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)

        new_candle = BinanceCandle(
            open=candle_group[-1].open,
            close=candle_group[0].close,
            high=max([candle.high for candle in candle_group]),
            low=min([candle.low for candle in candle_group]),
            volume=sum([candle.volume for candle in candle_group]),
            quote_asset_volume=sum([candle.quote_asset_volume for candle in candle_group]),
            ticker=self.ticker,
            open_date=int(open_date.timestamp() * K),
            close_date=int(close_date.timestamp() * K),
            trades=sum([candle.trades for candle in candle_group]),
        )
        return new_candle

    def request_raw_candles(self, newest_open_date):
        client = Client()
        return client.get_klines(symbol=self.ticker,
                                 interval=Client.KLINE_INTERVAL_1MINUTE,
                                 limit=1000,
                                 startTime=int(pytz.timezone('Europe/Kiev').fromutc(datetime.datetime.utcfromtimestamp(newest_open_date // K)).timestamp() * K))


class CandleController(BaseCandleController):
    candle_class = BinanceCandle
    candle_former_class = CandleFormer
    ticker_pairs = BINANCE_TICKER_PAIRS
    candles_pairs = BINANCE_AVAILABLE_PAIRS
    PROVIDER = Provider.BINANCE
    logger = logging.getLogger(__name__)

    def create_websocket_client(self):
        return BinanceSocketManager(None)

    def subscribe_candle(self, ws, ticker):
        cl = CandleListener(f'{ticker}')
        ws.start_kline_socket(ticker, cl.new_candle_info, Client.KLINE_INTERVAL_1MINUTE)

    def subscribe_ticker(self, ws, ticker):
        tl = TickerListener(ticker)
        ws.start_symbol_ticker_socket(ticker, tl.new_ticker_info)


class CandleListener:
    def __init__(self, ticker):
        self.ticker = ticker
        self.session = sessionmaker()
        self.logger = logging.getLogger(self.__class__.__name__)

    def new_candle_info(self, message):
        if self.ticker and message:
            candle = BinanceCandle.from_dict(message['k'], ticker=self.ticker)
            candle.add_or_update(self.session)
            self.session.commit()


class TickerListener:
    def __init__(self, symbol):
        self.symbol = symbol
        self.logger = logging.getLogger(self.__class__.__name__)
        self.session = sessionmaker()

    def new_ticker_info(self, message):
        ticker = self.session.query(Ticker) \
            .filter(Ticker.symbol == self.symbol) \
            .filter(Ticker.provider == Provider.BINANCE) \
            .first()
        if ticker is None:
            ticker = Ticker()

        ticker.provider = Provider.BINANCE
        ticker.symbol = self.symbol
        ticker.ask = message['a']
        ticker.bid = message['b']
        ticker.last_price = message['c']
        ticker.update_date = datetime.datetime.utcnow()
        self.session.add(ticker)
        self.session.commit()
