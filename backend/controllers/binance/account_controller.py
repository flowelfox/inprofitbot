import datetime
import logging
from _decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_DOWN
from time import sleep

from binance.client import Client
from binance.websockets import BinanceSocketManager

from backend.controllers.account_controller import BaseAccountController
from models import Position, PositionType, Account, sessionmaker, Wallet, WalletChange, Order, PositionStatus, Provider, OrderOwner
from settings import ALLOW_TRADE, BINANCE_AVAILABLE_PAIRS

logger = logging.getLogger(__name__)


class AccountController(BaseAccountController):
    AVAILABLE_PAIRS = BINANCE_AVAILABLE_PAIRS
    PROVIDER = Provider.BINANCE
    logger = logging.getLogger(__name__)

    def __init__(self, session):
        super(AccountController, self).__init__(session)
        self.logger.info("Getting symbols info")
        self.symbols_info = {}
        client = Client()
        for ticker in self.AVAILABLE_PAIRS:
            self.logger.debug(f"Requesting symbol info for {ticker}")
            self.symbols_info.update({ticker: client.get_symbol_info(ticker)})
            sleep(0.06)
        self.logger.info("Getting symbols info finished")

    def update_wallet_info(self, type, account):
        account_client = Client(api_key=account.api_key, api_secret=account.secret)
        if type == 'margin':
            wallets = account_client.get_margin_account()['userAssets']
        elif type == 'exchange':
            wallets = account_client.get_account()['balances']
        else:
            return []

        for wallet in wallets:
            if float(wallet['free']) + float(wallet['locked']) == 0:
                continue

            wallet_obj = self.accounts_session.query(Wallet) \
                .filter(Wallet.type == type) \
                .filter(Wallet.account == account) \
                .filter(Wallet.currency == wallet['asset']) \
                .first()

            if wallet_obj is None:
                wallet_obj = Wallet(account=account)
                wallet_obj.type = type
                wallet_obj.currency = wallet['asset']
                self.accounts_session.add(wallet_obj)

            wallet_change = WalletChange()
            wallet_change.balance = float(wallet['free']) + float(wallet['locked'])
            wallet_change.unsettled_interest = wallet.get('interest', None)
            wallet_change.balance_available = wallet['free']
            wallet_change.wallet = wallet_obj

            self.accounts_session.add(wallet_change)

        self.accounts_session.commit()

    def update_orders(self, account):
        # get newest orders info
        client = Client(api_key=account.api_key, api_secret=account.secret)
        active_orders = []

        for symbol in self.AVAILABLE_PAIRS:
            orders = client.get_all_margin_orders(symbol=symbol)

            for order in orders:
                order_obj = self.session.query(Order) \
                    .filter(Order.cid == order['clientOrderId']) \
                    .filter(Order.account_id == account.id) \
                    .first()
                if not order_obj:
                    order_obj = Order(cid=order['clientOrderId'], account_id=account.id, owner=OrderOwner.binance)
                order_obj.provider_id = order['orderId']
                order_obj.create_date = datetime.datetime.utcfromtimestamp(order['time'] / 1e3)
                order_obj.symbol = order['symbol']
                order_obj.amount = float(order['executedQty']) if order['side'] == "BUY" else -float(order['executedQty'])
                order_obj.amount_orig = float(order['origQty']) if order['side'] == "BUY" else -float(order['origQty'])
                order_obj.type = order['type']
                order_obj.status = order['status']
                order_obj.price = order['price']
                order_obj.price_avg = None
                if order_obj.status.startswith("REJECTED") or order_obj.status.startswith("EXPIRED") or order_obj.status.startswith("CANCELED") or order_obj.status.startswith("FILLED") or order_obj.status.startswith("PENDING_CANCEL"):
                    order_obj.close_date = datetime.datetime.utcfromtimestamp(order['updateTime'] / 1e3)
                if order_obj.status == 'NEW' or order_obj.status == 'PARTIALLY_FILLED':
                    active_orders.append(order_obj)

                self.session.add(order_obj)
            sleep(0.1)
        self.session.commit()
        return active_orders

    def create_account_ws(self, account):
        pass
        # get wallets info before connecting to websocket stream
        # self.update_wallet_info('margin', account)
        # self.update_wallet_info('exchange', account)
        #
        # # connect to accounts websocket
        account_client = Client(api_key=account.api_key, api_secret=account.secret)
        account_ws = BinanceSocketManager(account_client)
        #
        # # add margin account websocket
        # al_margin = AccountListener(account.id, self.orders, 'margin')
        # account_ws.start_margin_socket(al_margin.new_account_info)
        #
        # # add exchange account websocket
        # al_exchange = AccountListener(account.id, self.orders, 'exchange')
        # account_ws.start_user_socket(al_exchange.new_account_info)
        #
        # account_ws.start()
        self.auth_websockets[account.id] = account_ws

        return account_ws

    def create_orderbook_ws(self):
        pass
        # self.orderbook_ws = BinanceSocketManager(None)
        # for ticker in BINANCE_AVAILABLE_PAIRS:
        #     bl = BookListener(ticker, self.auth_websockets, self.orders)
        #     self.orderbook_ws.start_depth_socket(ticker, bl.new_book_info)
        # self.orderbook_ws.start()

    def close_order(self, account, provider_id):
        pass
        account_ws = self.auth_websockets[account.id]
        order = self.session.query(Order).filter(Order.provider_id == provider_id).first()
        account_ws._client.cancel_margin_order(symbol=order.ticker, origClientOrderId=order.cid)

    def create_sell_order(self, account, ticker, amount, price):
        account_ws = self.auth_websockets[account.id]

        symbol_info = self.symbols_info[ticker]
        price_tick = symbol_info['filters'][0]['tickSize']
        amount_tick = symbol_info['filters'][2]['stepSize']
        price = Decimal(price).quantize(Decimal(price_tick[:price_tick.index('1') + 1]), rounding=ROUND_HALF_UP)
        amount = Decimal(amount).quantize(Decimal(amount_tick[:amount_tick.index('1') + 1]), rounding=ROUND_HALF_DOWN)

        order = account_ws._client.create_margin_order(symbol=ticker,
                                                       side=Client.SIDE_SELL,
                                                       type=Client.ORDER_TYPE_LIMIT,
                                                       timeInForce="GTC",
                                                       quantity=str(amount),
                                                       price=str(price))
        cid = order["clientOrderId"]
        return cid

    def create_buy_order(self, account, ticker, amount, price):
        account_ws = self.auth_websockets[account.id]

        symbol_info = self.symbols_info[ticker]
        price_tick = symbol_info['filters'][0]['tickSize']
        amount_tick = symbol_info['filters'][2]['stepSize']
        price = Decimal(price).quantize(Decimal(price_tick[:price_tick.index('1') + 1]), rounding=ROUND_HALF_DOWN)
        amount = Decimal(amount).quantize(Decimal(amount_tick[:amount_tick.index('1') + 1]), rounding=ROUND_HALF_DOWN)

        order = account_ws._client.create_margin_order(symbol=ticker,
                                                       side=Client.SIDE_BUY,
                                                       type=Client.ORDER_TYPE_LIMIT,
                                                       timeInForce="GTC",
                                                       quantity=str(amount),
                                                       price=str(price))
        cid = order["clientOrderId"]
        return cid


class AccountListener:
    def __init__(self, account_id, orders, type):
        self.session = sessionmaker()
        self.account_id = account_id
        self.account = self.session.query(Account) \
            .filter(Account.provider == Provider.BINANCE) \
            .filter(Account.id == self.account_id) \
            .first()
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}")
        self.orders = orders
        self.type = type

    def wallet_update(self, message):
        self.logger.debug(f"Binance {self.type} wallet websocket triggered for {self.account.name}")
        wallets = message['B']

        for wallet in wallets:
            if float(wallet['f']) + float(wallet['l']) == 0:
                continue

            wallet_obj = self.session.query(Wallet) \
                .filter(Wallet.type == self.type) \
                .filter(Wallet.account == self.account) \
                .filter(Wallet.currency == wallet['a']).first()

            if wallet_obj is None:
                wallet_obj = Wallet(account=self.account)
                wallet_obj.type = self.type
                wallet_obj.currency = wallet['a']
                self.session.add(wallet_obj)

            wallet_change = WalletChange()
            wallet_change.balance = float(wallet['f']) + float(wallet['l'])
            wallet_change.unsettled_interest = 0
            wallet_change.balance_available = wallet['f']
            wallet_change.wallet = wallet_obj

            self.session.add(wallet_change)

        self.session.commit()

    def order_update(self, message):
        self.logger.debug(f"Binance {self.type} order websocket triggered for {self.account.name}")
        order = message

        retries = 0
        while retries < 3:
            order_obj = self.session.query(Order).filter(Order.cid == order['c']).filter(Order.close_date == None).first()
            if order_obj:
                order_obj.account_id = self.account_id
                order_obj.provider_id = order['i']
                order_obj.create_date = datetime.datetime.utcfromtimestamp(order['O'] / 1e3)
                order_obj.symbol = order['s']
                order_obj.amount = order['z']
                order_obj.amount_orig = order['q']
                order_obj.type = order['o']
                order_obj.status = order['X']
                order_obj.price = order['p']
                order_obj.price_avg = order['p']

                self.session.add(order_obj)
                self.session.commit()
                self.logger.debug(f"Order with cid {order_obj.cid} updated")

                if order['X'].startswith("REJECTED") or order['X'].startswith("EXPIRED") or order['X'].startswith("CANCELED") or order['X'].startswith("FILLED") or order['X'].startswith("PENDING_CANCEL"):
                    # remove order from memory
                    order_obj.close_date = datetime.datetime.utcnow()
                    for ord in self.orders.get(order_obj.symbol, []):
                        if ord.cid == order_obj.cid:
                            self.orders[order_obj.symbol].remove(ord)
                            break
                else:
                    # add oder to memory
                    self.session.expunge(order_obj)
                    self.orders[order_obj.symbol].append(order_obj)
                break
            else:
                retries += 1
                sleep(0.5)

    def position_update(self, message):
        if message[1] == 'ps':
            positions = message[2]
        else:
            positions = [message[2]]

        for position in positions:
            position_query = self.session.query(Position) \
                .filter(Position.symbol == position[0]) \
                .filter(Position.account_id == self.account_id) \
                .filter(Position.type == PositionType.real) \
                .order_by(Position.create_date.desc())

            # filter by side
            if position[2] > 0:
                side_position_query = position_query.filter(Position.full_amount > 0)
            elif position[2] < 0:
                side_position_query = position_query.filter(Position.full_amount < 0)
            else:
                side_position_query = position_query

            position_obj = side_position_query.first()
            # ignore side if position not found
            if position_obj is None:
                position_obj = position_query.first()

            # create position if it does not exists
            if position_obj is None:
                order_obj = self.session.query(Order) \
                    .filter(Order.symbol == position[0]) \
                    .filter(Order.account_id == self.account_id) \
                    .order_by(Order.create_date.desc()) \
                    .first()
                if order_obj is None:
                    continue
                self.session.add(order_obj)
                position_obj = Position(account_id=self.account_id, type=PositionType.real, level=1, order=order_obj)

            # close previous position
            if position_obj.status == PositionStatus.pending and position_obj.amount == 0:
                last_positions = self.session.query(Position) \
                    .filter(Position.account_id == self.account_id) \
                    .filter(Position.symbol == position[0]) \
                    .filter(Position.type == PositionType.real) \
                    .filter(Position.is_active == True) \
                    .order_by(Position.create_date.desc()) \
                    .limit(2) \
                    .all()
                if last_positions and len(last_positions) == 2:
                    previous_position = last_positions[-1]
                    previous_position.status = PositionStatus.closed
                    self.session.add(previous_position)

            position_obj.symbol = position[0]
            position_obj.amount = position[2]
            position_obj.base_price = position[3]
            position_obj.profit = position[6]
            position_obj.profit_perc = position[7]
            position_obj.price_liq = position[8]

            # get position id
            if position_obj.provider_id is None:
                client_v2 = ClientV2(self.account.api_key, self.account.secret)
                active_positions = client_v2.active_positions()
                for bitfinex_position in active_positions:
                    if bitfinex_position[0] == position[0]:
                        position_obj.provider_id = bitfinex_position[11]
                        break

            # determinate position status
            if position_obj.status is PositionStatus.stopped:
                pass
            elif position[1] == "CLOSED" or position_obj.amount == 0:
                position_obj.status = PositionStatus.closed
                self.logger.debug(f"Position {position_obj.id} closed")
            elif position_obj.order and not position_obj.order.is_active:
                position_obj.status = PositionStatus.opened
            elif position_obj.order and position_obj.order.is_active:
                position_obj.status = PositionStatus.partially_opened
            else:
                position_obj.status = PositionStatus.closed
                self.logger.debug(f"Position {position_obj.id} closed")

            self.session.add(position_obj)
            self.session.commit()

    def notification_update(self, message):
        notification = message[2]
        if notification[6] == "ERROR" or notification[6] == "FAILURE":
            if notification[1] == 'ou-req' and notification[7] == "order: invalid":  # removing invalid order
                orders = []
                [orders.extend(o) for o in self.orders.values()]
                for order in orders:
                    if order.provider_id == notification[4][0]:
                        self.orders[order.symbol].remove(order)
                        break
            else:
                self.logger.error(notification[7])

    def new_account_info(self, message):
        if self.account is None:
            return

        # if isinstance(message, dict) and message['e'] == 'outboundAccountInfo' and (self.type == 'margin' or self.type == 'exchange'):
        #     self.wallet_update(message)
        #
        # if isinstance(message, dict) and message['e'] == 'executionReport' and self.type == 'margin':
        #     self.order_update(message)
        #
        # if isinstance(message, list) and len(message) > 2 and (message[1] == 'ps' or message[1] == 'pn' or message[1] == 'pu' or message[1] == 'pc'):
        #     self.position_update(message)
        #
        # if isinstance(message, list) and len(message) > 2 and message[1] == 'n':
        #     self.notification_update(message)


class BookListener:
    def __init__(self, symbol, auth_websockets, orders):
        self.orders = orders
        self.auth_websockets = auth_websockets
        self.symbol = symbol
        self.sell_book = dict()
        self.buy_book = dict()
        self.now = int(datetime.datetime.utcnow().timestamp() * 1e3)
        self.session = sessionmaker()
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}")

    def new_book_info(self, message):
        """
        0 - order_id
        1 - price
        2 - amount
        """
        if isinstance(message, dict):
            pass
        else:
            if isinstance(message[1], list) and isinstance(message[1][0], list):
                for order in message[1][:25]:
                    self.buy_book.update({order[0]: order[1:]})

                for order in message[1][25:]:
                    self.sell_book.update({order[0]: order[1:]})

            elif isinstance(message[1], list):
                order = message[1]
                if order[1] == 0:
                    try:
                        if order[2] > 0:
                            del self.buy_book[order[0]]
                        if order[2] < 0:
                            del self.sell_book[order[0]]
                    except KeyError:
                        pass
                else:
                    if order[2] > 0:
                        self.buy_book.update({order[0]: order[1:]})
                    if order[2] < 0:
                        self.sell_book.update({order[0]: order[1:]})

                new_now = int(datetime.datetime.utcnow().timestamp() * 1e3)
                if new_now - self.now > 1000:
                    self.update_orders()
                    self.now = new_now
            else:
                pass

    def update_orders(self):
        target_position = 0
        orders = self.orders.get(self.symbol, [])
        if not orders:
            return

        sorted_buy_book = sorted(self.buy_book.values(), key=lambda x: x[0], reverse=True)
        sorted_sell_book = sorted(self.sell_book.values(), key=lambda x: x[0])
        if len(sorted_sell_book) > 20 and len(sorted_buy_book) > 20:
            for order in orders:
                top_price_buy = sorted_buy_book[target_position][0]
                top_price_sell = sorted_sell_book[target_position][0]
                if order.amount > 0:
                    price = top_price_buy - (1 / 10 ** len(str(top_price_buy).split('.')[-1]))
                    if order.price == top_price_buy:  # ignore when price is lowering
                        continue
                else:
                    price = top_price_sell + (1 / 10 ** len(str(top_price_sell).split('.')[-1]))
                    if order.price == top_price_sell:  # ignore when price is rising
                        continue

                if price != order.price and order.account_id in self.auth_websockets and ALLOW_TRADE:
                    ws = self.auth_websockets[order.account_id]
                    if ws.factories['auth'].protocol_instance is not None:  # is websocket already authenticated
                        ws.update_order(id=order.provider_id, price=f"{price:.8f}")
                        self.logger.debug(f"Changed order {order.id} from {order.price} to {price}.")
                        order.price = price
