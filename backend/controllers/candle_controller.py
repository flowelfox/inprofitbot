import datetime
import logging
import time
from abc import ABC

from binance.exceptions import BinanceAPIException
from bitfinex.rest.restv2 import BitfinexException
from math import ceil
from threading import Event

import pytz
import requests

from backend.helpers import datetime_to_minutes, minute_to_timedelta
from models import DBSession, sessionmaker

K = 1000


class BaseCandleController(ABC):
    candle_class = None
    candle_former_class = None
    ticker_pairs = []
    candles_pairs = []
    PROVIDER = None
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.running = False
        self.is_idle = False
        self.public_websocket_candles = self.create_websocket_client()
        self.public_websocket_candles.channels = 0
        self.public_websocket_tickers = self.create_websocket_client()
        self.public_websocket_tickers.channels = 0
        self.session = sessionmaker()

    def create_websocket_client(self):
        raise NotImplementedError

    def subscribe_candle(self, ws, ticker):
        raise NotImplementedError

    def subscribe_ticker(self, ws, ticker):
        raise NotImplementedError

    def subscribe_to_candles(self):
        ws = self.public_websocket_candles
        for ticker in self.candles_pairs:
            last_candle = DBSession.query(self.candle_class).filter(self.candle_class.ticker == ticker).order_by(self.candle_class.open_date.desc()).first()
            if last_candle:
                cf = self.candle_former_class(None, None, ticker, session=self.session)
                cf.request_candles(last_candle.open_date_dt, session=self.session)
            self.subscribe_candle(ws, ticker)
            ws.channels += 1
        self.logger.info(f"Subscribed to candles with {ws.channels} channels.")

    def subscribe_to_tickers(self):
        tickers = set(self.ticker_pairs)
        ws = self.public_websocket_tickers

        for ticker in tickers:
            self.subscribe_ticker(ws, ticker)
            ws.channels += 1
        self.logger.info(f"Subscribed to tickers with {ws.channels} channels.")

    def stop(self):
        self.public_websocket_candles.close()
        self.public_websocket_tickers.close()

    def start(self):
        self.subscribe_to_candles()
        self.subscribe_to_tickers()
        self.public_websocket_candles.start()
        self.public_websocket_tickers.start()

        self.logger.info(f"{self.PROVIDER.value if self.PROVIDER else 'None'} candle controller started.")


class BaseCandleFormer(ABC):
    candle_class = None
    SLEEP_AFTER_REQUEST = 0
    PROVIDER = None
    logger = logging.getLogger(__name__)

    def __init__(self, timeframe, period, ticker, end_time=None, session=None):
        self.timeframe = timeframe
        self.period = period
        self.ticker = ticker
        self._end_time = None
        if end_time is not None:
            self.end_time = end_time.replace(tzinfo=pytz.utc)
        self.candles = []
        self.running = False
        self.is_idle = False
        self.session = session
        self.request_start_time = time.time()
        self.request_end_time = time.time()

    def combine_candle(self, candle_group):
        raise NotImplementedError

    def request_raw_candles(self, newest_open_date):
        raise NotImplementedError

    def query(self):
        end_time = self.end_time
        start_time = self.start_time()
        self.logger.debug(f"Forming candles from {start_time.strftime('%d.%m.%Y %H:%M')} to {end_time.strftime('%d.%m.%Y %H:%M')}")
        return self.session.query(self.candle_class)\
            .filter(self.candle_class.ticker == self.ticker)\
            .filter(self.candle_class.open_date < end_time.timestamp() * K)\
            .filter(self.candle_class.open_date >= start_time.timestamp() * K)\
            .order_by(self.candle_class.open_date.desc())\
            .all()

    def get_candles(self):
        """Gives candles grouped with self.timeframe
        :return: list of Candle
        """
        if not self.candles:
            candles = self.combine_candles(self.grouped_candles())
            self.candles = candles
        else:
            candles = self.candles
        return candles

    def combine_candles(self, grouped_raw_candles):
        """Make self.timeframe candles from groups of one minute candles.

        :param grouped_raw_candles: list[list[Candle]]
        :return: list[Candle]
        """

        if self.timeframe <= 1:
            return [candle_group[0] for candle_group in grouped_raw_candles]
        else:
            candle_list = []
            for candle_group in grouped_raw_candles:
                new_candle = self.combine_candle(candle_group)
                candle_list.append(new_candle)

            return candle_list

    def grouped_candles(self):
        """Groups one minute candles to self.timeframe sized groups.

        :return: list[list[Candles]]
        """
        raw_candles = list(reversed(self.query()))

        if not raw_candles:
            self.request_candles(self.start_time(), session=self.session)
            raw_candles = list(reversed(self.query()))
            if not raw_candles:
                return []

        start_time = datetime.datetime.utcfromtimestamp(raw_candles[0].open_date / K).replace(tzinfo=pytz.utc)
        end_time = start_time + minute_to_timedelta(self.timeframe)
        if start_time.day != end_time.day:
            end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0)

        candle_group = []
        list_of_groups = []
        for candle in raw_candles:
            if start_time <= candle.open_date_dt < end_time:
                candle_group.append(candle)
            else:
                list_of_groups.append(list(reversed(candle_group)))
                candle_group = [candle]
                start_time = end_time
                end_time = end_time + minute_to_timedelta(self.timeframe)
                if start_time.day != end_time.day:
                    end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0)
        list_of_groups.append(list(reversed(candle_group)))

        return reversed(list_of_groups)

    @property
    def end_time(self):
        """Date when last timeframe candle closes"""
        if self._end_time:
            return self._end_time
        else:
            now = datetime.datetime.utcnow()

            current_minutes = datetime_to_minutes(now)
            timeframes_today = current_minutes // self.timeframe
            minutes_today = timeframes_today * self.timeframe

            return now.replace(hour=minutes_today // 60, minute=minutes_today % 60, second=0, microsecond=0, tzinfo=pytz.utc)

    @end_time.setter
    def end_time(self, value):
        self._end_time = value

    def start_time(self):
        """Date from when we must get candles for current period"""

        end_time = self.end_time
        today_minutes = datetime_to_minutes(end_time)
        candles = self.period

        candles_in_day = ceil(1440 / self.timeframe)
        full_days = candles // candles_in_day
        minutes_residue = (candles - (candles_in_day * full_days)) * self.timeframe

        if minutes_residue > today_minutes:
            minutes_residue -= today_minutes
            last_candle_in_day_size = (1440 % self.timeframe)
            if last_candle_in_day_size == 0:
                minutes_residue += self.timeframe + today_minutes - self.timeframe
            else:
                minutes_residue += last_candle_in_day_size + today_minutes - self.timeframe

        minutes_shift = (full_days * 24 * 60) + minutes_residue

        return (end_time - minute_to_timedelta(minutes_shift)).replace(tzinfo=pytz.utc)

    def request_candles(self, back_date, interrupt=Event(), session=None):
        if session is None:
            session = sessionmaker()
        now = datetime.datetime.utcnow().replace(second=0, microsecond=0)


        interrupt.set()

        self.logger.info(f"Requesting missing candles for {self.ticker}")
        newest_requested_candle = self.candle_class()
        newest_requested_candle.open_date = int(back_date.timestamp() * K)

        requests_finished = 0
        failed_requests = 0
        while (datetime.datetime.utcfromtimestamp(newest_requested_candle.open_date / K).replace(minute=0) < now.replace(minute=0)) and interrupt.is_set():
            try:
                self.request_start_time = time.time()
                time_from_last_request = self.request_start_time - self.request_end_time
                if time_from_last_request < self.SLEEP_AFTER_REQUEST:
                    time.sleep(self.SLEEP_AFTER_REQUEST - time_from_last_request)

                raw_candles = self.request_raw_candles(newest_requested_candle.open_date)
                missed_candles = int((self.candle_class.from_list(raw_candles[-1]).open_date - self.candle_class.from_list(raw_candles[0]).open_date) // 60000) - 1000
                missed_candles = missed_candles if missed_candles > 0 else 0

                self.logger.info("Requested candles for period {frm} - {to} for {ticker}, missed {missed} candles"
                            .format(frm=self.candle_class.from_list(raw_candles[0]).open_date_dt.strftime('%d.%m.%Y %H:%M'),
                                    to=self.candle_class.from_list(raw_candles[-1]).open_date_dt.strftime('%d.%m.%Y %H:%M'),
                                    ticker=self.ticker,
                                    missed=missed_candles))

                requests_finished += 1

                if not raw_candles or self.candle_class.from_list(raw_candles[-1]).open_date == newest_requested_candle.open_date:
                    break

                candles_in_database = session.query(self.candle_class.open_date, self.candle_class.ticker) \
                    .filter(self.candle_class.open_date >= self.candle_class.from_list(raw_candles[0], ticker=self.ticker).open_date) \
                    .filter(self.candle_class.open_date <= self.candle_class.from_list(raw_candles[-1], ticker=self.ticker).open_date) \
                    .filter(self.candle_class.ticker == self.ticker).all()

                for rc in raw_candles:
                    candle = self.candle_class.from_list(rc, ticker=self.ticker)
                    if (candle.open_date, candle.ticker) not in candles_in_database:
                        candle.add_or_update(session)

                    newest_requested_candle = candle
                session.commit()
                self.request_end_time = time.time()

            except (BinanceAPIException, BitfinexException) as e:
                self.logger.warning(f"Exchange error, retrying after 30 seconds.\n{e}")
                failed_requests += 1
                time.sleep(30)
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                self.logger.warning(f"Bitfinex did not respond in time, retrying after 10 seconds")
                failed_requests += 1
                time.sleep(10)
            except IndexError:
                failed_requests += 1
                break

        self.logger.info(f"Finished with {requests_finished} requests. and {failed_requests} failed requests.")