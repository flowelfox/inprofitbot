import os

from setuptools import setup

VERSION = "1.0.0-beta1.1"

def install_deps():
    default = open('requirements.txt', 'r').readlines()
    new_pkgs = []
    for resource in default:
        if 'git+ssh' in resource or 'git+https' in resource:
            pass
        else:
            new_pkgs.append(resource.strip())
    return new_pkgs


pkgs = install_deps()

setup(name='inprofit',
      version=VERSION,
      description='Python Trading Bot',
      author='Trading team',
      author_email='flowelcat@gmail.com',
      url='www.example.com',
      packages=['bot', 'backend', 'indicators'],
      install_requires=pkgs,
      )


if not os.path.exists('logs'):
    os.mkdir('logs')

