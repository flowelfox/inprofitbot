import itertools
import logging.config
from os import path

from botmanlib.utils import parse_environ_settings

BITFINEX_AVAILABLE_COINS = ["BTC", "ETC", "DSH", "XRP", "EOS", "ETH", "IOT", "XMR", "LTC", "NEO", "OMG", "ZEC", "ZRX", "ETP", "BTG", "BAB", "BSV", "XLM"]
BITFINEX_AVAILABLE_RIGHT_TICKERS = ['USD']
BITFINEX_AVAILABLE_PAIRS = [p[0] + p[1] for p in itertools.product(BITFINEX_AVAILABLE_COINS, BITFINEX_AVAILABLE_RIGHT_TICKERS) if p[0] != p[1]]

BITFINEX_TICKER_LISTENER_COINS = ["BTC", "ETH", "LTC", "EOS", "XRP", "UST", "NEO", "BAB", "IOT", "DSH", "OMG", "ETC", "XMR", "ZEC", "MGO", "BSV", "MKR", "DAI", "TKN", "TRX", "ZRX", "XLM", "BTG", "SEE", "ETP", "BAT", "QTM"]
BITFINEX_TICKER_LISTENER_BASE = ["USD"]
BITFINEX_TICKER_PAIRS = [p[0] + p[1] for p in itertools.product(BITFINEX_TICKER_LISTENER_COINS, BITFINEX_TICKER_LISTENER_BASE) if p[0] != p[1]]

BINANCE_AVAILABLE_COINS = ["BTC", "ETH", "XRP", "BNB", "TRX", "LINK", "EOS", "ADA", "ONT", "ETC", "LTC", "BCHABC", "XLM", "XMR", "NEO", "ATOM"]
BINANCE_AVAILABLE_RIGHT_TICKERS = ['USDT']
BINANCE_AVAILABLE_PAIRS = [p[0] + p[1] for p in itertools.product(BINANCE_AVAILABLE_COINS, BINANCE_AVAILABLE_RIGHT_TICKERS) if p[0] != p[1]]

BINANCE_TICKER_LISTENER_COINS = ["BTC", "ETH", "XRP", "BNB", "TRX", "LINK", "EOS", "ADA", "ONT", "USDC", "ETC", "LTC", "BCHABC", "XLM", "XMR", "NEO", "ATOM", "DASH", "ZEC", "MATIC", "USDT"]
BINANCE_TICKER_LISTENER_BASE = ["USDT"]
BINANCE_TICKER_PAIRS = [p[0] + p[1] for p in itertools.product(BINANCE_TICKER_LISTENER_COINS, BINANCE_TICKER_LISTENER_BASE) if p[0] != p[1]]

ALLOW_TRADE = True

SLEEP_AFTER_BITFINEX_REQUEST = 2  # Recommended 2 second
SLEEP_AFTER_BINANCE_REQUEST = 2  # Recommended ? second

"""---------------------------- WEBHOOK ---------------------------"""
WEBHOOK_ENABLE = False
WEBHOOK_URL = "/"
WEBHOOK_IP = '127.0.0.1'
WEBHOOK_PORT = -1

"""---------------------------- PATHS AND FOLDERS ---------------------------"""
PROJECT_ROOT = path.normpath(path.join(path.dirname(path.realpath(__file__))))
MEDIA_FOLDER = path.join(PROJECT_ROOT, 'media')
RESOURCES_FOLDER = path.join(PROJECT_ROOT, 'resources')
SETTINGS_FILE = path.join(RESOURCES_FOLDER, 'settings.json')
LOGS_BOT_DEBUG_FILE = 'bot_debug.log'
LOGS_BOT_ERROR_FILE = 'bot_error.log'
LOGS_BACKEND_DEBUG_FILE = 'backend_debug.log'
LOGS_BACKEND_ERROR_FILE = 'backend_error.log'
LOGS_ALL_FILE = 'all.log'
PLOTS_FOLDER = path.join(RESOURCES_FOLDER, 'plots')

parse_environ_settings(locals())

"""---------------------------- LOGGER CONFIG ---------------------------"""
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s-%(name)s-%(levelname)s-%(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'formatter': 'default',
            'class': 'logging.StreamHandler',
        },
        'debug_bot': {
            'level': 'DEBUG',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 5242880,  # 10MB
            'backupCount': 5,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_BOT_DEBUG_FILE)
        },
        'error_bot': {
            'level': 'ERROR',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 10485760,  # 10MB
            'backupCount': 2,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_BOT_ERROR_FILE)
        },
        'debug_backend': {
            'level': 'DEBUG',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 5242880,  # 10MB
            'backupCount': 5,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_BACKEND_DEBUG_FILE)
        },
        'error_backend': {
            'level': 'ERROR',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 10485760,  # 10MB
            'backupCount': 2,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_BACKEND_ERROR_FILE)
        },
        'all': {
            'level': 'DEBUG',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 52428800,  # 50MB
            'backupCount': 10,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_ALL_FILE)
        },
        'null': {
            'level': 'NOTSET',
            'class': 'logging.NullHandler',
        }

    },
    'loggers': {
        '': {
            'handlers': ['all', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'bot': {
            'handlers': ['debug_bot', 'error_bot', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'botmanlib': {
            'handlers': ['debug_bot', 'error_bot', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'telegram': {
            'handlers': ['debug_bot', 'error_bot', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'backend': {
            'handlers': ['debug_backend', 'error_backend', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'models': {
            'handlers': ['debug_backend', 'error_backend', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
})

try:
    from local_settings import *
except ImportError:
    pass
