from indicators.indicator import IndicatorDataInvalidError, Indicator


class SMA(Indicator):
    def __init__(self, period, source='close'):
        super(SMA, self).__init__()
        self.period = period
        self.source = source

    def add(self, candle):
        self._candles.append(candle)
        if len(self._candles) > self.period:
            del self._candles[0]
        elif len(self._candles) < self.period:
            return

        self.recalculate()

    def recalculate(self):
        if self.source == 'hl2':
            self._value = sum([(c['high'] + c['low']) / 2 for c in self._candles]) / len(self._candles)
        else:
            self._value = sum([c[self.source] for c in self._candles]) / len(self._candles)

    def value(self):
        if len(self._candles) < self.period:
            raise IndicatorDataInvalidError(indicator='SMA')

        return super(SMA, self).value()
