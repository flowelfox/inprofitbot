from indicators.hma import HMA
from indicators.indicator import Indicator


class HMACCI(Indicator):
    def __init__(self, period, source='close'):
        super(HMACCI, self).__init__()
        self.period = period

        self.hma = HMA(period, source=source)

    def add(self, candle):
        self._candles.append(candle)

        self.hma.add(candle)
        self.recalculate()
        return candle

    def recalculate(self):
        if len(self._candles) >= self.period:
            self._value = self._candles[-1]['close'] - self.hma.value()
