from indicators.indicator import Indicator


class WMA(Indicator):
    def __init__(self, period, source='close'):
        super(WMA, self).__init__()
        self.period = period
        self.source = source

    def add(self, candle):
        self._candles.append(candle)
        if len(self._candles) > self.period:
            del self._candles[0]
        elif len(self._candles) < self.period:
            return

    def recalculate(self):
        weight = sum(list(range(len(self._candles) + 1)))
        self._value = sum([c[self.source] * ((idx + 1) / weight) for idx, c in enumerate(self._candles)])

    def value(self):
        self.recalculate()
        return self._value
