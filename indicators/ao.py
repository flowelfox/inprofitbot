from indicators.indicator import Indicator, IndicatorDataInvalidError
from indicators.sma import SMA


class AO(Indicator):
    def __init__(self, longer_period, shorter_period, source='close'):
        super(AO, self).__init__()
        self.longer_period = longer_period
        self.shorter_period = shorter_period

        self.longer_sma = SMA(longer_period, source=source)
        self.shorter_sma = SMA(shorter_period, source=source)

    def add(self, candle):
        self._candles.append(candle)

        self.longer_sma.add(candle)
        self.shorter_sma.add(candle)
        self.recalculate()

    def recalculate(self):
        if len(self._candles) >= self.longer_period:
            self._value = self.shorter_sma.value() - self.longer_sma.value()
