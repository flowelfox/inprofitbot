from indicators.hma import HMA
from indicators.indicator import Indicator


class HMAO(Indicator):
    def __init__(self, longer_period, shorter_period, source='close'):
        super(HMAO, self).__init__()
        self.longer_period = longer_period
        self.shorter_period = shorter_period

        self.longer_hma = HMA(longer_period, source=source)
        self.shorter_hma = HMA(shorter_period, source=source)

    def add(self, candle):
        self._candles.append(candle)
        self.longer_hma.add(candle)
        self.shorter_hma.add(candle)

        if len(self._candles) > self.longer_period:
            del self._candles[0]
        elif len(self._candles) < self.longer_period:
            return

        self.recalculate()
        return candle

    def recalculate(self):
        self._value = self.shorter_hma.value() - self.longer_hma.value()
